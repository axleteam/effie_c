import { AppRegistry } from 'react-native';

import './shim';
import App from './src/App';


AppRegistry.registerComponent('Effie', () => App);
