

export const createRouteEpic = () => {
  currentRouteStore.curRoute = GetSalerRoutes
    .filter(salerRoute => salerRoute.TTExtID && GetTT[salerRoute.TTExtID])
    .map(salerRoute => ({
      TTData: GetTT[salerRoute.TTExtID],
      RouteData: salerRoute,
      isVisited: false,
    }))
}
//////////////////////////////////////

/* Route screen */

componentWillMount() {
  createRoute()
}

render() {
  // use currentRouteStore.curRoute to render data
}






export const createVisitEpic = (TTExtId) => {
  currentRouteStore.curVisitQuests = StepConfigClient
    .filter(step => step.StageID === STAGE_ID_VISIT)
    .map((stepData) => {
      return {
        stepData,
        isCompleted: false,
      };
    })

  currentRouteStore.curVisitItems = QuestItems
    .filter(question => !question.TTExtID || question.TTExtID === TTExtId)
    .map((question) => {
      return {
        question,
        answer: null,
        comment: null
      };
    })
}


  /* Visit screen */

  componentWillMount() {
    createVisit()
  }

  render() {
    // use currentRouteStore.curVisitQuests variable to render data
  }

  ///////////////////////////////////

/* Questions screen */

export const createQuestItemsEpic = (QuestHeaderID) => {
  currentRouteStore.curQuestItems = currentRouteStore.curVisitItems
    .filter(question => question.QuestHeaderID === QuestHeaderID)
    .reduce()
    {
      [id]: question,
      ids: []
    }
  }


  /* Questions screen */

  componentWillMount() {
    createQuestItems()
  }

  render() {
    // use currentRouteStore.curQuestItems variable to render data
  }

  ///////////////////////////////////

/* Questions screen */
  

state = {
  activeQuestionId: "",

}

render() {
  <View>
    <View activeQuestionId questions openCamera>table</View>
    <View activeQuestionId anwerFormatId onResult={(answer) => {}}>keyboard</View>
  </View>
}



<Table>
  questions.map((question) => {
    return (<QuesitonRow question openCamera/>);
  })





export const createVisitEpic = () => {
  currentRouteStore.curVisitQuests = StepConfigClient
    .filter(step => step.StageID === STAGE_ID_VISIT && (!!step.QuestHeaderID || step.StepID === STEP_ID_GPS))
    .map((stepData) => {
      return {
        stepData,
        isCompleted: false,
      };
    })
  }


  /* Visit screen */

  componentWillMount() {
    createVisit()
  }

  render() {
    // use currentRouteStore.curVisitQuests variable to render data
  }




bufferStore.visitsToSend = [...bufferStore.visitsToSend, {
    visitData: {
        "UserGuid": userStore.user.UserGuid,
        "ID": uuid.v4(),
        "IsDeleted": 0,
        "BegDate": new Date().toISOString()// this should be filled in when user opens Visit screen
            "EmployeeExtID": userStore.user.EmployeeID,
        "EndDate": new Date() // this should be filled in when user press Finish cisit button
            // "SessionID": { string }
            "TTExtID": 1,
        // "VisitEndReasonID": { string } - Причина окончания визита
        // "VisitTypeMS": { long } - Типы визитов(если их несколько ID из таблицы VisitTypes суммируются побитово)
        "StartLatitude": 0 // we will fill these fields later when will have Map screen finished
            "StartLongitude" 0
            "EndLatitude": 0
            "EndLongitude": 0
            "TimeInRoad": 0 // we will fill it later
            "GPSMatched": true

    },
    questItems: [{
        "UserGuid": userStore.user.UserGuid,
        "ID": uuid.v4(),
        "VisitID": // id that is returned from /api/Directory/CreateVisit API
            "QuestItemID",
        "Answer",
        "Comments",
        "QuestItemName" ,
        "QuestCategoryID": { string } 
        "AnswerFormatID": { string } 
        "AnswerRecommend": { string }
    }],
}]


private static String decodeAzureStorageKey(String azureKey) {
    byte[] encryptedTextBytes = new byte[0];
    try {
        String s = new String(Base64.decode(azureKey));
        String key = reverse(remove(s.substring(s.length() - 36, s.length()).toLowerCase(), "-")).substring(0, 11).concat("effie");
        SecretKey secret = new SecretKeySpec(key.getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secret);
        byte[] decode = Base64.decode(azureKey);
        encryptedTextBytes = cipher.doFinal(Arrays.copyOf(decode, decode.length - 36));

    } catch (Exception e) {
        e.printStackTrace();
    }
    return new String(encryptedTextBytes);
}

StorageCredentialsSharedAccessSignature sasBlobCredentials = new StorageCredentialsSharedAccessSignature(token);
CloudStorageAccount account = new CloudStorageAccount(sasBlobCredentials, true, "core.windows.net", AZURE_BLOB_PHOTOS);
CloudBlobClient cloudBlobClient = account.createCloudBlobClient();
cloudBlobClient.getDefaultRequestOptions().setTimeoutIntervalInMs(280000);
container = cloudBlobClient.getContainerReference("photos");

//////////////
if (container != null && container.exists()) {
    Files fileToSend = filesList.get(i);
        String fileName = fileToSend.getFilePath().substring(fileToSend.getFilePath().lastIndexOf("/") + 1);
        CloudBlockBlob blockBlobReference = container.getBlockBlobReference(fileName);
        blockBlobReference.uploadFromFile(fileToSend.getFilePath());
        fileToSend.markFileSendToAzure();
}