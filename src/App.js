
import React, { PureComponent } from 'react';
import { StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import { Root, Container } from 'native-base';
import { PersistGate } from 'redux-persist/integration/react';
import ScreenNavigation from './navigations';
import configureStore from './store/configureStore';
import moment from 'moment';
import 'moment/locale/ru';
// import NavigatorService from './utils/navigation';

moment.locale('ru');

export default class App extends PureComponent {
  render() {
    const { store, persistor } = configureStore();
    return (
      <Root>
        <Container>
          <StatusBar backgroundColor="rgba(0,0,0,0.5)" barStyle="light-content" translucent={true} />
          <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
              <ScreenNavigation />
            </PersistGate>
          </Provider>
        </Container>
      </Root>
    );
  }
}
