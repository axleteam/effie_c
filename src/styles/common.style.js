import {
  StyleSheet,
} from 'react-native';
import Config from 'react-native-config';

const styles = StyleSheet.create({
  titleTextStyle: {
    fontSize: 42,
    fontWeight: '800',
    color: Config.SETTINGS_TEXT_COLOR,
    textAlign: 'left',
  },
  titleDescriptionTextStyle: {
    fontSize: 23,
    color: Config.SETTINGS_TEXT_COLOR,
  },
});

export default styles;
