import { API_URL, APP_ID, DB_API_AUTH_HEADER } from '../../utils/constants';
import fetch from '../../utils/fetch';

export const getAppBasicData = async () => {
  return fetch(`${API_URL}/getapp.php?id=${APP_ID}`, {
    headers: DB_API_AUTH_HEADER,
  })
    .then(res => res.json());
};
