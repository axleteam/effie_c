import { combineEpics } from 'redux-observable';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import {
  INIT, LOADING, SUCCESS, ERROR,
} from '../../utils/constants';
import * as api from './methods';

export const LOGIN_USER = 'effie/login/LOGIN_USER';
const LOGIN_USER_SUCCESS = 'effie/login/LOGIN_USER_SUCCESS';
const LOGIN_USER_ERROR = 'effie/login/LOGIN_USER_ERROR';
const LOGIN_USER_CANCELLED = 'effie/login/LOGIN_USER_CANCELLED';
const LOGOUT_USER = 'effie/logout';
const UPDATE_SAVED_USER_CREDS = 'effie/user/UPDATE_SAVED_USER_CREDS';

const initialState = {
  user: null,
  phase: INIT,
  error: null,
  savedCredentials: null,
};

export const userStore = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_USER:
      return {
        ...state,
        phase: LOADING,
      };
    case LOGIN_USER_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        error: null,
        user: action.payload,
        savedCredentials: {
          ...state.savedCredentials,
          ...action.savedCredentials,
        },
      };
    case LOGIN_USER_ERROR:
      return {
        ...state,
        phase: ERROR,
        error: action.payload.error,
      };
    case LOGIN_USER_CANCELLED:
      return {
        ...state,
        phase: INIT,
        error: null,
      };
    case LOGOUT_USER:
      return {
        ...state,
        phase: INIT,
        error: null,
        user: null,
      };
    case UPDATE_SAVED_USER_CREDS:
      return {
        ...state,
        savedCredentials: {
          ...state.savedCredentials,
          ...action.payload,
        },
      };

    default: {
      return state;
    }
  }
};


/* Actions */
export const loginUser = credentials => ({
  type: LOGIN_USER,
  payload: credentials,
});

export const cancelLoginUser = () => ({
  type: LOGIN_USER_CANCELLED,
});

export const logoutUser = () => ({
  type: LOGOUT_USER,
});

export const updateSavedUserCreds = savedCredentials => ({
  type: UPDATE_SAVED_USER_CREDS,
  payload: savedCredentials,
});


const loginUserEpic = action$ => action$.ofType(LOGIN_USER)
  .mergeMap((action) => {
    const { remember, ...loginData } = action.payload;
    return Observable.fromPromise(api.loginUser(loginData))
      .map((payload) => {
        return {
          type: LOGIN_USER_SUCCESS,
          payload,
          savedCredentials: remember ? action.payload : { Password: undefined, remember: false },
        };
      })
      .catch(error => Observable.of({
        type: LOGIN_USER_ERROR,
        payload: { error },
      }))
      .takeUntil(action$.ofType(LOGIN_USER_CANCELLED));
  });

export const userEpic = combineEpics(loginUserEpic);
