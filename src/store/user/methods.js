
import { API_URL_EFFIE } from '../../utils/constants';
import fetch from '../../utils/fetch';

export const loginUser = (credentials) => {
  return fetch(
    `${API_URL_EFFIE}v1/questsync/api/account/GetUser`,
    {
      method: 'POST',
      body: JSON.stringify(credentials),
    },
  ).then(res => res.json());
};
