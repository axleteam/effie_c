import { Observable } from 'rxjs';
import { combineEpics } from 'redux-observable';
import moment from 'moment';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/observable/concat';
import {
  INIT, LOADING, SUCCESS, ERROR, STEP_ID_GPS,
} from '../../utils/constants';
import { getRandomUUID } from '../../utils/methods';
import { GET_STATISTICS_SUCCESS } from '../serverData/duck';

const GET_ROUTE_DATA = 'GET_ROUTE_DATA';
const GET_ROUTE_DATA_SUCCESS = 'GET_ROUTE_DATA_SUCCESS';
const GET_ROUTE_DATA_ERROR = 'GET_ROUTE_DATA_ERROR';
const UPDATE_DATA = 'UPDATE_DATA';

const GET_VISIT_DATA = 'GET_VISIT_DATA';
const GET_VISIT_DATA_SUCCESS = 'GET_VISIT_DATA_SUCCESS';
const GET_VISIT_ITEM_SUCCESS = 'GET_VISIT_ITEM_SUCCESS';

const GET_QUEST_ITEMS = 'GET_QUEST_ITEMS';
const GET_QUEST_ITEMS_SUCCESS = 'GET_QUEST_ITEMS_SUCCESS';

const SUBMIT_ANSWER = 'SUBMIT_ANSWER';
const SUBMIT_ANSWER_SUCCESS = 'SUBMIT_ANSWER_SUCCESS';

const COPY_QUESTIONS_TO_VISIT_ITEMS = 'COPY_QUESTIONS_TO_VISIT_ITEMS';
const COPY_VISIT_ITEM_SUCCESS = 'COPY_VISIT_ITEM_SUCCESS';
const CLEAR_DATA = 'CLEAR_DATA';

const CREATE_CURRENT_VISIT = 'CREATE_CURRENT_VISIT';
const CREATE_CURRENT_VISIT_SUCCESS = 'CREATE_CURRENT_VISIT_SUCCESS';
const UPDATE_CURRENT_VISIT = 'UPDATE_CURRENT_VISIT';

const FINISH_CURRENT_VISIT = 'FINISH_CURRENT_VISIT';

const SET_CUR_TT = 'effie/currentRoute/SET_CUR_TT';

const STAGE_ID_VISIT = 8;


const initialState = {
  curRoute: [],
  curVisit: null,
  curVisitQuests: { ids: [] },
  curVisitItems: { ids: [] },
  currentQuestItems: { ids: [] },
  phase: INIT,
  curTT: null,
  curStepData: null,
  timeOfLastVisitFinish: null,
};

export const currentRouteStore = (state = initialState, action) => {
  switch (action.type) {
    case GET_ROUTE_DATA:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_ROUTE_DATA_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        curRoute: action.data,
      };
    case GET_ROUTE_DATA_ERROR:
      return {
        ...state,
        phase: ERROR,
        curRoute: action.payload,
      };
    case GET_VISIT_DATA:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_VISIT_DATA_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        curVisitQuests: action.data,
      };
    case GET_VISIT_ITEM_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        curVisitItems: action.data,
      };
    case GET_QUEST_ITEMS_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        currentQuestItems: action.data,
      };
    case SUBMIT_ANSWER_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        currentQuestItems: action.payload,
      };
    case COPY_VISIT_ITEM_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        curVisitItems: action.curVisitItems,
        currentQuestItems: { ids: [] },
        curVisitQuests: action.curVisitQuests,
      };
    case CREATE_CURRENT_VISIT_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        curVisit: action.payload,
      };
    case UPDATE_CURRENT_VISIT:
      return {
        ...state,
        phase: SUCCESS,
        curVisit: {
          ...state.curVisit,
          ...action.payload,
        },
      };
    case FINISH_CURRENT_VISIT:
      return {
        ...state,
        timeOfLastVisitFinish: new Date().toISOString(),
      };
    case CLEAR_DATA:
      return {
        ...state,
        phase: SUCCESS,
        curVisit: initialState.curVisit,
        curVisitItems: initialState.curVisitItems,
        curVisitQuests: initialState.curVisitQuests,
      };
    case UPDATE_DATA:
      return {
        ...state,
      };
    case SET_CUR_TT:
      return {
        ...state,
        curTT: action.payload,
      };
    default:
      return state;
  }
};

/* Actions */

export const getRouteData = () => ({
  type: GET_ROUTE_DATA,
});

export const getQuestions = QuestHeaderID => ({
  type: GET_QUEST_ITEMS,
  QuestHeaderID,
});

export const getVisitData = TTExtId => ({
  type: GET_VISIT_DATA,
  payload: TTExtId,
});

export const updateQuestion = (questionId, question) => ({
  type: SUBMIT_ANSWER,
  payload: {
    questionId, question,
  },
});

export const clearVariables = () => ({
  type: CLEAR_DATA,
});

export const questionsToVisitItems = (curVisitQuests, curVisitItems) => ({
  type: COPY_QUESTIONS_TO_VISIT_ITEMS,
  payload: { curVisitQuests, curVisitItems },
});

export const createCurrentVisit = TTExtID => ({
  type: CREATE_CURRENT_VISIT,
  TTExtID,
});

export const finishCurrentVisit = () => ({
  type: FINISH_CURRENT_VISIT,
});

export const updateCurrentVisit = payload => ({
  type: UPDATE_CURRENT_VISIT,
  payload,
});

export const setCurTT = ttData => ({
  type: SET_CUR_TT,
  payload: ttData,
});

/* */


const createRouteEpic = (action$, state$) => action$.ofType(GET_ROUTE_DATA)
  .mergeMap(() => {
    const { SalerRoutes, TT, Visits, visitsToSend } = state$.value.serverDataStore;
    const curRouteDate = moment().format('YYYY-MM-DDT00:00:00'); // 2018-08-26T00:00:00
    const salerRoutes = [...SalerRoutes];
    const data = salerRoutes
      .filter(salerRoute => salerRoute.RouteDate === curRouteDate)
      .sort((a, b) => Number(a.TTSortID) - Number(b.TTSortID))
      .reduce((acc, salerRoute) => {
        const tt = TT.find(ttData => salerRoute.TTExtID === ttData.ExtID);
        if (tt) acc.push(tt);
        return acc;
      }, [])
      .map((TTData) => {
        const isVisitCompleted = visitsToSend.find(({ visitData }) => visitData.TTExtID === TTData.ExtID);
        const isVisited = isVisitCompleted || !!Visits.find((({ TTExtID }) => TTData.ExtID === TTExtID));
        return {
          TTData,
          isVisited,
        };
      });
    return Observable.concat(
      Observable.of({
        type: GET_ROUTE_DATA_SUCCESS,
        data,
      }),
      Observable.of({
        type: GET_STATISTICS_SUCCESS,
        payload: {
          totalVisits: data.length,
        },
      }),
    );
  })
  // .map(() => state$.value.serverDataStore)
  // .mergeMap(payload => Observable.of(payload)
  //   .map(({ SalerRoutes, TT }) => SalerRoutes
  //     .filter(salerRoute => salerRoute.TTExtID
  //       && TT
  //         .some(t => salerRoute.TTExtID === t.ExtID)))
  //   .map(salerRoute => payload.TT
  //     .filter(o1 => salerRoute
  //       .some(o2 => o1.ExtID === o2.TTExtID)))
  //   .map(TTDatas => TTDatas
  //     .map(TTData => ({
  //       TTData,
  //       isVisited: false,
  //     })))
  //   .map(data => ({
  //     type: GET_ROUTE_DATA_SUCCESS,
  //     data,
  //   })))
  .catch(error => Observable.of({
    type: GET_ROUTE_DATA_ERROR,
    payload: { error },
  }));

const fetchVisitQuest = (TTExtID, store) => Observable.of(store.value.serverDataStore)
  .map(({ StepConfigClient }) => {
    const { QuestItems } = store.value.serverDataStore;
    const questItems = QuestItems
      .filter(question => !question.TTExtID || question.TTExtID === TTExtID);
    return StepConfigClient.filter(
      step => step.StageID === STAGE_ID_VISIT
        && (
          (step.QuestHeaderID && questItems.filter(question => question.QuestHeaderID === step.QuestHeaderID).length)
          || step.StepID === STEP_ID_GPS
        ),
    );
  })
  .map(quests => quests.reduce((acc, quest) => {
    acc.ids.push(quest.StepID);
    acc[quest.StepID] = {
      stepData: quest,
      isCompleted: false,
    };
    return acc;
  }, { ids: [] }))
  .map(data => ({
    type: GET_VISIT_DATA_SUCCESS,
    data,
  }));

const fetchCurrentVisitItems = (action, store) => Observable.of(store.value.serverDataStore)
  .map(({ QuestItems }) => QuestItems
    .filter(question => !question.TTExtID || question.TTExtID === action.payload))
  .map(questions => questions.reduce((acc, question) => {
    acc.ids.push(question.ID);
    acc[question.ID] = {
      question,
      answer: null,
      comment: null,
      photos: [],
    };
    return acc;
  }, { ids: [] }))
  .map(data => ({
    type: GET_VISIT_ITEM_SUCCESS,
    data,
  }));

export const createVisitEpic = (action$, state$) => action$.ofType(GET_VISIT_DATA)
  .flatMap(action => Observable.concat(
    fetchVisitQuest(action.payload, state$),
    fetchCurrentVisitItems(action, state$)
      .catch(error => Observable.of({
        type: GET_ROUTE_DATA_ERROR,
        payload: { error },
      })),
  ));

export const createQuestItemsEpic = (action$, state$) => action$.ofType(GET_QUEST_ITEMS)
  .mergeMap(action => Observable.of(state$.value.currentRouteStore)
    .map(routeStore => routeStore.curVisitItems)
    .map(questions => questions.ids
      .reduce((acc, questionID) => {
        if (questions[questionID].question.QuestHeaderID === action.QuestHeaderID) {
          acc.ids.push(questionID);
          acc[questionID] = questions[questionID];
        }
        return acc;
      }, { ids: [] }))
    .map((questionsObj) => {
      return {
        ...questionsObj,
        ids: questionsObj.ids.sort((a, b) => Number(questionsObj[a].question.SortID) - Number(questionsObj[b].question.SortID)),
      };
    })
    .map(data => ({
      type: GET_QUEST_ITEMS_SUCCESS,
      data,
    }))).catch(error => Observable.of({
      type: GET_ROUTE_DATA_ERROR,
      payload: { error },
    }));

const updateQuestionEpic = ($action, $state) => $action.ofType(SUBMIT_ANSWER)
  .map(({ payload: { questionId, question } }) => {
    const currentQuestItems = {
      ...$state.value.currentRouteStore.currentQuestItems,
      [questionId]: question,
    };
    return {
      type: SUBMIT_ANSWER_SUCCESS,
      payload: currentQuestItems,
    };
  });

const copyQuestionToVisitEpic = ($action, $state) => $action.ofType(COPY_QUESTIONS_TO_VISIT_ITEMS)
  .map(({ payload: { curVisitQuests, curVisitItems } }) => ({
    type: COPY_VISIT_ITEM_SUCCESS,
    curVisitQuests,
    curVisitItems,
  }));

const createCurrentVisitEpic = ($action, $state) => $action.ofType(CREATE_CURRENT_VISIT)
  .map(({ TTExtID }) => {
    const { userStore, currentRouteStore: { timeOfLastVisitFinish } } = $state.value;
    const TimeInRoad = timeOfLastVisitFinish ? moment().diff(moment(timeOfLastVisitFinish), 'minutes') : 0;
    return {
      type: CREATE_CURRENT_VISIT_SUCCESS,
      payload: {
        UserGuid: userStore.user.userGuid,
        ID: getRandomUUID(), // please see corresponding function in `/utils/methods.js`
        IsDeleted: 0, // date when wisit begun
        EmployeeExtID: userStore.user.employeeID,
        EndDate: null, // this should be filled in when user press Finish visit button
        BegDate: new Date().toISOString(),
        TTExtID,
        StartLatitude: 0,
        StartLongitude: 0,
        EndLatitude: 0, // we will work on this later, just leave 0 for now
        EndLongitude: 0,
        TimeInRoad,
        GPSMatched: 0,
      },
    };
  });

export const routeEpics = combineEpics(createRouteEpic);
export const visitEpics = combineEpics(createVisitEpic);
export const questItemsEpics = combineEpics(createQuestItemsEpic);
export const updateQuestionEpics = combineEpics(updateQuestionEpic);
export const copyQuestionToVisitEpics = combineEpics(copyQuestionToVisitEpic);
export const createCurrentVisitEpics = combineEpics(createCurrentVisitEpic);
