
import { getRequiredFields, isAnswerEmpty, isCommentEmpty, isPhotosEmpty } from '../../utils/methods';

export const checkIfQuestionsAnswered = currentQuestItems => currentQuestItems
  .ids.filter((questionID) => {
    const {
      isAnswered,
    } = checkIfQuestionAnswered(currentQuestItems[questionID]);
    return !isAnswered;
  });

export const checkIfQuestionAnswered = (questItem) => {
  const {
    isAnswerRequired,
    isPhotoRequired,
    isCommentRequired,
  } = getRequiredFields(questItem.question, questItem.answer);
  let isAnswered = true;
  if (
    (isAnswerRequired && isAnswerEmpty(questItem.answer))
    || (isPhotoRequired && isPhotosEmpty(questItem.photos))
    || (isCommentRequired && isCommentEmpty(questItem.comment))
  ) {
    isAnswered = false;
  }
  return {
    isAnswered,
    isAnswerRequired,
    isPhotoRequired,
    isCommentRequired,
  };
};
