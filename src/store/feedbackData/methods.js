
import { SENDGRID_URL, SENDGRID_API_KEY } from '../../utils/constants';
import fetch from '../../utils/fetch';

export const sendMail = data => fetch(
  `${SENDGRID_URL}mail/send`,
  {
    method: 'POST',
    headers: {
      authorization: `Bearer ${SENDGRID_API_KEY}`,
      'content-type': 'application/json',
    },
    body: data,
  },
).then(res => res);
