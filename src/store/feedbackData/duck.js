import { Observable } from 'rxjs';
import { combineEpics } from 'redux-observable';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/observable/concat';
import {
  INIT, LOADING, SUCCESS, ERROR,
} from '../../utils/constants';
import * as api from './methods';

const SEND_MAIL_INIT = 'effie/feedbackData/SEND_MAIL_INIT';
const SEND_MAIL_SUCCESS = 'effie/feedbackData/SEND_MAIL_SUCCESS';
const SEND_MAIL_ERROR = 'effie/feedbackData/SEND_MAIL_ERROR';
const CLEAR_DATA = 'effie/feedbackData/CLEAR_DATA';
const initialState = {
  sent: false,
  sendMailPhase: INIT,
  sendMailError: null,
};

export const feedBackRouteStore = (state = initialState, action) => {
  switch (action.type) {
    case SEND_MAIL_INIT:
      return {
        ...state,
        sendMailPhase: LOADING,
      };
    case SEND_MAIL_SUCCESS:
      return {
        ...state,
        sendMailPhase: SUCCESS,
        sent: true,
      };
    case SEND_MAIL_ERROR:
      return {
        ...state,
        sendMailPhase: ERROR,
        sendMailError: action.payload,
      };
    case CLEAR_DATA:
      return {
        ...state,
        phase: SUCCESS,
        sent: initialState.sent,
        sendMailPhase: initialState.sendMailPhase,
        sendMailError: initialState.sendMailError,
      };
    default:
      return state;
  }
};

/* Actions */
export const sendMail = data => ({
  type: SEND_MAIL_INIT,
  payload: data,
});

export const clearfeedBackVariables = () => ({
  type: CLEAR_DATA,
});

const sendMailEpic = action$ => action$.ofType(SEND_MAIL_INIT)
  .mergeMap(action => Observable.fromPromise(api.sendMail(action.payload))
    .map((payload) => {
      console.log(payload);
      return {
        type: SEND_MAIL_SUCCESS,
        payload,
      };
    })
    .catch(error => Observable.of({
      type: SEND_MAIL_ERROR,
      payload: { error },
    })));

export const feedbackEpic = combineEpics(sendMailEpic);
