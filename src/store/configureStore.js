import { createStore, applyMiddleware } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
// import logger from 'redux-logger';
import { createBlacklistFilter } from 'redux-persist-transform-filter';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { rootEpic, rootReducer } from './index';

const userStoreFilter = createBlacklistFilter(
  'userStore',
  ['phase', 'error'],
);

const serverDataStoreFilter = createBlacklistFilter(
  'serverDataStore',
  ['synchDataPhase', 'pushDataPhase', 'sendVisitPhase'],
);

const feedbackDataStoreFilter = createBlacklistFilter(
  'feedBackRouteStore',
  ['sendMailPhase', 'sendMailError'],
);

const epicMiddleware = createEpicMiddleware();

const middleware = [
  epicMiddleware,
  // logger,
];

const persistConfig = {
  timeout: 10000,
  key: 'root',
  storage,
  transforms: [
    userStoreFilter,
    serverDataStoreFilter,
    feedbackDataStoreFilter,
  ],
};
const persistedReducer = persistReducer(persistConfig, rootReducer);

export default function configureStore() {
  const store = createStore(
    persistedReducer,
    applyMiddleware(...middleware),
  );

  const persistor = persistStore(store);

  epicMiddleware.run(rootEpic);

  return { store, persistor };
}
