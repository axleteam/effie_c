import { combineEpics } from 'redux-observable';
import { combineReducers } from 'redux';

import { userStore, userEpic } from './user/duck';
import { feedBackRouteStore, feedbackEpic } from './feedbackData/duck';
import { serverDataStore, serverEpic } from './serverData/duck';
import { workingDayStore } from './workingDay/duck';
import {
  currentRouteStore,
  routeEpics,
  visitEpics,
  questItemsEpics,
  updateQuestionEpics,
  copyQuestionToVisitEpics,
  createCurrentVisitEpics,
} from './currentRoute/duck';

export const rootEpic = combineEpics(
  userEpic,
  serverEpic,
  routeEpics,
  visitEpics,
  questItemsEpics,
  updateQuestionEpics,
  copyQuestionToVisitEpics,
  createCurrentVisitEpics,
  feedbackEpic,
);

export const rootReducer = combineReducers({
  userStore,
  serverDataStore,
  workingDayStore,
  currentRouteStore,
  feedBackRouteStore,
});
