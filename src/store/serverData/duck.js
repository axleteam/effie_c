import { combineEpics, ofType } from 'redux-observable';
import { Observable, of, throwError, from, forkJoin } from 'rxjs';
import {
  mergeMap,
  catchError,
  map,
  concat,
  takeUntil,
  take,
  mapTo,
} from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/from';
// import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/observable/concat';
import {
  getKeyByValue, concatMapThrough, getRandomUUID, getSASFromAzureStorageKey,
  isPhotosEmpty,
  isAnswerEmpty,
  isCommentEmpty,
} from '../../utils/methods';
import { getFileName, getDataForListItem } from './methods';
import { API_NAMES_FOR_PULL, azureAccountName, azureContainerName } from './constants';
import * as api from './api';

import {
  INIT, LOADING, SUCCESS, ERROR,
} from '../../utils/constants';
import { getRouteData } from '../currentRoute/duck';


const SYNC_DATA = 'effie/serverData/SYNC_DATA';
const SYNC_DATA_SUCCESS = 'effie/serverData/SYNC_DATA_SUCCESS';
const SYNC_DATA_ERROR = 'effie/serverData/SYNC_DATA_ERROR';
const SYNC_DATA_PROGRESS = 'effie/serverData/SYNC_DATA_PROGRESS';
const SYNC_DATA_CANCELLED = 'effie/serverData/SYNC_DATA_CANCELLED';

const SYNC_LIST_SUCCESS = 'effie/serverData/SYNC_LIST_SUCCESS';

const PUSH_DATA_SUCCESS = 'effie/serverData/PUSH_DATA_SUCCESS';
const PUSH_DATA_PROGRESS = 'effie/serverData/PUSH_DATA_PROGRESS';

const PULL_DATA_SUCCESS = 'effie/serverData/PULL_DATA_SUCCESS';
const PULL_DATA_PROGRESS = 'effie/serverData/PULL_DATA_PROGRESS';

const FINISH_QUEST = 'effie/serverData/FINISH_QUEST';
const FINISH_QUEST_SUCCESS = 'effie/serverData/FINISH_QUEST_SUCCESS';

const BACKGROUND_SYNC_ERROR = 'effie/serverData/BACKGROUND_SYNC_ERROR';

const ON_CREATE_VISIT_SUCCESS = 'effie/serverData/ON_CREATE_VISIT_SUCCESS';
const ON_CREATE_ANSWER_SUCCESS = 'effie/serverData/ON_CREATE_ANSWER_SUCCESS';
const ON_GET_AZURE_API_KEY = 'ON_GET_AZURE_API_KEY';
const ON_SEND_PHOTOS_SUCCESS = 'ON_SEND_PHOTOS_SUCCESS';
const ON_QUEST_PHOTOS_SUCCESS = 'ON_QUEST_PHOTOS_SUCCESS';
const SEND_VISIT = 'SEND_VISIT';
const SEND_VISIT_SUCCESS = 'SEND_VISIT_SUCCESS';
const SEND_VISIT_ERROR = 'SEND_VISIT_ERROR';
export const GET_STATISTICS_SUCCESS = 'GET_STATISTICS_SUCCESS';
const REMOVE_VISIT_FROM_VISITS_TO_SEND = 'effie/serverData/REMOVE_VISIT_FROM_VISITS_TO_SEND';
const CLEAR_DATA = 'effie/serverData/CLEAR_DATA';

const initialState = {
  lastSynchDatetime: null,
  synchList: null,
  synchDataProgress: 0,
  syncListFetchProgress: 0,
  pullDataProgress: 0,
  pushDataProgress: 0,
  // also here will be data fetched from server by values of API_NAMES_FOR_PULL, see fetchApiData()
  synchDataPhase: INIT,
  visitsToSend: [],
  pushDataPhase: INIT,
  sendVisitPhase: INIT,
  completionStatistics: {
    totalVisits: 0,
    totalQuestions: 0,
    completedVisits: 0,
    completedQuestions: 0,
  },
};

export const serverDataStore = (state = initialState, action) => {
  switch (action.type) {
    case SYNC_DATA:
      return {
        ...state,
        synchDataPhase: LOADING,
        synchDataProgress: 0,
        syncListFetchProgress: 0,
        pushDataProgress: 0,
        pullDataProgress: 0,
      };
    case SYNC_DATA_SUCCESS:
      return {
        ...state,
        synchDataPhase: SUCCESS,
        lastSynchDatetime: new Date().toISOString(),
      };
    case SYNC_LIST_SUCCESS:
      return {
        ...state,
        synchList: action.synchList,
        syncListFetchProgress: 100,
      };
    case PULL_DATA_SUCCESS:
      return {
        ...state,
        ...action.payload,
      };
    case SYNC_DATA_ERROR:
      return {
        ...state,
        synchDataPhase: ERROR,
        data: action.payload,
      };

    case SYNC_DATA_PROGRESS:
      return {
        ...state,
        // synchDataPhase: LOADING,
        synchDataProgress: action.payload,
      };

    case PULL_DATA_PROGRESS:
      return {
        ...state,
        pullDataProgress: action.payload,
      };
    case PUSH_DATA_PROGRESS:
      return {
        ...state,
        pushDataProgress: action.payload,
      };
    case FINISH_QUEST_SUCCESS:
      return {
        ...state,
        visitsToSend: [
          ...state.visitsToSend,
          action.payload,
        ],
      };
    case SYNC_DATA_CANCELLED:
      return {
        ...state,
        synchDataPhase: INIT,
        synchDataProgress: 0,
      };
    case 'INIT_SYNC':
      return {
        ...state,
        synchDataPhase: INIT,
        synchDataProgress: 0,
      };
    case GET_STATISTICS_SUCCESS:
      return {
        ...state,
        completionStatistics: {
          ...state.completionStatistics,
          ...action.payload,
        },
      };
    case REMOVE_VISIT_FROM_VISITS_TO_SEND:
      return {
        ...state,
        visitsToSend: action.payload,
      };
    case SEND_VISIT:
      return {
        ...state,
        sendVisitPhase: LOADING,
      };
    case SEND_VISIT_SUCCESS:
      return {
        ...state,
        sendVisitPhase: SUCCESS,
      };
    case SEND_VISIT_ERROR:
      return {
        ...state,
        sendVisitPhase: ERROR,
      };
    case CLEAR_DATA:
      return {
        ...state,
        lastSynchDatetime: initialState.lastSynchDatetime,
        synchList: initialState.synchList,
        synchDataProgress: initialState.synchDataProgress,
        synchDataPhase: initialState.synchDataPhase,
        pushDataPhase: initialState.pushDataPhase,
        sendVisitPhase: initialState.sendVisitPhase,
        completionStatistics: initialState.completionStatistics,
      };
    default:
      return state;
  }
};


/* Actions */

export const syncData = payload => ({
  type: SYNC_DATA,
  payload,
});

export const cancelSyncData = () => ({
  type: SYNC_DATA_CANCELLED,
});

export const finishQuest = payload => ({
  type: FINISH_QUEST,
  payload,
});

/** *********** */
const fetchGetSyncList = (action$, store$) => action$.pipe(
  ofType(SYNC_DATA),
  mergeMap((action) => {
    if (store$.value.serverDataStore.sendVisitPhase === LOADING) {
      return action$.pipe(
        ofType(SEND_VISIT_SUCCESS, SEND_VISIT_ERROR),
        takeUntil(action$.pipe(
          ofType(SYNC_DATA_CANCELLED),
        )),
        take(1),
      );
    }
    return of(action);
  }),
  mergeMap(() => {
    return concatMapThrough(
      () => getSyncListObservable(),
      () => pushDataObservable(action$, store$),
      () => pullDataObservable(action$, store$),
      () => of({ type: SYNC_DATA_SUCCESS }),
    ).catch(error => of({
      type: SYNC_DATA_ERROR,
      payload: error,
    })).takeUntil(action$.pipe(
      ofType(SYNC_DATA_CANCELLED),
    ));
  }),
);

const syncProgressUpdateEpic = (action$, store$) => action$.pipe(
  ofType(PULL_DATA_PROGRESS, PUSH_DATA_PROGRESS, SYNC_LIST_SUCCESS),
  map(() => {
    const {
      syncListFetchProgress,
      pullDataProgress,
      pushDataProgress,
    } = store$.value.serverDataStore;
    return {
      type: SYNC_DATA_PROGRESS,
      payload: ((10 * syncListFetchProgress) + (45 * pushDataProgress) + (45 * pullDataProgress)) / 100,
    };
  }),
);

const getSyncListObservable = () => from(api.GetSynchronizationList()).pipe(
  map(synchList => ({
    type: SYNC_LIST_SUCCESS,
    synchList,
  })),
);

const pullDataObservable = (action$, store$) => of([1]).pipe(
  mergeMap(() => {
    const { synchList } = store$.value.serverDataStore;
    const apiList = synchList.filter(listItem => Object.values(API_NAMES_FOR_PULL).includes(listItem.Name));
    const resObj = {};
    let completedCount = 0;
    const totalAmount = apiList.length;
    return Observable.from(apiList)
      .mergeMap(listItem => Observable.fromPromise(api.GetDataFromURL(listItem.Url, getDataForListItem(listItem, store$.value.userStore.user)))
        .flatMap((d) => {
          resObj[listItem.Name] = d;
          completedCount += 1;

          if (completedCount === totalAmount) {
            return Observable.concat(
              Observable.of({
                type: PULL_DATA_PROGRESS,
                payload: totalAmount === 0 ? 0 : completedCount / totalAmount * 100,
                url: listItem.Name,
              }), Observable.of({
                type: PULL_DATA_SUCCESS,
                payload: resObj,
              }),
              Observable.of(getRouteData()),
            );
          }
          return Observable.of({
            type: PULL_DATA_PROGRESS,
            payload: totalAmount === 0 ? 0 : completedCount / totalAmount * 100,
            url: listItem.Name,
          });
        }));
  }),
);


export const clearserverDataVariables = () => ({
  type: CLEAR_DATA,
});

export const pushDataObservable = (action$, state$) => of([1]).pipe(
  mergeMap(() => {
    const visitsToSend = [...state$.value.serverDataStore.visitsToSend];
    const totalProgress = visitsToSend.length * 9 + 1; // 9 - number of requests per visit, 1 - last success action
    let counter = 0;
    return concatMapThrough(
      ...visitsToSend.map(visitData => sendVisitObservable(visitData, state$)),
      () => Observable.of({
        type: PUSH_DATA_SUCCESS,
      }),
    ).mergeMap((action) => {
      counter += 1;
      return Observable.concat(
        Observable.of({
          type: PUSH_DATA_PROGRESS,
          payload: counter / totalProgress * 100,
        }),
        Observable.of(action),
      );
    });
  }),
);

const onFinishVisitEpic = (action$, store$) => action$.ofType(FINISH_QUEST)
  .map(({ payload }) => {
    const questItems = payload.questItems.ids.reduce((acc, questItemId) => {
      const questItem = payload.questItems[questItemId];
      const item = { ...questItem };
      item.ID = getRandomUUID();
      acc[questItemId] = item;
      return acc;
    }, { ids: payload.questItems.ids });
    return {
      type: FINISH_QUEST_SUCCESS,
      payload: {
        questItems,
        visitData: payload.visitData,
      },
    };
  });

const completeBackgroundSyncEpic = (action$, store$) => action$.ofType(FINISH_QUEST_SUCCESS)
  .mergeMap(({ payload }) => {
    return sendVisitObservable(payload, store$)
      .catch(() => of({
        type: BACKGROUND_SYNC_ERROR,
      }));
  });


const sendVisitObservable = (visitToSendData, $state) => of(visitToSendData).pipe(
  mergeMap(data => concatMapThrough(
    () => Observable.of({ type: SEND_VISIT }),
    () => CreateVisitObservable(data),
    () => CreateAnswerObservable(data),
    () => getAzureAPIKeyObservable(data),
    ({ key }) => uploadImagesObservable(key, data),
    ({ payload }) => questPhotosObservable(payload === null ? [] : payload.questPhotos),
    () => calculateStatisticOfCompletionObservable(data, $state),
    () => Observable.concat(
      Observable.of({
        type: SEND_VISIT_SUCCESS,
      }),
      removeVisitFromVisitsToSendObservable(visitToSendData, $state),
    ),
  ).catch(error => Observable.concat(
    of({
      type: SEND_VISIT_ERROR,
      payload: error,
    }),
    throwError(error),
  ))),
);

const calculateStatisticOfCompletionObservable = (data, $state) => Observable
  .of(data)
  .mergeMap(({ questItems }) => {
    const totalVisits = $state.value.currentRouteStore.curRoute.length;
    const totalQuestions = $state.value.serverDataStore.QuestItems.length;
    const completedVisits = $state.value.serverDataStore.Visits.length;
    const completedQuestions = questItems.ids
      .filter(questId => !isAnswerEmpty(questItems[questId].answer) || !isPhotosEmpty(questItems[questId].photos) || !isCommentEmpty(questItems[questId].comment));

    const { completionStatistics } = $state.value.serverDataStore;

    return Observable.of({
      type: GET_STATISTICS_SUCCESS,
      payload: {
        totalVisits: !completionStatistics.totalVisits ? totalVisits : completionStatistics.totalVisits,
        totalQuestions,
        completedQuestions: completionStatistics.completedQuestions + completedQuestions.length,
        completedVisits,
        // completedVisits: completionStatistics.completedVisits + 1,
      },
    });
  });

const questPhotosObservable = photos => of(photos).pipe(
  mergeMap((photosData) => {
    if (photosData.length > 0) {
      return from(api.createPhotos(photosData)).pipe(
        map(payload => ({
          type: ON_QUEST_PHOTOS_SUCCESS,
          payload,
        })),
      );
    }

    return of({
      type: ON_QUEST_PHOTOS_SUCCESS,
    });
  }),
);


const uploadImagesObservable = (key, data) => of(data).pipe(
  map(({ questItems, visitData }) => questItems.ids
    .reduce((acc, questId) => {
      questItems[questId].photos.forEach((photo, i) => {
        const fileName = getFileName(visitData.ID, questId, visitData.TTExtID, i);
        acc.photos.push({
          blobName: fileName,
          imageBase64Str: photo,
        });
        acc.questPhotos.push({
          UserGuid: visitData.UserGuid,
          ID: fileName,
          QuestAnswerID: questItems[questId].ID,
        });
      });
      return acc;
    }, { photos: [], questPhotos: [] })
  ),
  mergeMap((data) => {
    const { photos, questPhotos } = data;
    if (photos.length > 0) {
      return of(photos).pipe(
        () => forkJoin(photos.map(photo => from(api.sendImageToAzue(azureAccountName,
          azureContainerName,
          photo.blobName,
          key,
          photo.imageBase64Str,
        )))),
        mapTo({ questPhotos }),
      );
    }
    return of({ questPhotos });
  }),
  map(({ questPhotos }) => ({
    type: ON_SEND_PHOTOS_SUCCESS,
    payload: { questPhotos },
  })),
);

const getAzureAPIKeyObservable = data => Observable
  .fromPromise(api.getAzureKey({ UserGuid: data.visitData.UserGuid }))
  .map(key => ({
    type: ON_GET_AZURE_API_KEY,
    key: getSASFromAzureStorageKey(key),
  }));

const CreateVisitObservable = data => Observable
  .fromPromise(api.createVisit([data.visitData]))
  .map(payload => ({
    type: ON_CREATE_VISIT_SUCCESS,
    payload,
  }));

const CreateAnswerObservable = data => Observable.of(data)
  .map(({ visitData, questItems }) => questItems
    .ids.reduce((acc, questId) => {
      if (!isAnswerEmpty(questItems[questId].answer) || !isPhotosEmpty(questItems[questId].photos) || !isCommentEmpty(questItems[questId].comment)) {
        acc.push({
          UserGuid: visitData.UserGuid,
          ID: questItems[questId].ID,
          VisitID: visitData.ID,
          QuestItemID: questId,
          Answer: questItems[questId].answer,
          Comments: questItems[questId].comment,
          QuestItemName: questItems[questId].question.Name,
          QuestCategoryID: questItems[questId].question.QuestCategoryID,
          AnswerFormatID: questItems[questId].question.AnswerFormatID,
          AnswerRecommend: questItems[questId].question.AnswerRecommend,
          Brand: questItems[questId].question.Brand,
          Category: questItems[questId].question.Category,
          ManufacturerName: questItems[questId].question.Mnfct,
        });
      }
      return acc;
    }, []))
  .mergeMap(items => Observable.fromPromise(api.createAnswers(items))
    .map(payload => ({
      type: ON_CREATE_ANSWER_SUCCESS,
      payload,
    })));

const removeVisitFromVisitsToSendObservable = (visitToSend, store$) => Observable.of(visitToSend)
  .map(() => {
    const visitsToSend = [...store$.value.serverDataStore.visitsToSend];
    const payload = visitsToSend.filter(visitToSendFromArr => visitToSendFromArr.visitData.ID !== visitToSend.visitData.ID);
    return {
      type: REMOVE_VISIT_FROM_VISITS_TO_SEND,
      payload,
    };
  });
export const serverEpic = combineEpics(
  fetchGetSyncList,
  syncProgressUpdateEpic,
  onFinishVisitEpic,
  completeBackgroundSyncEpic,
);
