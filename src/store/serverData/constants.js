export const API_NAMES_FOR_PULL = {
  TT: 'TT',
  STEP_CONFIG: 'StepConfigClient',
  SALER_ROUTES: 'SalerRoutes',
  QUEST_HEADERS: 'QuestHeaders',
  QUEST_ITEMS: 'QuestItems',
  QUEST_CATEGORIES: 'QuestCategories',
  QUEST_ANSWER_COMMENTS: 'QuestAnswerComments',
  COMPLETED_VISITS: 'Visits',
};

export const azureAccountName = 'effiedevstorage';
export const azureContainerName = 'photos';
