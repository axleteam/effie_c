import RNFetchBlob from 'rn-fetch-blob';
import { API_URL } from '../../utils/constants';
import fetch from '../../utils/fetch';

export const GetSynchronizationList = () => fetch(
  `${API_URL}api/Synchronization/GetSynchronizationList`,
  {
    method: 'POST',
    auth: true,
  },
).then(res => res.json());

export const GetDataFromURL = (url, body) => fetch(
  `${url}`,
  {
    method: 'POST',
    auth: true,
    body: body ? JSON.stringify(body) : undefined,
  },
).then(res => res.json());

export const createVisit = (visit) => {
  visit[0].EndDate = new Date().toISOString();
  console.log(visit);
  return fetch(
    `${API_URL}api/Directory/CreateVisit`,
    {
      method: 'POST',
      body: JSON.stringify(visit),
    },
  );
};
export const createAnswers = (answers) => {
  return fetch(
    `${API_URL}api/Quest/CreateQuestAnswers`,
    {
      method: 'POST',
      body: JSON.stringify(answers),
    },
  ).then(res => res.json())
};

export const getAzureKey = userGuid => fetch(
  'http://api-dev.effie.ua/v1/questsync/api/admin/azure/blob/effie',
  {
    method: 'OPTIONS',
    body: JSON.stringify(userGuid),
    authentication: true,
  },
).then(res => res.text());

export const createPhotos = (photos) => {
  return fetch(
    `${API_URL}api/Quest/CreateQuestPhotos`,
    {
      method: 'POST',
      body: JSON.stringify(photos),
    },
  ).then(res => res.json());
};

/*
  accountName - should be taken from ./constants
  containerName - should be taken from ./constants
  blobName - name of photo (including extension), like `[bf3a8aba-99ca-4cb7-a48b-ead71dc0e054]_[9fdcfdce-6796-4a44-8f92-4a41dee3afe0]_[3c12dd97-3ad2-4db9-9a29-911ae3d8d663]_[20180804]_[3].png`
  sas - key received from '/utils/methods/` getSASFromAzureStorageKey() function
  imageBase64Str - image converted into Base64 format
*/
export const sendImageToAzue = (accountName, containerName, blobName, sas, imageBase64Str) => RNFetchBlob.fetch('PUT', `https://${accountName}.blob.core.windows.net/${containerName}/${blobName}?${sas}`, {
  'Content-Type': 'application/octet-stream',
  'x-ms-blob-type': 'BlockBlob',
}, imageBase64Str);
