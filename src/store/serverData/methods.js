import moment from 'moment';
import { API_NAMES_FOR_PULL } from './constants';

const dateToPhotoDateStr = () => {
  const date = new Date();
  return `${date.getFullYear()}${(`0${date.getMonth()+1}`).slice(-2)}${(`0${date.getDate()}`).slice(-2)}`;
};

/* 
  visitId - visit UUID,
  questItemId - QuestItemID,
  TTExtID,
  indexOfImageInAnswer
*/
export const getFileName = (visitId, questItemId, TTExtID, indexOfImageInAnswer) => `[${visitId}]_[${questItemId}]_[${TTExtID}]_[${dateToPhotoDateStr()}]_[${indexOfImageInAnswer}].png`;

export const groupPredefinedCommentsByCategoryId = (serverDataStore) => {
  const { QuestAnswerComments } = serverDataStore;
  return QuestAnswerComments.reduce((acc, comment) => {
    const isAlreadyAdded = acc.allCategoryIds.indexOf(comment.QuestCategoryID) !== -1;
    if (!isAlreadyAdded) {
      acc.allCategoryIds.push(comment.QuestCategoryID);
      acc.byCategoryId[comment.QuestCategoryID] = [];
    }
    acc.byCategoryId[comment.QuestCategoryID].push(comment);
    return acc;
  }, { allCategoryIds: [], byCategoryId: {} });
};

export const getDataForListItem = ({ Name }, { employeeID, userGuid }) => {
  if (Name === API_NAMES_FOR_PULL.COMPLETED_VISITS) {
    const curDayDate = moment({ h: 0, m: 0, s: 0, ms: 0 }).utc({ h: 0, m: 0, s: 0, ms: 0 });
    return {
      UserGuid: userGuid,
      BegDate: curDayDate.toISOString(),
      EmployeeExtID: employeeID,
    };
  }
  return undefined;
}
