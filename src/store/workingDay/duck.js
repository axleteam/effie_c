import { INIT } from '../../utils/constants';

const SET_WORKING_DAY_STAGE = 'effie/workingDay/SET_WORKING_DAY_STAGE';
const SET_WORKING_DAY_DATE = 'effie/workingDay/SET_WORKING_DAY_DATE';
const SET_WORKING_TIME_TIMER = 'effie/workingDay/SET_WORKING_TIME_TIMER';
const SET_WORKING_DAY_CHANGED = 'effie/workingDay/SET_WORKING_DAY_CHANGED';
const CLEAR_DATA = 'effie/workingDay/CLEAR_DATA';
const initialState = {
  workingDayStage: null,
  dayDate: null,
  hasWorkingDayChanged: false,
  phase: INIT,
  dayBegDatetime: '',
  isWorkingTimeTimerShowing: false,
};

export const workingDayStore = (state = initialState, action) => {
  switch (action.type) {
    case SET_WORKING_DAY_STAGE:
      return {
        ...state,
        workingDayStage: action.payload,
      };
    case SET_WORKING_DAY_DATE:
      return {
        ...state,
        dayDate: action.payload,
      };
    case SET_WORKING_TIME_TIMER:
      return {
        ...state,
        dayBegDatetime: new Date().toISOString(),
        isWorkingTimeTimerShowing: true,
      };
    case SET_WORKING_DAY_CHANGED:
      return {
        ...state,
        hasWorkingDayChanged: action.payload,
      };
    case CLEAR_DATA:
      return {
        workingDayStage: initialState.workingDayStage,
        dayDate: initialState.dayDate,
        hasWorkingDayChanged: initialState.hasWorkingDayChanged,
        phase: initialState.phase,
        dayBegDatetime: initialState.dayBegDatetime,
        isWorkingTimeTimerShowing: initialState.isWorkingTimeTimerShowing,
      };
    default:
      return state;
  }
};

/* Actions */
export const setWorkingDayStage = payload => ({
  type: SET_WORKING_DAY_STAGE,
  payload,
});

export const setWorkingDayDate = payload => ({
  type: SET_WORKING_DAY_DATE,
  payload,
});

export const setTimer = () => ({
  type: SET_WORKING_TIME_TIMER,
});

export const setWorkingDayChanged = hasWorkingDayChanged => ({
  type: SET_WORKING_DAY_CHANGED,
  payload: hasWorkingDayChanged,
});
export const clearworkingDayVariables = () => ({
  type: CLEAR_DATA,
});
