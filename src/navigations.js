import { createStackNavigator, createDrawerNavigator, createSwitchNavigator } from 'react-navigation';
import React from 'react';
import LeftDrawer from './components/LeftDrawer';
import LoginScreen from './containers/LoginScreen';
import OnBoardingScreen from './containers/OnBoardingScreen';
import SyncronizationScreen from './containers/SyncronizationScreen';
import RouteScreen from './containers/RouteScreen';
import VisitScreen from './containers/VisitScreen';
import QuestionsTableScreen from './containers/QuestionsTableScreen';
import MapScreen from './containers/MapScreen';
import FeedbackScreen from './containers/FeedbackScreen';
import AboutUsScreen from './containers/AboutUsScreen/AboutUsScreen';
import ResultScreen from './containers/ResultScreen';

const AuthStack = createStackNavigator({
  AuthFeedback: {
    screen: FeedbackScreen,
  },
  AuthAboutUs: {
    screen: AboutUsScreen,
  },
  Login: {
    screen: LoginScreen,
  },
}, {
  initialRouteName: 'Login',
});

const HomeDrawerNavigator = createDrawerNavigator({
  OnBoarding: {
    screen: OnBoardingScreen,
  },
  Syncronization: {
    screen: SyncronizationScreen,
  },
  RouteScreen: {
    screen: RouteScreen,
  },
  ResultScreen: {
    screen: ResultScreen,
  },
  Feedback: {
    screen: FeedbackScreen,
  },
  AboutUs: {
    screen: AboutUsScreen,
  },
  VisitScreen: {
    screen: VisitScreen,
    navigationOptions: () => ({
      drawerLockMode: 'locked-closed',
    }),
  },
  QuestionsTableScreen: {
    screen: QuestionsTableScreen,
    navigationOptions: () => ({
      drawerLockMode: 'locked-closed',
    }),
  },
  MapScreen: {
    screen: MapScreen,
    navigationOptions: () => ({
      drawerLockMode: 'locked-closed',
    }),
  },
  // Login: {
  //   screen: LoginScreen,
  // },
}, {
  initialRouteName: 'RouteScreen',
  contentComponent: ({ navigation }) => (
    <LeftDrawer navigation={navigation} />
  ),
});

HomeDrawerNavigator.navigationOptions = {
  header: null,
  drawerLockMode: 'locked-closed',
  backBehavior: 'initialRoute',
};
// export default createSwitchNavigator({
//   Home: {
//     screen: HomeDrawerNavigator,
//   },
//   Auth: {
//     screen: AuthStack,
//   },
// }, {
//   initialRouteName: 'Auth',
//   navigationOptions: {
//     header: null,
//   },
// });

export default createSwitchNavigator({
  Home: {
    screen: HomeDrawerNavigator,
  },
  Auth: {
    screen: AuthStack,
  },
}, {
  initialRouteName: 'Auth',
  navigationOptions: {
    header: null,
  },
});
