import React from 'react';
import {
  View, Text, StyleSheet,
} from 'react-native';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import { Button } from 'native-base';
import PropTypes from 'prop-types';
import moment from 'moment';
import { capitalizeFirstLetter } from '../../utils/methods';
import { DATE_ANSWER_FORMAT } from '../../utils/constants';


LocaleConfig.locales['ru'] = {
  monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
  monthNamesShort: ['Janv.', 'Févr.','Mars','Avril','Mai','Juin','Juil.','Août','Sept.','Oct.','Nov.','Déc.'],
  dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
  dayNamesShort: ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ']
};

LocaleConfig.defaultLocale = 'ru';

const calendarFormat = 'YYYY-MM-DD';

class CalendarModal extends React.Component {

  constructor(props) {
    super(props);

    const initialDate = props.date ? moment(props.date, DATE_ANSWER_FORMAT) : moment();

    this.state = {
      selectedDate: initialDate.format(calendarFormat),
    };
  }

  onChangeDate = ({ dateString }) => this.setState(() => ({ selectedDate: dateString }))

  onSubmit = () => {
    const formatted = moment(this.state.selectedDate, calendarFormat).format(DATE_ANSWER_FORMAT);
    this.props.onSubmit(formatted);
  }

  render() {
    const {
      onCancel,
    } = this.props;
    const { selectedDate } = this.state;
    const selectedMoment = moment(selectedDate, calendarFormat);
    const displayingStr = capitalizeFirstLetter(selectedMoment.format('dd, DD MMMM'));
    return (
      <View style={styles.modalContainer}>
        <View style={styles.headerContainer}>
          <Text style={{ color: '#fff' }}>
            {selectedMoment.year()}
          </Text>
          <Text style={{ color: '#fff', fontSize: 17 }}>
            {displayingStr}
          </Text>
        </View>
        <View>
          <Calendar
            onDayPress={this.onChangeDate}
            current={selectedDate}
            markedDates={{
              [selectedDate]: { selected: true, selectedColor: 'rgb(95,95,209)' },
            }}
            theme={{
              dayTextColor: 'rgb(133,133,133)',
              textDayFontSize: 14,
              textMonthFontSize: 15,
              todayTextColor: 'rgb(133,133,133)',
              arrowColor: 'rgb(133,133,133)',
            }}
            firstDay={1}
          />
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', paddingVertical: 20 }}>
          <Button onPress={onCancel} style={styles.calenderButton}>
            <Text style={styles.buttonText}>
              Отмена
            </Text>
          </Button>
          <Button
            style={styles.calenderButton}
            onPress={this.onSubmit}
          >
            <Text style={styles.buttonText}>
              Ок
            </Text>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  calenderButton: {
    padding: 40,
    backgroundColor: 'rgb(95,95,209)',
    width: 136,
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    flex: 1,
    textAlign: 'center',
  },
  modalContainer: {
    marginHorizontal: 20,
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderRadius: 8,
  },
  headerContainer: {
    backgroundColor: 'rgb(95,95,209)',
    padding: 12,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
});

CalendarModal.propTypes = {
  date: PropTypes.string,
  onSelectDate: PropTypes.func,
  onCancel: PropTypes.func,
  onSubmit: PropTypes.func,
};

export default CalendarModal;
