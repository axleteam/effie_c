import React from 'react';
import Modal from 'react-native-modal';
import PropTypes from 'prop-types';
import {
  View, Text, StyleSheet, ScrollView, Image, TouchableOpacity, Dimensions
} from 'react-native';
import moment from 'moment';
import { Button } from 'native-base';
import deletePng from '../../img/dell.png';
import ModalTitle from '../ModalTitle';
import { DATE_ANSWER_FORMAT } from '../../utils/constants';
import { getRecommentDropdownAnswer } from '../../utils/methods';

const { height: viewportHeight } = Dimensions.get('window');

const dateFormat = 'DD.MM.YY';

const InfoModal = ({
  onClick, isVisible, data: {
    question, answer, comment, photos,
  }, onRemovePhoto, categoryName,
}) => {
  const startMoment = question.StartDate && moment(question.StartDate).format(dateFormat);
  const endMoment = question.EndDate && moment(question.EndDate).format(dateFormat);
  const startEndStr = [startMoment, endMoment].filter(d => !!d).join(' - ');

  let answerStr = answer;
  if (question.AnswerFormatID === '2' && answer) answerStr = moment(answer, DATE_ANSWER_FORMAT).format('YYYY-MM-DD');

  let recommendStr = question.AnswerRecommend;
  if (question.AnswerFormatID === '8') recommendStr = question.AnswerRecommend ? getRecommentDropdownAnswer(question.AnswerRecommend) : '';

  return (
    <Modal isVisible={isVisible}>
      <ModalTitle title={question.Name} />
      <View style={{ backgroundColor: '#fff' }}>
        <ScrollView style={styles.contentContainerStyle}>
          <ListInfoData
            title="Рекомендуемый ответ:"
            text={recommendStr}
            backgroundColor="rgba(216,216,216,0.2)"
          />
          <ListInfoData title="Ответ:" text={answerStr} />
          <ListInfoData title="Комментарий:" text={comment} backgroundColor="rgba(216,216,216,0.2)" />
          {categoryName && <ListInfoData title="Категория товара:" text={categoryName} />}
          {photos.length > 0 && (
            <View style={styles.imageListContainer}>
              <Text style={styles.labelStyle}>
                Фото:
              </Text>
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                {photos.map((photo, i) => (
                  <View key={i} style={styles.imageView}>
                    <Image
                      source={{ uri: `data:image/gif;base64,${photo}` }}
                      style={styles.imageStyle}
                    />
                    <TouchableOpacity style={styles.removeIcon} onPress={() => onRemovePhoto(i)}>
                      <Image source={deletePng} />
                    </TouchableOpacity>
                  </View>
                ))}

              </ScrollView>
            </View>
          )}
          {startEndStr && (
            <View style={styles.timeContainer}>
              <Text style={styles.timeText}>
                ПЕРИОД: {startEndStr}
              </Text>
            </View>
          )}
        </ScrollView>
        <Button block style={styles.buttonStyle} onPress={onClick}>
          <Text style={styles.buttonText}>
            Ок
          </Text>
        </Button>
      </View>
    </Modal>
  );
};

const ListInfoData = ({ title, text, backgroundColor }) => (
  <View style={[
    { backgroundColor }, styles.listContainer]
  }
  >
    <Text style={styles.labelStyle}>
      {title}
    </Text>
    <Text style={styles.valueStyle}>
      {text}
    </Text>
  </View>
);

InfoModal.propTypes = {
  isVisible: PropTypes.bool.isRequired,
  data: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,
  onRemovePhoto: PropTypes.func.isRequired,
};

ListInfoData.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.any,
  backgroundColor: PropTypes.string,
};

const styles = StyleSheet.create({
  contentContainerStyle: {
    maxHeight: viewportHeight * 0.6,
  },
  buttonStyle: {
    marginHorizontal: 20,
    marginVertical: 20,
    backgroundColor: 'rgb(95,95,209)',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
  },
  listContainer: {
    paddingHorizontal: 20,
    minHeight: 66,
    paddingVertical: 10,
  },
  imageListContainer: {
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  timeText: {
    fontSize: 11,
    color: 'rgb(133,133,133)',
    textAlign: 'center',
  },
  timeContainer: {
    height: 33,
    backgroundColor: 'rgba(216,216,216,0.2)',
    justifyContent: 'center',
  },
  labelStyle: {
    fontSize: 14,
    color: 'rgb(133,133,133)',
    marginBottom: 2,
  },
  valueStyle: {
    fontSize: 17,
    color: 'rgb(133,133,133)',
  },
  imageView: {
    marginRight: 10,
    paddingRight: 8,
    paddingTop: 8,
  },
  removeIcon: {
    position: 'absolute',
    right: 0,
  },
  imageStyle: {
    height: 64,
    width: 50,
  },
});


export default InfoModal;
