import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
} from 'react-native';
import PropTypes from 'prop-types';

const { width } = Dimensions.get('window');

const ScoreCard = ({
  title, get, total, getText, totalText,
}) => (
  <View style={styles.mainArea}>
    <View style={styles.textArea}>
      <Text style={styles.titleText}>
        {title}
      </Text>
      <View style={styles.bottomArea}>
        <View>
          <Text style={styles.scoreNumber}>
            {get}
          </Text>
          <Text style={styles.scoreText}>
            {getText}
          </Text>
        </View>
        <View>
          <Text style={[styles.scoreNumber, { alignSelf: 'flex-end' }]}>
            {total}
          </Text>
          <Text style={styles.scoreText}>
            {totalText}
          </Text>
        </View>

      </View>
    </View>
  </View>
);
const styles = StyleSheet.create({
  mainArea: {
    height: 160,
    width: width - 36,
    backgroundColor: 'rgb(140, 195, 91)',
    borderRadius: 4,
    alignSelf: 'center',
    margin: 10,
  },
  textArea: {
    padding: 20,
    justifyContent: 'space-between',
    flex: 1,
  },
  titleText: {
    color: '#fff',
    fontSize: 17,
    fontWeight: '600',
  },
  scoreNumber: {
    color: '#fff',
    fontSize: 36,
    fontWeight: '600',
  },
  scoreText: {
    color: '#fff',
    fontSize: 14,
    fontWeight: '600',
  },
  bottomArea: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
ScoreCard.propTypes = {
  title: PropTypes.string,
  get: PropTypes.number,
  total: PropTypes.number,
  getText: PropTypes.string,
  totalText: PropTypes.string,
};
export default ScoreCard;
