import React from 'react';
import {
  View, Text, StyleSheet, ScrollView, Dimensions,
} from 'react-native';
import {
  Button, Item, Input, Label,
} from 'native-base';
import PropTypes from 'prop-types';
import Modal from 'react-native-modal';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import { truncateDropdownRecommentAnswer } from '../../utils/methods';

const { height } = Dimensions.get('window');

class DropdownModal extends React.PureComponent {


  constructor(props) {
    super(props);

    const textModal = props.value && props.options.findIndex(option => this.loadValue(option) === props.value) === -1;

    this.state = {
      textModal,
      textValue: textModal ? props.value : '',
    };
  }

  toggleTextModal = () => {
    this.setState(state => ({ textModal: !state.textModal }));
  }

  onTextInputViewCancel = () => this.toggleTextModal();

  onTextInputViewSubmit = () => {
    // await this.toggleTextModal();
    this.props.onSubmit(this.state.textValue);
  }

  onTextInputViewChange = textValue => this.setState(() => ({ textValue }));

  onModalCancel = () => this.props.onCancel();

  loadValue = (option) => {
    const { name, extraOptionText } = this.props;

    switch (true) {
      case (option === ''):
        return extraOptionText;
      case (typeof option === 'string'):
        return truncateDropdownRecommentAnswer(option);
      case (typeof option === 'object'):
        return option[name];
      default:
        return '';
    }
  }

  renderTextInput = () => {
    const { textValue } = this.state;
    const { maxLength } = this.props;
    const {
      textInputViewContainer,
      textInputContainer,
      textViewButtonsContainer,
      textLengthContainer,
      textLengthTextStyle,
    } = styles;
    return (
      <View style={textInputViewContainer}>
        <View style={textInputContainer}>
          <Item floatingLabel>
            <Label>
              Введите комментарий
            </Label>
            <Input
              onChangeText={this.onTextInputViewChange}
              value={textValue}
              multiline
              autoFocus
              maxLength={maxLength || 100}
            />
          </Item>
        </View>
        <View style={textLengthContainer}>
          <Text style={textLengthTextStyle}>{textValue.length}/100</Text>
        </View>
        <View style={textViewButtonsContainer}>
          <Button onPress={this.onTextInputViewCancel} style={styles.calenderButton}>
            <Text style={styles.buttonText}>
              Отмена
            </Text>
          </Button>
          <Button
            style={styles.calenderButton}
            onPress={this.onTextInputViewSubmit}
          >
            <Text style={styles.buttonText}>
              Ок
              </Text>
          </Button>
        </View>
      </View>
    );
  }

  render() {
    const {
      options, isVisible, value, name, key, title,
    } = this.props;
    const { textModal } = this.state;
    const { textStyle, radioButtonStyle, optionsViewContainer } = styles;
    let activeIndex = options.findIndex(option => this.loadValue(option) === value);
    if (activeIndex === -1 && value != null) {
      activeIndex = options.length - 1;
    }

    return (
      <Modal
        isVisible={isVisible}
        avoidKeyboard
        onBackdropPress={this.onModalCancel}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalHeaderContainer}>
            <Text style={styles.headerStyle}>
              {title}
            </Text>
          </View>
          {textModal
            ? this.renderTextInput()
            : (
              <View style={optionsViewContainer}>
                <ScrollView
                  showsVerticalScrollIndicator={false}
                  contentContainerStyle={{ paddingVertical: 10 }}
                  contentContainerStyle={{ borderWidth: 0, marginHorizontal: 0 }}
                  showsVerticalScrollIndicator
                >
                  <View style={{ paddingVertical: 10 }}>
                    <RadioGroup
                      color="rgb(95,95,209)"
                      style={{ paddingHorizontal: 10 }}
                      onSelect={(index, selectValue) => {
                        if (selectValue === '') {
                          this.toggleTextModal();
                        } else {
                          this.props.onSubmit(selectValue);
                        }
                      }}
                      selectedIndex={activeIndex !== -1 ? activeIndex : 0}
                    >
                      {options.map((option, i) => (
                        <RadioButton value={typeof option === 'string' ? truncateDropdownRecommentAnswer(option) : option[name]} key={this.loadValue(option)} style={radioButtonStyle}>
                          <Text style={textStyle}>
                            {this.loadValue(option)}
                          </Text>
                        </RadioButton>
                      ))}
                    </RadioGroup>
                  </View>
                </ScrollView>
              </View>
            )}
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    marginHorizontal: 20,
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderRadius: 10,
  },
  modalHeaderContainer: {
    backgroundColor: 'rgb(95,95,209)',
    padding: 16,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  headerStyle: {
    color: '#fff',
    fontSize: 17,
  },
  inputContainer: {
    marginBottom: 8,
  },
  textStyle: {
    color: 'rgb(133,133,133)',
    fontSize: 17,
  },
  radioButtonStyle: {
    paddingVertical: 20,
  },
  textInputViewContainer: {
    maxHeight: height * 0.43,
  },
  textInputContainer: {
    marginTop: 10,
    paddingHorizontal: 10,
    maxHeight: height * 0.43 - 80, // 90 is height of cancel and ok buttons
  },
  textLengthContainer: {
    alignItems: 'flex-end',
    paddingHorizontal: 10,
    marginTop: 3,
  },
  textLengthTextStyle: {
    fontSize: 15,
    color: 'rgb(133, 133, 133)',
  },
  textViewButtonsContainer: {
    paddingVertical: 20,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  calenderButton: {
    flex: 0.45,
    backgroundColor: 'rgb(95,95,209)',
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
    flex: 1,
  },
  optionsViewContainer: {
    maxHeight: height * 0.7,
  },
});

DropdownModal.propTypes = {
  onSubmit: PropTypes.func,
  value: PropTypes.any,
  options: PropTypes.array,
  isVisible: PropTypes.bool,
  key: PropTypes.any,
  name: PropTypes.any,
  extraOptionText: PropTypes.string,
};

export default DropdownModal;
