import React from 'react';
import {
  Text, TouchableOpacity, View, StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const QuestItem = ({
  item, index, enabled, onPress, length,
}) => {
  const completedColor = 'rgb(132,185,86)';
  const disabledColor = 'rgb(133,133,133)';
  let selectedColor = 'rgb(95,95,209)';
  if (enabled === true) {
    selectedColor = completedColor;
  } else if (!enabled) {
    selectedColor = disabledColor;
  }

  return (
    <TouchableOpacity
      onPress={onPress}
      disabled={!enabled}
    >
      <View style={[styles.topContainer, !enabled && { opacity: 0.5 }]}>
        <View style={styles.cardStyle}>
          <View style={styles.cardInner}>
            {index !== 0 && <View style={[styles.leftBorderStyles, { bottom: 15 }]} />}
            <View style={[styles.pointStyle, { backgroundColor: selectedColor }]}>
              {item.isCompleted
                ? <Icon name="check" color="#fff" size={18} />
                : (
                  <Text style={{ color: '#fff', fontWeight: 'bold' }}>
                    {index + 1}
                  </Text>
                )}

            </View>
            {index !== length - 1
               && <View style={[styles.leftBorderStyles, { top: 15 }]} />}
          </View>
          <View style={[styles.rightCard, { borderLeftColor: selectedColor }]}>
            <Text style={styles.cardTopText}>
              {item.stepData.Name}
            </Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  topContainer: {
    flexDirection: 'row',
    shadowColor: '#000000',
    shadowOpacity: 0.1,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 8,
  },
  cardStyle: {
    marginTop: 15,
    paddingLeft: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  leftBorderStyles: {
    position: 'absolute',
    minHeight: 30,
    borderWidth: 0.5,
    width: 1,
    borderColor: 'rgb(192,192,192)',
    marginVertical: 5,
    alignSelf: 'center',
  },
  pointStyle: {
    borderRadius: 30 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    height: 30,
    width: 30,
    position: 'absolute',
    borderWidth: 2,
    borderColor: '#fff',
  },
  rightCard: {
    backgroundColor: '#fff',
    paddingLeft: 14,
    paddingRight: 10,
    paddingVertical: 15,
    width: '88%',
    borderRadius: 4,
    borderLeftWidth: 5,
  },
  cardTopText: {
    fontSize: 17,
    fontWeight: '600',
    color: 'rgb(5, 6, 51)',
  },
  cardInner: {
    alignSelf: 'center',
    width: '10%',
    justifyContent: 'center',
  },
});

QuestItem.propTypes = {
  item: PropTypes.object,
  index: PropTypes.number,
  enabled: PropTypes.any,
  onPress: PropTypes.func,
  length: PropTypes.number
};

export default QuestItem;
