import React from 'react';
import {
  View, Text, StyleSheet, TouchableOpacity, Image,
} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import mapPng from '../../img/mapicon.png';

const mapIconHitSlop = { top: 5, left: 10, right: 7, bottom: 10 };

const RouteCard = ({
  route: { TTData, isVisited }, index, length, onPress,
}) => {
  const addressString = [TTData.Address_1, TTData.Address_2, TTData.Address_3, TTData.Address_4]
    .filter(address => !!address).join(', ');
  return (
    <View style={styles.topContainer}>
      <View style={styles.cardStyle}>
        <View style={styles.cardInner}>
          {index !== 0 && <View style={[styles.leftBorderStyles, { bottom: 15 }]} />}
          <View style={[styles.pointStyle, { backgroundColor: isVisited ? 'rgb(132,185,86)' : 'rgb(95,95,209)' }]}>
            {isVisited
              ? <Icon name="check" color="#fff" size={18} />
              : (
                <Text style={{ color: '#fff', fontWeight: 'bold' }}>
                  {index + 1}
                </Text>
              )}

          </View>
          {index !== length - 1
            && <View style={[styles.leftBorderStyles, { top: 15 }]} />}
        </View>
        <View style={styles.rightCard}>
          <View style={{ flexDirection: 'row', paddingLeft: 10, paddingVertical: 8 }}>
            <View style={styles.topCard1}>
              <View>
                <Text style={styles.cardTopText}>
                  {TTData.Name}
                </Text>
              </View>
              <View style={{ marginTop: 3 }}>
                <Text style={styles.cardBottomText}>
                  {addressString}
                </Text>
              </View>
            </View>

            <TouchableOpacity onPress={onPress} hitSlop={mapIconHitSlop}>
              <Image source={mapPng} />
            </TouchableOpacity>
          </View>
          <View style={[
            styles.bottomCardStyle,
            { backgroundColor: isVisited ? 'rgb(132,185,86)' : 'rgb(95,95,209)' },
            !(TTData.RecTimeBeg && TTData.RecTimeEnd) && { paddingVertical: 3 }]}
          >
            {(TTData.RecTimeBeg && TTData.RecTimeEnd) && (
              <Text style={styles.bottomCardText}>
                {`Рек. время визита: ${TTData.RecTimeBeg} - ${TTData.RecTimeEnd}`}
              </Text>
            )}
            {TTData.TimeInTT && (
              <Text style={styles.bottomCardText}>
                {`В торговой точке: ${TTData.TimeInTT} минут`}
              </Text>
            )}
          </View>
        </View>

      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  topContainer: {
    flexDirection: 'row',
    shadowColor: '#000000',
    shadowOpacity: 0.1,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 8,
  },
  cardStyle: {
    marginTop: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  bottomCardStyle: {
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    justifyContent: 'center',
    paddingLeft: 10,
    paddingVertical: 10,
  },
  topCard1: {
    width: '90%',
  },
  cardTopText: {
    fontSize: 17,
    fontWeight: '700',
  },
  cardBottomText: {
    fontSize: 13,
  },
  bottomCardText: {
    color: '#fff',
    fontSize: 13,
  },
  pointStyle: {
    borderRadius: 30 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    height: 30,
    width: 30,
    position: 'absolute',
    borderWidth: 2,
    borderColor: '#fff',
  },
  leftBorderStyles: {
    position: 'absolute',
    minHeight: 50,
    borderWidth: 0.5,
    width: 1,
    borderColor: 'rgb(192,192,192)',
    marginVertical: 5,
    alignSelf: 'center',
  },
  cardInner: {
    alignSelf: 'center',
    width: '15%',
    justifyContent: 'center',
  },
  rightCard: {
    backgroundColor: '#fff',
    width: '85%',
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
  },
});

RouteCard.propTypes = {
  route: PropTypes.object,
  index: PropTypes.number,
  length: PropTypes.number,
  onPress: PropTypes.func,
};

export default RouteCard;
