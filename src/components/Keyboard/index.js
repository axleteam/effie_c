import React from 'react';
import {
  View, StyleSheet, TouchableOpacity, Image, Dimensions,
} from 'react-native';
import PropTypes from 'prop-types';
import Modal from 'react-native-modal';
import { isIphoneX } from 'react-native-iphone-x-helper';
import CalendarModal from '../CalendarModal';
import TextModal from '../TextModal';
import CalculatorModal from '../CalculatorModal';
import DropdownModal from '../DropdownModal';
import InfoModal from '../InfoModal';
import NonNumbericKeyboard from '../NonNumbericKeyboard';
import NumericKeyboard from '../NumericKeyboard';

import downPng from '../../img/downArrow.png';
import upPng from '../../img/upArrow.png';

const ImagePicker = require('react-native-image-picker');

const { height, width } = Dimensions.get('window');

const RIGHT_BLOCK_WIDTH = width * 0.16;
const LEFT_BLOCK_WIDTH = width - RIGHT_BLOCK_WIDTH;

/* Non numeric keyboards */
const SMALL_BUTTON_SIZE = LEFT_BLOCK_WIDTH * 0.14;
const BIG_BUTTON_SIZE = LEFT_BLOCK_WIDTH * 0.19;

/* 6 - number of paddings between buttons */
const PADDING = (LEFT_BLOCK_WIDTH - (SMALL_BUTTON_SIZE * 3) - (BIG_BUTTON_SIZE * 2)) / 6 / 2;

/* Numberic keyboards */
const BTN_SIZE = LEFT_BLOCK_WIDTH * 0.115;
const PADDING_HORIZONTAL = (LEFT_BLOCK_WIDTH - (BTN_SIZE * 7)) / 8 / 2;

const bottomSpace = isIphoneX() ? 40 : 0;

class Keyboard extends React.Component {
  static propTypes = {
    active: PropTypes.any,
    onAnswer: PropTypes.func,
    onPressArrow: PropTypes.func,
    onAddImage: PropTypes.func,
    onRemoveImage: PropTypes.func,
    viewHeight: PropTypes.number,
  }

  state = {
    calendarModal: false,
    textModal: false,
    calculatorModal: false,
    dropdownModal: false,
    infoModal: false,
  };

  componentDidMount() {
    this.props.onRef(this);
  }

  openCamera = async () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        path: 'effie',
      },
    };

    ImagePicker.launchCamera(options, (response) => {
      if (!response || response.error || response.didCancel) return;
      this.props.onAddImage(response.data);
    });
  }

  onClear = () => this.submitAnswer(null);

  toggleInfo = () => this.setState(state => ({ infoModal: !state.infoModal }))

  removePhoto = (index) => {
    this.props.onRemoveImage(index);
  }

  toggleCalendar = () => {
    this.setState(state => ({ calendarModal: !state.calendarModal }));
  }

  toggleTextModal = () => {
    this.setState(state => ({ textModal: !state.textModal }));
  }

  toggleCalculatorModal = () => {
    this.setState(state => ({ calculatorModal: !state.calculatorModal }));
  }

  toggleDropdownModal = () => {
    this.setState(state => ({ dropdownModal: !state.dropdownModal }));
  }

  submitAnswer = (answer) => {
    const { onAnswer } = this.props;
    return onAnswer(answer);
  }

  onDropdownSubmit = (text) => {
    this.submitAnswer(text);
    this.toggleDropdownModal();
  }

  renderCalendarType = (PhotoReport) => {
    return (
      <NonNumbericKeyboard
        keyboardType="calendar"
        answerSubmit={this.submitAnswer}
        smallButtonSize={SMALL_BUTTON_SIZE}
        bigButtonSize={BIG_BUTTON_SIZE}
        padding={PADDING}
        isPhotoBtnShowing={PhotoReport}
        onInfoPress={this.toggleInfo}
        onClearPress={this.onClear}
        onPhotoPress={this.openCamera}
        onCalendarPress={this.toggleCalendar}
      />
    );
  }

  renderBooleanType = (PhotoReport) => {
    return (
      <NonNumbericKeyboard
        keyboardType="boolean"
        answerSubmit={this.submitAnswer}
        smallButtonSize={SMALL_BUTTON_SIZE}
        bigButtonSize={BIG_BUTTON_SIZE}
        padding={PADDING}
        isPhotoBtnShowing={PhotoReport}
        onInfoPress={this.toggleInfo}
        onClearPress={this.onClear}
        onPhotoPress={this.openCamera}
      />
    );
  }

  renderTextType = (PhotoReport) => {
    return (
      <NonNumbericKeyboard
        keyboardType="text"
        answerSubmit={this.submitAnswer}
        smallButtonSize={SMALL_BUTTON_SIZE}
        bigButtonSize={BIG_BUTTON_SIZE}
        padding={PADDING}
        isPhotoBtnShowing={PhotoReport}
        onInfoPress={this.toggleInfo}
        onClearPress={this.onClear}
        onTextPress={this.toggleTextModal}
        onPhotoPress={this.openCamera}
      />
    );
  }

  renderDropdownType = (PhotoReport) => {
    return (
      <NonNumbericKeyboard
        keyboardType="dropdown"
        answerSubmit={this.submitAnswer}
        smallButtonSize={SMALL_BUTTON_SIZE}
        bigButtonSize={BIG_BUTTON_SIZE}
        padding={PADDING}
        isPhotoBtnShowing={PhotoReport}
        onInfoPress={this.toggleInfo}
        onClearPress={this.onClear}
        onDropdownPress={this.toggleDropdownModal}
        onPhotoPress={this.openCamera}
      />
    );
  }

  renderCalculatorType = (PhotoReport, answer) => {
    return (
      <NumericKeyboard
        keyboardType="calculator"
        btnSize={BTN_SIZE}
        paddingHorizontal={PADDING_HORIZONTAL}
        answer={answer}
        answerSubmit={this.submitAnswer}
        isPhotoBtnShowing={PhotoReport}
        onInfoPress={this.toggleInfo}
        onClearPress={this.onClear}
        onCalculatorPress={this.toggleCalculatorModal}
        onPhotoPress={this.openCamera}
      />
    );
  }

  renderFractionalType = (PhotoReport, answer) => {
    return (
      <NumericKeyboard
        keyboardType="decimal"
        btnSize={BTN_SIZE}
        paddingHorizontal={PADDING_HORIZONTAL}
        answer={answer}
        answerSubmit={this.submitAnswer}
        isPhotoBtnShowing={PhotoReport}
        onInfoPress={this.toggleInfo}
        onClearPress={this.onClear}
        onPhotoPress={this.openCamera}
      />
    );
  }

  renderIntegerType = (PhotoReport, answer) => {
    return (
      <NumericKeyboard
        keyboardType="integer"
        btnSize={BTN_SIZE}
        paddingHorizontal={PADDING_HORIZONTAL}
        answer={answer}
        answerSubmit={this.submitAnswer}
        isPhotoBtnShowing={PhotoReport}
        onInfoPress={this.toggleInfo}
        onClearPress={this.onClear}
        onPhotoPress={this.openCamera}
      />
    );
  }

  renderBarcodeType = (PhotoReport, answer) => {
    return (
      <NumericKeyboard
        keyboardType="barcode"
        btnSize={BTN_SIZE}
        paddingHorizontal={PADDING_HORIZONTAL}
        answer={answer}
        answerSubmit={this.submitAnswer}
        isPhotoBtnShowing={PhotoReport}
        onInfoPress={this.toggleInfo}
        onClearPress={this.onClear}
        onPhotoPress={this.openCamera}
      />
    );
  }

  renderKeyboard = () => {
    const {
      question: { AnswerFormatID, PhotoReport },
      answer,
    } = this.props.active;
    const keybaordHeight = height - this.props.viewHeight - 60;


    const keyHeight = keybaordHeight / 2 - 12;
    const keyWidth = ((width - ((width * 18) / 100)) - 10) / 7;
    switch (AnswerFormatID) {
      case '1':
        return this.renderBooleanType(PhotoReport);
      case '2':
        return this.renderCalendarType(PhotoReport);
      case '3':
        return this.renderCalculatorType(PhotoReport, answer);
      case '4':
        return this.renderTextType(PhotoReport);
      case '5':
        return this.renderFractionalType(PhotoReport, answer, keyHeight, keyWidth);
      case '6':
        return this.renderIntegerType(PhotoReport, answer, keyHeight, keyWidth);
      case '7':
        return this.renderBarcodeType(PhotoReport, answer, keyHeight, keyWidth);
      case '8':
        return this.renderDropdownType(PhotoReport);
      default:
        return ('default');
    }
  }

  render() {
    // let isColumn = false;
    // let leftIconsFlex = 1;
    // let iconHeight = 44;
    // let iconWidth = 44;
    const {
      calendarModal,
      calendarDate,
      textModal,
      calculatorModal,
      dropdownModal,
      infoModal,
    } = this.state;

    const {
      active: {
        question: { AnswerFormatID, PhotoReport, AnswerRecommend, Name },
        answer,
      },
      onPressArrow,
      viewHeight,
    } = this.props;
    const keyboardContainerHeight = height - viewHeight - 60;
    const arrowSpace = (bottomSpace / 2);
    const arrowButtonHeight = keyboardContainerHeight / 2;
    const upArrowHeight = arrowButtonHeight - arrowSpace;
    const downArrowHeight = arrowButtonHeight + arrowSpace;
    // const { onPressArrow, categoryName } = this.props;
    // if (['3', '5', '6', '7'].includes(AnswerFormatID)) {
    //   isColumn = true;
    //   iconHeight = 36;
    //   iconWidth = 36;
    // }
    // if (['2', '4', '8'].includes(AnswerFormatID)) {
    //   leftIconsFlex = 0.5;
    // }
    // const {
    //   container,
    //   leftContainer,
    //   left1Container,
    //   rightContainer,
    //   topArrowContainer,
    //   bottomArrowContainer,
    // } = styles;
    return (
      <View style={[styles.container, { height: keyboardContainerHeight }]}>
        <View style={styles.leftContainer}>
          {this.renderKeyboard()}
        </View>
        <View style={styles.rightContainer}>
          <TouchableOpacity
            hitSlop={{
              top: 20, bottom: 0, left: 3, right: 20,
            }}
            style={[styles.topArrowContainer, { height: upArrowHeight }]}
            onPress={() => onPressArrow('up')}
          >
            <Image source={upPng} />
          </TouchableOpacity>
          <TouchableOpacity
            hitSlop={{
              top: 0, bottom: 20, left: 3, right: 20,
            }}
            style={[styles.bottomArrowContainer, { height: downArrowHeight, paddingBottom: arrowSpace }]}
            onPress={() => onPressArrow('down')}
          >
            <Image source={downPng} />
          </TouchableOpacity>
        </View>
        <Modal
          isVisible={calendarModal}
        >
          <CalendarModal
            date={answer}
            onCancel={this.toggleCalendar}
            onSubmit={(date) => {
              this.submitAnswer(date);
              this.toggleCalendar();
            }}
          />
        </Modal>
        <Modal
          isVisible={textModal}
          onRequestClose={this.toggleTextModal}
          avoidKeyboard
          onBackdropPress={this.toggleTextModal}
        >
          <TextModal
            onCancel={this.toggleTextModal}
            onSubmit={(text) => {
              this.submitAnswer(text);
              this.toggleTextModal();
            }}
            value={answer}
            title={Name}
            maxLength={100}
          />
        </Modal>
        <Modal
          isVisible={calculatorModal}
          avoidKeyboard
          onRequestClose={this.toggleCalculatorModal}
          onBackdropPress={this.toggleCalculatorModal}
        >
          <CalculatorModal
            onCancel={this.toggleCalculatorModal}
            onSubmit={(text) => {
              this.submitAnswer(text);
              this.toggleCalculatorModal();
            }}
            title={Name}
          />
        </Modal>
        {AnswerFormatID === '8' && (
          <DropdownModal
            value={answer}
            isVisible={dropdownModal}
            onCancel={this.toggleDropdownModal}
            onSubmit={this.onDropdownSubmit}
            options={AnswerRecommend !== null ? AnswerRecommend.split(';') : []}
            extraOptionText="Ввести свой ответ"
            title={Name}
            maxLength={100}
          />
        )}
        <InfoModal
          isVisible={this.state.infoModal}
          data={this.props.active}
          onClick={this.toggleInfo}
          onRemovePhoto={this.removePhoto}
          categoryName={this.state.categoryName}
        />
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'rgb(95,95,209)',
  },
  leftContainer: {
    width: LEFT_BLOCK_WIDTH,
    // alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: bottomSpace,
  },
  leftinnerMain: {
    flex: 1,
    flexDirection: 'row',
  },
  leftinnerMainIn: {
    flex: 1,
    justifyContent: 'space-evenly',
  },
  left1Container: {
    justifyContent: 'space-around',
  },
  yesNoContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
  },
  rightContainer: {
    width: RIGHT_BLOCK_WIDTH,
    justifyContent: 'space-evenly',
    borderLeftWidth: 1,
    borderColor: '#fff',
  },
  booleanIconsContainer: {
    height: 60,
    width: 60,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
    borderColor: '#fff',
    borderWidth: 2,
    marginRight: 7,
  },
  topArrowContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderColor: '#fff',
  },
  bottomArrowContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  numberContainer: {
    // height: 36,
    // width: 36,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 18,
    borderColor: '#fff',
    borderWidth: 2,
    marginHorizontal: 2,
    // marginTop: 8,
  },
  textStyle: {
    color: '#fff',
    fontSize: 15,
  },
  centerNumberContainer: {
    flexDirection: 'row',
    // position: 'relative',
    // justifyContent: 'space-evenly',
  },
});

export default Keyboard;
