import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Animated,
  Easing,
  Text,
  Modal,
} from 'react-native';
import loaderPng from '../../img/group8.png';

const { height, width } = Dimensions.get('window');

class Loader extends React.Component {
  spinValue = new Animated.Value(0)

  componentWillMount = () => {
    Animated.loop(Animated.timing(
      this.spinValue,
      {
        toValue: 1,
        duration: 3000,
        easing: Easing.linear,
        useNativeDriver: true,
      },
    )).start();
  }


  render() {
    const {
      mainArea,
      bodyArea,
      bottomContainer,
      bottomLinksText,
    } = styles;
    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg'],
    });
    const { navigateToFedbackScreen, displayFeedBackLink } = this.props;
    return (
      <Modal
        transparent
        animationType="slide"
      >
        <View style={mainArea}>
          <View style={bodyArea}>
            <Animated.Image style={{ transform: [{ rotate: spin }] }} source={loaderPng} />
          </View>
          {displayFeedBackLink && (
          <View style={bottomContainer}>
            <Text style={bottomLinksText} onPress={navigateToFedbackScreen}>
                Связаться с нами
            </Text>
          </View>
          )}
        </View>
      </Modal>
    );
  }
}
const styles = StyleSheet.create({
  mainArea: {
    backgroundColor: 'rgba(236, 236, 239, 0.95)',
    height,
    width,
    justifyContent: 'center',
  },
  bodyArea: {
    height: height * 0.8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bottomContainer: {
    height: height * 0.2,
    alignSelf: 'center',
    alignItems: 'center',
  },
  bottomLinksText: {
    paddingBottom: 20,
    color: 'rgb(5,6,51)',
    textDecorationLine: 'underline',
  },
});
export default Loader;
