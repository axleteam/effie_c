import React, { Component } from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';

import KeyboardButton from './KeyboardButton';

import iPng from '../img/iCopy.png';
import cameraPng from '../img/photo.png';
import closePng from '../img/x.png';
import dotPng from '../img/dot.png';
import calculatorPng from '../img/kal.png';
import NumericButtons from './NumericButtons';

class NumbericKeyboard extends Component {

  onDotPress = () => {
    const { answer, answerSubmit } = this.props;
    answerSubmit(answer ? `${answer}.` : '0.');
  }

  onNumPress = (btnNum) => {
    const { answer, answerSubmit } = this.props;
    answerSubmit(answer ? `${answer}${btnNum}` : btnNum);
  }

  renderNumberKeyboardButtons = (btnSize, paddingHorizontal) => {
    return (
      <NumericButtons
        btnSize={btnSize}
        paddingHorizontal={paddingHorizontal}
        onNumPress={this.onNumPress}
      />
    );
  }

  renderDecimalActionButton = (btnSize) => {
    return (
      <KeyboardButton
        style={{ height: btnSize, width: btnSize }}
        source={dotPng}
        onPress={this.onDotPress}
      />
    );
  }

  renderCalculatorActionButton = (btnSize) => {
    const { onCalculatorPress } = this.props;
    return (
      <KeyboardButton
        style={{ height: btnSize, width: btnSize }}
        source={calculatorPng}
        onPress={onCalculatorPress}
      />
    )
  }

  renderActionButton = (keyboardType, btnSize) => {
    switch (keyboardType) {
      case 'barcode':
      case 'integer':
        return null;
      case 'decimal':
        return this.renderDecimalActionButton(btnSize);
      case 'calculator':
        return this.renderCalculatorActionButton(btnSize);
      default:
        return null;
    }
  }

  render() {
    const {
      btnSize,
      paddingHorizontal,
      isPhotoBtnShowing,
      onInfoPress,
      onClearPress,
      onPhotoPress,
      keyboardType,
    } = this.props;
    const {
      container,
    } = styles;
    return (
      <View style={styles.leftinnerMain}>
        <View style={styles.leftinnerMainIn}>
          <View style={{ height: btnSize, width: btnSize }}>
            {isPhotoBtnShowing && (
              <KeyboardButton
                style={{ height: btnSize, width: btnSize }}
                source={cameraPng}
                onPress={onPhotoPress}
              />
            )}
          </View>
          <View style={{ height: btnSize, width: btnSize }}>
            <KeyboardButton
              style={{ height: btnSize, width: btnSize }}
              source={closePng}
              onPress={onClearPress}
            />
          </View>
        </View>
        <View>
          {this.renderNumberKeyboardButtons(btnSize, paddingHorizontal)}
        </View>
        <View style={styles.leftinnerMainIn}>
          <View style={{ height: btnSize, width: btnSize, marginHorizontal: paddingHorizontal }}>
            {this.renderActionButton(keyboardType, btnSize)}
          </View>
          <View style={{ height: btnSize, width: btnSize }}>
            <KeyboardButton
              style={{ height: btnSize, width: btnSize }}
              source={iPng}
              onPress={onInfoPress}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  leftinnerMain: {
    flex: 1,
    flexDirection: 'row',
  },
  leftinnerMainIn: {
    flex: 1,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  centerNumberContainer: {
    flexDirection: 'row',
  },
});

export default NumbericKeyboard;
