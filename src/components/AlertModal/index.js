import React from 'react';
import {
  View, Text, Dimensions, StyleSheet,
} from 'react-native';
import { Button } from 'native-base';
import PropTypes from 'prop-types';
import Modal from 'react-native-modal';
import ModalTitle from '../ModalTitle';

const { height } = Dimensions.get('window');

class AlertModal extends React.PureComponent {
  render() {
    const {
      onClick,
      visible,
      title,
      message,
      leftButtonText,
      rightButtonText,
      leftClick,
      largeButtons,
      subMessage,
      middleClick,
      middleButtonText,
    } = this.props;
    let buttonClass = styles.buttonContainer;
    if (largeButtons) {
      buttonClass = styles.buttonLargeContainer;
    }

    return (
      <Modal
        isVisible={visible}
      >
        <View style={styles.modalContainer}>
          <ModalTitle title={title} />
          <View style={styles.messageContainer}>
            <Text style={styles.textArea}>
              {message}
            </Text>
            {subMessage && (
            <Text style={styles.subMessage}>
              {subMessage}
            </Text>
            )}
          </View>
          <View style={{ justifyContent: 'space-evenly', flexDirection: largeButtons ? 'column' : 'row' }}>
            {
          leftButtonText && (
          <Button
            style={buttonClass}
            onPress={leftClick}
            block
          >
            <Text style={styles.buttonText}>
              {leftButtonText}
            </Text>
          </Button>
          )
                      }
            {
          middleButtonText && (
          <Button
            style={buttonClass}
            onPress={middleClick}
            block
          >
            <Text style={styles.buttonText}>
              {middleButtonText}
            </Text>
          </Button>
          )
                      }
            <Button
              style={buttonClass}
              onPress={onClick}
              block
            >
              <Text style={styles.buttonText}>
                {rightButtonText}
              </Text>
            </Button>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    marginHorizontal: 20,
    justifyContent: 'center',
    backgroundColor: 'rgb(255, 255, 255)',
    borderRadius: 8,
  },
  textArea: {
    textAlign: 'center',
    paddingTop: 30,
    paddingBottom: 30,
  },
  subMessage: {
    alignSelf: 'center',
  },
  buttonText: {
    color: 'rgb(255, 255, 255)',
  },
  buttonContainer: {
    padding: 10,
    marginBottom: 20,
    width: 130,
    alignSelf: 'center',
    backgroundColor: 'rgb(95, 95, 209)',
  },
  buttonLargeContainer: {
    padding: 10,
    marginBottom: 10,
    alignSelf: 'center',
    width: '90%',
    backgroundColor: 'rgb(95, 95, 209)',
  },
  messageContainer: {
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
});

AlertModal.propTypes = {
  onClick: PropTypes.func,
  visible: PropTypes.bool,
  title: PropTypes.string,
  message: PropTypes.string,
  leftButtonText: PropTypes.string,
  rightButtonText: PropTypes.string,
  leftClick: PropTypes.func,
  largeButtons: PropTypes.bool,
  subMessage: PropTypes.any,
  middleButtonText: PropTypes.any,
  middleClick: PropTypes.any,
};

AlertModal.defaultProps = {
  leftButtonText: null,
  rightButtonText: 'Ок',
  largeButtons: false,
};

export default AlertModal;
