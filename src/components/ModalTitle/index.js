import React from 'react';
import {
  View, Text, StyleSheet,
} from 'react-native';

const ModalTitle = ({ title }) => (
  <View style={style.titleArea}>
    <Text style={style.modalTitle}>
      {title}
    </Text>
  </View>
);


const style = StyleSheet.create({
  titleArea: {
    backgroundColor: 'rgb(95,95,209)',
    padding: 16,
    borderTopRightRadius: 8,
    borderTopLeftRadius: 8,
  },
  modalTitle: {
    color: '#fff',
    fontSize: 17,
    textAlign: 'center',
  },
});

export default ModalTitle;
