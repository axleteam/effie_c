import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import PropTypes from 'prop-types';
import { TouchableOpacity, Image, StatusBar } from 'react-native';
import {
  Header, View, Button, Text,
} from 'native-base';
import backButtonPng from '../../img/pinLeft.png';

const MyHeader = ({
  navigation, title, backButton, backGoto, rightIcon, onRightIconClick,
}) => (
  <Header style={styles.headerStyle}>
    <StatusBar hidden />
    <View style={styles.sideButtonStyle}>
      {backButton ? (
        <Button
          transparent
          onPress={backGoto}
        >
          <Image source={backButtonPng} />
        </Button>
      ) : (
        <Button transparent onPress={() => navigation.toggleDrawer()}>
          <Icon name="menu" style={styles.iconStyle} size={35} />
        </Button>
      )}

    </View>
    <View style={styles.titleContainer}>
      <Text style={styles.titleStyle} numberOfLines={1}>
        {title}
      </Text>
    </View>
    <View style={styles.sideButtonStyle}>
      {rightIcon && (
      <TouchableOpacity onPress={onRightIconClick}>
        <Icon name={rightIcon} style={styles.iconStyle} size={30} />
      </TouchableOpacity>
      )}

    </View>
  </Header>
);

const styles = {
  headerStyle: {
    height: 60,
    backgroundColor: 'rgb(95,95,209)',
  },
  iconStyle: {
    color: '#fff',
  },
  titleStyle: {
    color: '#fff',
    fontSize: 17,
    fontWeight: 'bold',
  },
  titleContainer: {
    flex: 80,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 7,
  },
  sideButtonStyle: {
    flex: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
};

MyHeader.propTypes = {
  navigation: PropTypes.object,
  title: PropTypes.string,
  backButton: PropTypes.bool,
  backGoto: PropTypes.func,
  rightIcon: PropTypes.any,
  onRightIconClick: PropTypes.func,
};

MyHeader.defaultProps = {
  backButton: false,
  backGoto: () => {},
  rightIcon: null,
  onRightIconClick: () => {},
};

export default MyHeader;
