import React from 'react';
import { View, CheckBox, Text } from 'native-base';

const EfCheckbox = ({ text, onChange, value }) => (
  <View style={styles.container}>
    <CheckBox
      checked={value}
      style={[styles.checkboxStyle, { backgroundColor: value ? 'rgb(95,95,209)' : '#fff' }]}
      onPress={onChange}
    />
    <Text style={styles.textStyle} onPress={onChange}>
      {text}
    </Text>
  </View>
);

const styles = {
  container: {
    flexDirection: 'row',
  },
  textStyle: {
    marginLeft: 10,
    fontSize: 14,
    alignSelf: 'center',
    color: 'rgb(133,133,133)',
  },
  checkboxStyle: {
    borderRadius: 5,
    borderColor: 'rgb(151,151,151)',
    left: 0,
  },
};

export default EfCheckbox;
