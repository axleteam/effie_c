import React, { Component } from 'react';
import { TextField } from 'react-native-material-textfield';
import { Image, TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';
import errorImg from '../../img/shape.png';
import eyeImg from '../../img/eye.png';
import eyeClosedImg from '../../img/eyeClosed.png';

class EfInput extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isSecret: props.secret,
    };
  }

  changeIsSecret = () => this.setState((prevState => ({ isSecret: !prevState.isSecret })))

  renderAccesory = () => {
    if (this.props.secret) {
      return (
        <TouchableWithoutFeedback onPress={this.changeIsSecret}>
          <Image
            source={this.state.isSecret ? eyeClosedImg : eyeImg}
            style={{ height: 16, width: 20 }}
            resizeMode="contain"
          />
        </TouchableWithoutFeedback>
      );
    }
    if (!this.props.error) return null;
    return (
      <Image
        source={errorImg}
        style={{ height: 16, width: 16 }}
      />
    );
  }

  render() {
    const {
      placeholder,
      onChange,
      name,
      value,
      autoCapitalize,
      keyboardType,
      returnKeyType,
      onRef,
      onSubmitEditing,
      blurOnSubmit,
      multiline
    } = this.props;
    const { isSecret } = this.state;
    return (
      <TextField
        ref={onRef}
        label={placeholder}
        onChangeText={v => onChange(name, v)}
        tintColor="rgb(95,95,209)"
        renderAccessory={this.renderAccesory}
        inputContainerPadding={2}
        secureTextEntry={isSecret}
        value={value}
        autoCapitalize={autoCapitalize}
        keyboardType={keyboardType}
        returnKeyType={returnKeyType}
        onSubmitEditing={onSubmitEditing}
        blurOnSubmit={blurOnSubmit}
        multiline={multiline}
      />
    );
  }
}

EfInput.defalutProps = {
  secret: false,
  autoCapitalize: 'sentences',
  onChange: () => { },
  onClickIcon: () => { },
  isSecret: false,
  error: null,
  keyboardType: 'default',
};

EfInput.propTypes = {
  placeholder: PropTypes.string,
  autoCapitalize: PropTypes.string,
  secret: PropTypes.bool,
  onChange: PropTypes.func,
  name: PropTypes.string,
  error: PropTypes.any,
  value: PropTypes.any,
  keyboardType: PropTypes.string,
};

export default EfInput;
