import React from 'react';
import {
  View, Text, Dimensions, StyleSheet, TouchableOpacity, KeyboardAvoidingView, ScrollView, TextInput,
} from 'react-native';
import {
  Button,
} from 'native-base';
import PropTypes from 'prop-types';
import { TextField } from 'react-native-material-textfield';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const { height } = Dimensions.get('window');

class CalculatorModal extends React.Component {
  state = {
    byValue: '',
    totalValue: '',
    percentage: '',
  }

  onSubmit = () => {
    const { percentage } = this.state;
    const { onSubmit } = this.props;
    if (!percentage || percentage === '') return onSubmit(null);
    onSubmit(this.state.percentage.replace('%', ''));
  }


  getSummarisedInteger = str => parseInt(
    str.split('+').filter(a => a).reduce((a, b) => Number(a) + Number(b), 0),
  )

  addPlus = (field) => {
    this.setState(state => ({ [field]: `${state[field]}+` }));
  }

  calculate = (field, value) => {
    this.setState({ [field]: value }, () => {
      const { byValue, totalValue } = this.state;
      const byValueFormatted = this.getSummarisedInteger(byValue);
      const totalValueFormatted = this.getSummarisedInteger(totalValue);
      const percentage = Math.ceil(totalValueFormatted === 0 ? 0 : byValueFormatted / totalValueFormatted * 100);
      const percentageStr = percentage !== '' ? `${percentage}%` : '';
      this.setState(() => ({ percentage: percentageStr }));
    });
  }

  render() {
    const { onCancel, onSubmit, title } = this.props;
    const { byValue, totalValue, percentage } = this.state;
    const { inputContainer } = styles;
    return (
      <View>
        <ScrollView contentContainerStyle={styles.modalContainer} keyboardShouldPersistTaps="always" keyboardDismissMode="none">
          <View style={styles.modalHeader}>
            <Text style={{ color: '#fff', fontSize: 17 }}>
              {title}
            </Text>
          </View>
          <View style={{ paddingVertical: 10, paddingHorizontal: 10 }}>
            <View style={inputContainer}>
              <TextField
                label="Компания"
                tintColor="rgb(95,95,209)"
                value={totalValue}
                keyboardType="numeric"
                onChangeText={value => this.calculate('totalValue', value)}
                renderAccessory={() => (
                  <TouchableOpacity onPress={() => this.addPlus('totalValue')}>
                    <Icon
                      name="plus-circle-outline"
                      size={25}
                    />
                  </TouchableOpacity>
                )}
              />
            </View>
            <View style={inputContainer}>
              <TextField
                label="Товар занимает"
                tintColor="rgb(95,95,209)"
                value={byValue}
                keyboardType="numeric"
                onChangeText={value => this.calculate('byValue', value)}
                renderAccessory={() => (
                  <TouchableOpacity onPress={() => this.addPlus('byValue')}>
                    <Icon
                      name="plus-circle-outline"
                      size={25}
                    />
                  </TouchableOpacity>
                )}
              />
            </View>
            <View style={inputContainer}>
              <TextField
                label="Доля полки"
                tintColor="rgb(95,95,209)"
                disabled
                value={percentage.toString()}
              />
            </View>
          </View>
          <View style={styles.buttonsContainer}>
            <Button onPress={onCancel} style={styles.calenderButton}>
              <Text style={styles.buttonText}>
                Отмена
              </Text>
            </Button>
            <Button
              style={styles.calenderButton}
              onPress={this.onSubmit}
            >
              <Text style={styles.buttonText}>
                Ок
              </Text>
            </Button>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  calenderButton: {
    flex: 0.45,
    backgroundColor: 'rgb(95,95,209)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
  },
  modalContainer: {
    marginHorizontal: 20,
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderRadius: 8,
  },
  modalHeader: {
    backgroundColor: 'rgb(95,95,209)',
    padding: 16,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  inputContainer: {
    marginBottom: 8,
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 20,
  },
});

CalculatorModal.propTypes = {
  onCancel: PropTypes.func,
  onSubmit: PropTypes.func,
};

export default CalculatorModal;
