import React, { PureComponent } from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';

import KeyboardButton from './KeyboardButton';

import number0Png from '../img/number0.png';
import number1Png from '../img/number1.png';
import number2Png from '../img/number2.png';
import number3Png from '../img/number3.png';
import number4Png from '../img/number4.png';
import number5Png from '../img/number5.png';
import number6Png from '../img/number6.png';
import number7Png from '../img/number7.png';
import number8Png from '../img/number8.png';
import number9Png from '../img/number9.png';

const numberImages = [
  number0Png,
  number1Png,
  number2Png,
  number3Png,
  number4Png,
  number5Png,
  number6Png,
  number7Png,
  number8Png,
  number9Png,
];

class NumericButtons extends PureComponent {

  render() {
    console.log('NumericButtons render: ');
    const { onNumPress, btnSize, paddingHorizontal } = this.props;
    const lis1 = [];
    const lis2 = [];
    for (let i = 0; i < 5; i++) {
      lis1.push(
        <View key={i} style={{ height: btnSize, width: btnSize, marginHorizontal: paddingHorizontal }}>
          <KeyboardButton
            style={{ height: btnSize, width: btnSize }}
            source={numberImages[i]}
            onPress={() => onNumPress(i)}
          />
        </View>,
      );
    }
    for (let j = 5; j < 10; j++) {
      lis2.push(
        <View key={j} style={{ height: btnSize, width: btnSize, marginHorizontal: paddingHorizontal }}>
          <KeyboardButton
            style={{ height: btnSize, width: btnSize }}
            source={numberImages[j]}
            onPress={() => onNumPress(j)}
          />
        </View>,
      );
    }
    return (
      <View style={{ flex: 1, justifyContent: 'space-evenly' }}>
        <View style={styles.centerNumberContainer}>
          {lis1}
        </View>
        <View style={styles.centerNumberContainer}>
          {lis2}
        </View>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  centerNumberContainer: {
    flexDirection: 'row',
  },
});


export default NumericButtons;
