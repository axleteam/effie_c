import React, { Component } from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';

import yesPng from '../img/yes.png';
import noPng from '../img/no.png';
import iPng from '../img/iCopy.png';
import cameraPng from '../img/photo.png';
import closePng from '../img/x.png';
import calendarPng from '../img/calendar.png';
import textPng from '../img/text.png';
import dropDownpng from '../img/dropdown.png';

import KeyboardButton from './KeyboardButton';

class NonNumbericKeyboard extends Component {

  onYesPress = () => this.props.answerSubmit('Да');

  onNoPress = () => this.props.answerSubmit('Нет');

  renderBooleanType = () => {
    const { mainButtonsContainer } = styles;
    const {
      bigButtonSize,
      padding,
    } = this.props;
    return (
      <View style={mainButtonsContainer}>
        <View style={{ width: bigButtonSize, marginHorizontal: padding }}>
          <KeyboardButton
            style={{ height: bigButtonSize, width: bigButtonSize }}
            source={yesPng}
            onPress={this.onYesPress}
          />
        </View>
        <View style={{ width: bigButtonSize, marginHorizontal: padding }}>
          <KeyboardButton
            style={{ height: bigButtonSize, width: bigButtonSize }}
            source={noPng}
            onPress={this.onNoPress}
          />
        </View>
      </View>
    )
  }

  renderCalendarType = () => {
    const { mainButtonsContainer } = styles;
    const {
      bigButtonSize,
      padding,
      onCalendarPress,
    } = this.props;
    return (
      <View style={mainButtonsContainer}>
        <View style={{ width: bigButtonSize, marginHorizontal: padding }} />
        <View style={{ width: bigButtonSize, marginHorizontal: padding }}>
          <KeyboardButton
            style={{ height: bigButtonSize, width: bigButtonSize }}
            source={calendarPng}
            onPress={onCalendarPress}
          />
        </View>
      </View>
    )
  }

  renderDropdownType = () => {
    const { mainButtonsContainer } = styles;
    const {
      bigButtonSize,
      padding,
      onDropdownPress,
    } = this.props;
    return (
      <View style={mainButtonsContainer}>
        <View style={{ width: bigButtonSize, marginHorizontal: padding }} />
        <View style={{ width: bigButtonSize, marginHorizontal: padding }}>
          <KeyboardButton
            style={{ height: bigButtonSize, width: bigButtonSize }}
            source={dropDownpng}
            onPress={onDropdownPress}
          />
        </View>
      </View>
    )
  }

  renderTextType = () => {
    const { mainButtonsContainer } = styles;
    const {
      bigButtonSize,
      padding,
      onTextPress,
    } = this.props;
    return (
      <View style={mainButtonsContainer}>
        <View style={{ width: bigButtonSize, marginHorizontal: padding }} />
        <View style={{ width: bigButtonSize, marginHorizontal: padding }}>
          <KeyboardButton
            style={{ height: bigButtonSize, width: bigButtonSize }}
            source={textPng}
            onPress={onTextPress}
          />
        </View>
      </View>
    )
  }

  renderKeyboard = (keyboardType) => {
    switch (keyboardType) {
      case 'calendar':
        return this.renderCalendarType();
      case 'text':
        return this.renderTextType();
      case 'dropdown':
        return this.renderDropdownType();
      case 'boolean':
        return this.renderBooleanType();
      default:
        return null;
    }
  }

  render() {
    const {
      smallButtonSize,
      padding,
      isPhotoBtnShowing,
      onInfoPress,
      onClearPress,
      onPhotoPress,
      keyboardType,
    } = this.props;
    const {
      container,
    } = styles;
    return (
      <View style={container}>
        <View style={{ width: smallButtonSize, marginHorizontal: padding }}>
          <KeyboardButton
            style={{ height: smallButtonSize, width: smallButtonSize }}
            source={closePng}
            onPress={onClearPress}
          />
        </View>
        <View style={{ width: smallButtonSize, marginHorizontal: padding }}>
          {isPhotoBtnShowing && (
            <KeyboardButton
              style={{ height: smallButtonSize, width: smallButtonSize }}
              source={cameraPng}
              onPress={onPhotoPress}
            />
          )}
        </View>
        {this.renderKeyboard(keyboardType)}
        <View style={{ width: smallButtonSize, marginHorizontal: padding }}>
          <KeyboardButton
            style={{ height: smallButtonSize, width: smallButtonSize }}
            source={iPng}
            onPress={onInfoPress}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainButtonsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default NonNumbericKeyboard;
