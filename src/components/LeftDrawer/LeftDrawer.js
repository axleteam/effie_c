import React from 'react';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
} from 'react-native';
import {
  ListItem, Text, Left, Body, Button,
} from 'native-base';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  ROUTE_FINISH,
} from '../../store/workingDay/constants';
import infoImage from '../../img/info.png';
import messageImage from '../../img/message.png';
import refreshImage from '../../img/loadingArrows.png';
import logoEffieSmall from '../../img/logoEffieSmall.png';
import AlertModal from '../AlertModal';

const { height } = Dimensions.get('window');

class LeftDrawer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      timer: '00:00:00',
      alert: false,
    };
    this.timer = setInterval(this.tick, 1000);
  }

  componentWillUnmount = () => {
    clearInterval(this.timer);
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.phase === 'INIT') {
      this.props.navigation.navigate('Login');
    }
  }

  tick = () => {
    const { workingDayStore } = this.props;
    const timer = workingDayStore.isWorkingTimeTimerShowing ? moment.utc(moment.duration('9:00:00').subtract(moment().diff(moment(workingDayStore.dayBegDatetime), 'milliseconds'), 'milliseconds').asMilliseconds()).format('HH:mm:ss') : '';
    this.setState({
      timer,
    });
  }

  navigateToSynchronizationFromUnfinishedWorkAlert = () => {
    this.navigateToSynchronizationScreen(true);
  }

  navigateToSynchronizationScreen = async (isVisitRedirect = false) => {
    const { navigation } = this.props;
    if (isVisitRedirect) {
      // await setWorkingDayStage(ROUTE_FINISH);
      this.setState({ isUnfinishedWorkAlertShowing: false });
    }
    navigation.navigate('Syncronization', { fromRoute: 'LEFT_SYNC' });
  }

  navigateToAboutUsScreen = () => this.props.navigation.navigate('AboutUs');

  navigateToFedbackScreen = () => {
    const { navigation } = this.props;
    navigation.navigate('Feedback', { previousRoute: 'RouteScreen' });
  }

  onLogout = () => {
    const {
      visitsToSend,
    } = this.props;
    if (visitsToSend.length > 0) {
      this.setState({
        isUnfinishedWorkAlertShowing: true,
      });
    } else {
      this.logOut();
    }
  }

  logOut = () => {
    const {
      logoutUser,
      clearVariables,
      clearfeedBackVariables,
      clearserverDataVariables,
      clearworkingDayVariables,
    } = this.props;
    clearVariables();
    clearfeedBackVariables();
    clearserverDataVariables();
    clearworkingDayVariables();
    logoutUser();
    this.props.navigation.navigate('Login');
  }

  closeAlert = () => {
    this.setState({
      isUnfinishedWorkAlertShowing: false,
    });
  }

  render() {
    const { user, workingDayStore } = this.props;
    const {
      hederArea,
      bodyArea,
      logoStyle,
      fontView,
      bottomText,
      fontStyle,
      userText,
      userView,
      bodyText,
      listArea,
      headerArea,
      headView,
    } = styles;
    const { isUnfinishedWorkAlertShowing } = this.state;
    const messageIcon = (<Image style={{ height: 35 }} source={messageImage} resizeMode="contain" />);
    const refreshIcon = (<Image style={{ height: 30 }} source={refreshImage} resizeMode="contain" />);
    const logoutIcon = (<Icon name="logout" size={26} color="rgb(51, 51, 153)" />);
    const infoIcon = (<Image style={{ height: 30 }} source={infoImage} resizeMode="contain" />);
    return (
      <View style={{ flex: 1 }}>
        <View style={headerArea}>
          <View style={hederArea}>
            <View style={headView}>
              <View style={{ flexDirection: 'row' }}>
                <Image source={logoEffieSmall} style={logoStyle} resizeMode="contain" />
              </View>
              <View style={userView}>
                <Text style={userText}>
                  { user.userFIO }
                </Text>
              </View>
              <View style={fontView}>
                <Text style={fontStyle}>
                  { user.userName }
                </Text>
              </View>
              {workingDayStore.isWorkingTimeTimerShowing && (
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <Text style={bottomText}>
                  Время рабочего деня:
                  {` ${this.state.timer} `}
                </Text>
              </View>
              )}
            </View>
          </View>
        </View>
        <View style={bodyArea}>
          {/* <ListItem onPress={() => this.pressRow('Карта')} icon noBorder>
            <Left>
              <Button transparent>
                {locationIcon}
              </Button>
            </Left>
            <Body style={{ itemOne }}>
              <Text style={bodyText}>
                Карта
              </Text>
            </Body>
          </ListItem> */}
          <ListItem onPress={this.navigateToFedbackScreen} style={listArea} icon noBorder>
            <Left>
              <Button transparent>
                {messageIcon}
              </Button>
            </Left>
            <Body>
              <Text style={bodyText}>
                Обратная связь
              </Text>
            </Body>
          </ListItem>
          <ListItem onPress={this.navigateToSynchronizationScreen} style={listArea} icon noBorder>
            <Left>
              <Button transparent>
                {refreshIcon}
              </Button>
            </Left>
            <Body>
              <Text style={bodyText}>
                Синхронизация
              </Text>
            </Body>
          </ListItem>
          <ListItem onPress={this.navigateToAboutUsScreen} style={listArea} icon noBorder>
            <Left>
              <Button transparent>
                {infoIcon}
              </Button>
            </Left>
            <Body>
              <Text style={bodyText}>
                О программе
              </Text>
            </Body>
          </ListItem>
          <ListItem onPress={this.onLogout} style={listArea} icon noBorder>
            <Left>
              <Button transparent>
                {logoutIcon}
              </Button>
            </Left>
            <Body>
              <Text style={bodyText}>
                Выход
              </Text>
            </Body>
          </ListItem>
        </View>
        <AlertModal
          title="Data is not stored"
          message="There are unsent visits. Are you sure you want to logout?"
          visible={isUnfinishedWorkAlertShowing}
          leftClick={this.navigateToSynchronizationFromUnfinishedWorkAlert}
          leftButtonText="Go to Synchronisation screen"
          middleButtonText="Logtout"
          middleClick={this.logOut}
          rightButtonText="Cancel"
          onClick={this.closeAlert}
          largeButtons
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  hederArea: {
    flexDirection: 'row',
    height: height / 4,
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  bodyArea: {
    flex: 1,
    flexDirection: 'column',
    paddingVertical: 20,
  },
  fontView: {
    flex: 1,
    flexDirection: 'row',
  },
  fontStyle: {
    fontSize: 13,
    letterSpacing: -0.4,
    color: 'rgb(255, 255, 255)',
  },
  userText: {
    fontSize: 17,
    letterSpacing: -0.4,
    color: 'rgb(255, 255, 255)',
    fontWeight: '600',
  },
  headerArea: {
    backgroundColor: 'rgb(95, 95, 209)',
  },
  bodyText: {
    fontSize: 17,
    color: 'rgb(133, 133, 133)',
  },
  userView: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 16,
  },
  logoStyle: {
    width: 100,
    height: 50,
  },
  headView: {
    flex: 1,
    flexDirection: 'column',
  },
  listArea: {
    paddingTop: 25,
    paddingBottom: 25,
    fontWeight: '500',
  },
  bottomText: {
    fontSize: 15,
    letterSpacing: -0.4,
    color: 'rgb(255, 255, 255)',
    marginTop: 5,
    fontWeight: '600',
  },
});
LeftDrawer.propTypes = {
  navigation: PropTypes.object,
  logoutUser: PropTypes.func,
  user: PropTypes.object,
  workingDayStore: PropTypes.object,
  clearVariables: PropTypes.func,
  clearfeedBackVariables: PropTypes.func,
  clearserverDataVariables: PropTypes.func,
  clearworkingDayVariables: PropTypes.func,
  setWorkingDayStage: PropTypes.func,
  visitsToSend: PropTypes.array,
};

export default LeftDrawer;
