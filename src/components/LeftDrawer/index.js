import { connect } from 'react-redux';
import LeftDrawer from './LeftDrawer';
import { logoutUser } from '../../store/user/duck';
import { clearVariables } from '../../store/currentRoute/duck';
import { clearfeedBackVariables } from '../../store/feedbackData/duck';
import { clearserverDataVariables } from '../../store/serverData/duck';
import { clearworkingDayVariables, setWorkingDayStage } from '../../store/workingDay/duck';

const mapStateToProps = ({
  userStore, workingDayStore, serverDataStore,
}) => ({
  user: userStore.user,
  workingDayStore,
  visitsToSend: serverDataStore.visitsToSend,
});

const mapDispatchToProps = {
  logoutUser,
  setWorkingDayStage,
  clearVariables,
  clearfeedBackVariables,
  clearserverDataVariables,
  clearworkingDayVariables,
};
export default connect(mapStateToProps, mapDispatchToProps)(LeftDrawer);
