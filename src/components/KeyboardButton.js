import React, { PureComponent } from 'react';
import {
  TouchableOpacity,
  Image,
} from 'react-native';

class KeyboardButton extends PureComponent {

  render() {
    const {
      onPress,
      style,
      source,
    } = this.props;
    return (
      <TouchableOpacity onPress={onPress}>
        <Image resizeMode="contain" style={style} source={source} />
      </TouchableOpacity>
    );
  }
}

export default KeyboardButton;
