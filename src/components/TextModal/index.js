import React from 'react';
import {
  View, Text, Dimensions, StyleSheet, KeyboardAvoidingView,
} from 'react-native';
import {
  Button, Item, Input, Label,
} from 'native-base';
import PropTypes from 'prop-types';

const { height } = Dimensions.get('window');

class TextModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      textValue: props.value || '',
    };
  }

  onTextValueChange = text => this.setState(() => ({ textValue: text }))

  render() {
    const {
      textInputContainer,
      textLengthContainer,
      textLengthTextStyle,
    } = styles;
    const { onCancel, onSubmit, title, maxLength } = this.props;
    const { textValue } = this.state;
    return (
      <View style={styles.modalContainer}>
        <View style={styles.modalHeaderStyle}>
          <Text style={{ color: '#fff' }}>
            {title}
          </Text>
        </View>
        <View style={textInputContainer}>
          <Item floatingLabel>
            <Label>
              Введите комментарий
            </Label>
            <Input
              onChangeText={this.onTextValueChange}
              value={textValue}
              multiline
              autoFocus
              maxLength={maxLength}
            />
          </Item>
        </View>
        {maxLength && (
          <View style={textLengthContainer}>
            <Text style={textLengthTextStyle}>{textValue.length}/{maxLength}</Text>
          </View>
        )}
        <View style={{ paddingVertical: 20, flexDirection: 'row', justifyContent: 'space-evenly' }}>
          <Button onPress={onCancel} style={styles.calenderButton}>
            <Text style={styles.buttonText}>
              Отмена
            </Text>
          </Button>
          <Button
            style={styles.calenderButton}
            onPress={() => onSubmit(textValue)}
          >
            <Text style={styles.buttonText}>
              Ок
            </Text>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  calenderButton: {
    flex: 0.45,
    backgroundColor: 'rgb(95,95,209)',
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
    flex: 1,
  },
  modalContainer: {
    marginHorizontal: 20,
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderRadius: 8,
    maxHeight: height * 0.45,
  },
  modalHeaderStyle: {
    backgroundColor: 'rgb(95,95,209)',
    padding: 20,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  textInputContainer: {
    marginTop: 10,
    paddingHorizontal: 10,
    maxHeight: height * 0.45 - 120, // 90 is height of cancel and ok buttons
  },
  textLengthContainer: {
    alignItems: 'flex-end',
    paddingHorizontal: 10,
    marginTop: 3,
  },
  textLengthTextStyle: {
    fontSize: 15,
    color: 'rgb(133, 133, 133)',
  },
});

TextModal.propTypes = {
  onCancel: PropTypes.func,
  onSubmit: PropTypes.func,
  value: PropTypes.any,
};

export default TextModal;
