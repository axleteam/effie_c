import React from 'react';
import {
  StyleSheet, TouchableOpacity, TouchableWithoutFeedback, Image,
} from 'react-native';
import { View, Text } from 'native-base';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  truncateDropdownRecommentAnswer,
  getRecommentDropdownAnswer,
  isAnswerEmpty,
} from '../../utils/methods';
import { checkIfQuestionAnswered } from '../../store/currentRoute/methods';
import { DATE_ANSWER_FORMAT } from '../../utils/constants';
import cameralistImg from '../../img/photoCamera.png';
import cameraActiveImg from '../../img/cameraActive.png';
import commentIcon from '../../img/comment.png';
import commentActiveIcon from '../../img/commentActive.png';

const ImagePicker = require('react-native-image-picker');

const recommendAnswerDateFormat = 'YYYY-MM-DD';

class QuestionItem extends React.Component {
  static propTypes = {
    question: PropTypes.object,
    active: PropTypes.any,
    onSelectQuestion: PropTypes.func,
    onAddImage: PropTypes.func,
    onCellPress: PropTypes.func,
  };

  shouldComponentUpdate(nextProps) {
    const {
      question,
      active,
    } = this.props;
    return active !== nextProps.active
    || question.question.ID !== nextProps.question.question.ID
      || question.answer !== nextProps.question.answer
      || question.comment !== nextProps.question.comment
      || question.photos !== nextProps.question.photos;
  }

  openCamera = async () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        path: 'effie',
      },
    };

    await this.props.onSelectQuestion();
    ImagePicker.launchCamera(options, (response) => {
      if (!response || response.error || response.didCancel) return;
      this.props.onAddImage(response.data);
    });
  }

  focusOnComment = async () => {
    await this.props.onSelectQuestion();
    this.props.focusOnComment();
  }

  onAnswerCellPress = async () => {
    const {
      question: {
        question:
              { AnswerFormatID },
        answer,
      },
      onAnswer,
      onCellPress,
      onSelectQuestion,
    } = this.props;
    await onSelectQuestion();
    if (AnswerFormatID === '1') {
      let newAnswer;
      switch (true) {
        case (isAnswerEmpty(answer)):
          newAnswer = 'Да';
          break;
        case (answer === 'Да'):
          newAnswer = 'Нет';
          break;
        case (answer === 'Нет'):
          newAnswer = null;
          break;
        default:
          newAnswer = null;
          break;
      }
      onAnswer(newAnswer);
    }
    onCellPress(AnswerFormatID);
  }

  getCellBackground = (recommentAnswer, actualAnswer, type) => {
    let cellBackGround = null;
    if (!isAnswerEmpty(recommentAnswer) && !isAnswerEmpty(actualAnswer)) {
      let answerAlias;
      let recommentAnswerAlias;
      if (type === '5') {
        answerAlias = parseFloat(recommentAnswer);
        recommentAnswerAlias = parseFloat(actualAnswer);
      } else {
        answerAlias = parseInt(recommentAnswer, 10);
        recommentAnswerAlias = parseInt(actualAnswer, 10);
      }
      if (answerAlias >= recommentAnswerAlias) {
        cellBackGround = 'rgb(99,200,36)';
      }
      if (answerAlias < recommentAnswerAlias) {
        cellBackGround = 'rgb(255, 57, 57)';
      }
    }
    return cellBackGround;
  }

  render() {
    const {
      rows, nameCell, textTable, cell, cameraContainer, answerText, cameraCell,
    } = styles;
    const { question: questItem, active, onSelectQuestion } = this.props;
    const {
      question, answer, comment, photos,
    } = questItem;
    const {
      isAnswered,
      isPhotoRequired,
      isCommentRequired,
    } = checkIfQuestionAnswered(questItem);

    let actualAnswer = answer;
    if (question.AnswerFormatID === '8') actualAnswer = truncateDropdownRecommentAnswer(answer || ' ');
    if (question.AnswerFormatID === '2' && answer) actualAnswer = moment(answer, DATE_ANSWER_FORMAT).format(recommendAnswerDateFormat);

    let recommentAnswer = question.AnswerRecommend;
    if (question.AnswerFormatID === '8') {
      recommentAnswer = truncateDropdownRecommentAnswer(question.AnswerRecommend ? getRecommentDropdownAnswer(question.AnswerRecommend) : ' ');
    }

    let cellBackGround = '#fff';
    let textColor = '#fff';
    const questionTitleColor = isAnswered ? 'rgb(133,133,133)' : 'rgb(255,57,57)';
    if (active) {
      cellBackGround = 'rgb(255,252,197)';
    }
    if (!isAnswerEmpty(answer) && recommentAnswer === actualAnswer) {
      cellBackGround = 'rgb(99,200,36)';
    }

    if (!isAnswerEmpty(answer) && recommentAnswer !== actualAnswer) {
      cellBackGround = 'rgb(255, 57, 57)';
    }

    if (recommentAnswer === null || recommentAnswer === '') {
      cellBackGround = 'transparent';
      textColor = 'rgb(133,133,133)';
    }

    // if integer, fraction, percent type question
    if (['3', '5', '6'].includes(question.AnswerFormatID)) {
      const intCellBackground = this.getCellBackground(recommentAnswer, actualAnswer, question.AnswerFormatID);
      if (intCellBackground) { cellBackGround = intCellBackground; }
    }

    let icon = null;
    let onIconPress = null;
    if (question.PhotoReport) {
      icon = (isPhotoRequired && (!photos || !photos.length)) ? cameraActiveImg : cameralistImg;
      onIconPress = this.openCamera;
    }
    if (isCommentRequired) {
      icon = (comment && comment !== '') ? commentIcon : commentActiveIcon;
      onIconPress = this.focusOnComment;
    }
    return (
      <TouchableWithoutFeedback
        key={question.ID}
        onPress={onSelectQuestion}
      >
        <View style={[rows, { backgroundColor: active ? 'rgb(255,252,197)' : '#fff' }]}>
          <View style={nameCell}>
            <Text style={[textTable, { color: questionTitleColor }]}>
              {question.Name}
            </Text>
          </View>
          <View style={cameraCell}>
            {icon && (
              <TouchableOpacity onPress={onIconPress} style={cameraContainer}>
                <Image source={icon} />
              </TouchableOpacity>
            )}
          </View>
          <View style={cell}>
            <Text style={textTable} numberOfLines={2}>
              {recommentAnswer}
            </Text>
          </View>
          <TouchableOpacity onPress={this.onAnswerCellPress} style={[cell, { backgroundColor: cellBackGround }]} activeOpacity={1}>
            <Text
              style={[{ ...textTable, ...answerText, color: textColor }]}
              numberOfLines={2}
            >
              {(question.AnswerFormatID === '3' && !isAnswerEmpty(actualAnswer)) ? `${actualAnswer}%` : actualAnswer}
            </Text>
          </TouchableOpacity>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  rows: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderColor: 'rgb(192,192,192)',
  },
  nameCell: {
    width: '57.2%',
    borderColor: 'rgb(192,192,192)',
    justifyContent: 'center',
  },
  cell: {
    width: '17%',
    borderLeftWidth: 0.5,
    borderColor: 'rgb(192,192,192)',
    minHeight: 40,
    justifyContent: 'center',
    alignItems: 'center',
  }, // / 375 - 213 - 33 - 63
  cameraCell: {
    width: '8.8%',
    borderLeftWidth: 0.8,
    borderColor: 'rgb(192,192,192)',
    minHeight: 40,
  },
  textTable: {
    fontSize: 13,
    color: 'rgb(133,133,133)',
    paddingVertical: 5,
    marginHorizontal: 8,
  },
  answerText: {
    fontWeight: 'bold',
  },
  cameraContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
});

export default QuestionItem;
