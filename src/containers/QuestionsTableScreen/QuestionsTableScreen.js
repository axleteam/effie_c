import React from 'react';
import {
  StyleSheet, Text, FlatList, TouchableOpacity, Dimensions,
} from 'react-native';
import {
  View, Input, Icon, Item,
} from 'native-base';
import PropTypes from 'prop-types';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { isIphoneX } from 'react-native-iphone-x-helper';
import {
  VISIT,
} from '../../store/workingDay/constants';
import MyHeader from '../../components/header';
import QuestionItem from '../../components/QuestionItem';
import Keyboard from '../../components/Keyboard';
import AlertModal from '../../components/AlertModal';
import DropdownModal from '../../components/DropdownModal';
import { checkIfQuestionsAnswered } from '../../store/currentRoute/methods';
import { STEP_ID_GPS } from '../../utils/constants';

const { height } = Dimensions.get('window');
const additionalHeight = isIphoneX() ? 100 : 60; // header, status bar etc
const viewHeight = (height - additionalHeight) * 0.85;
class QuestionsTableScreen extends React.Component {
  static navigationOptions = {
    header: null,
  }

  static propTypes = {
    navigation: PropTypes.object,
    getQuestions: PropTypes.func,
    currentRouteStore: PropTypes.object,
    updateQuestion: PropTypes.func,
    updateData: PropTypes.func,
    preComments: PropTypes.any,
  }

  constructor(props) {
    super(props);
    const { currentRouteStore: { curStepData, currentQuestItems } } = props;

    this.commentInputRef = null;

    this.state = {
      activeQuestion: null,
      activeQuestionIndex: null,
      comment: '',
      predefinedComments: [],
      alert: false,
      dropdownModal: false,
    };
    if (!curStepData.QuestHeaderID || curStepData.QuestHeaderID === STEP_ID_GPS) {
      props.navigation.navigate('VisitScreen');
    } else if (!currentQuestItems || !currentQuestItems.ids.length) {
      if (curStepData.QuestHeaderID) {
        props.getQuestions(curStepData.QuestHeaderID);
      } else {
        props.navigation.navigate('VisitScreen');
      }
    } else {
      /* Same as call to this.setActiveQuestion() */
      this.state = {
        ...this.state,
        ...this.getActiveQuestionStateUpdate(currentQuestItems.ids[0], 0),
      };
    }
  }

  componentWillReceiveProps = (nextProps) => {
    const { activeQuestion } = this.state;
    const {
      currentRouteStore: { currentQuestItems },
    } = nextProps;
    if (activeQuestion === null && currentQuestItems.ids.length > 0) {
      this.setActiveQuestion(currentQuestItems.ids[0], 0, nextProps);
    }
  }

  getPredefinedCommentsForQuestion = (activeQuestion, props) => {
    const {
      preComments,
      currentRouteStore: { currentQuestItems },
    } = props || this.props;
    let comments = [];
    if (currentQuestItems[activeQuestion]) {
      comments = preComments.byCategoryId[currentQuestItems[activeQuestion].question.QuestCategoryID] || [];
    }
    return comments;
  }

  getActiveQuestionStateUpdate = (activeQuestion, activeQuestionIndex, props) => {
    const { currentRouteStore: { currentQuestItems } } = props || this.props;
    return {
      activeQuestion,
      activeQuestionIndex,
      comment: currentQuestItems[activeQuestion].comment,
      predefinedComments: this.getPredefinedCommentsForQuestion(activeQuestion, props),
    };
  }

  setActiveQuestion = (activeQuestion, activeQuestionIndex, props) => {
    /* If you change this function - please doublecheck if constructor needs to be changed as well, because there is a duplicated code  */
    const state = this.getActiveQuestionStateUpdate(activeQuestion, activeQuestionIndex, props || this.props);
    this.setState(() => state);
  }


  answerSubmit = (answer) => {
    const { activeQuestion } = this.state;
    const {
      currentRouteStore: { currentQuestItems },
    } = this.props;
    this.props.updateQuestion(activeQuestion, {
      ...currentQuestItems[activeQuestion],
      answer,
    });
  }

  commentSubmit = async (comment) => {
    const { activeQuestion } = this.state;
    const {
      currentRouteStore: { currentQuestItems },
    } = this.props;
    this.props.updateQuestion(activeQuestion, {
      ...currentQuestItems[activeQuestion],
      comment,
    });
    this.setState(() => ({ comment }));
  }

  redirectToVisitScreen = async () => {
    const {
      setWorkingDayStage,
      navigation,
      currentRouteStore: { curVisitQuests, curVisitItems, currentQuestItems },
      questionsToVisitItems,
    } = this.props;
    const currentQuestions = { ...currentQuestItems };
    delete currentQuestions.ids;
    await setWorkingDayStage(VISIT);
    navigation.navigate('VisitScreen');
    const newVisitItems = {
      ...curVisitItems,
      ...currentQuestions,
    };
    questionsToVisitItems(curVisitQuests, newVisitItems);
  }

  upDownArrow = (type) => {
    const {
      currentRouteStore: { currentQuestItems },
    } = this.props;
    const { activeQuestionIndex } = this.state;
    // const index = currentQuestItems.ids.findIndex(item => item === activeQuestion);
    const index = activeQuestionIndex;
    if (index > -1) {
      switch (type) {
        case 'up':
          if (index !== 0) {
            this.setActiveQuestion(currentQuestItems.ids[index - 1], activeQuestionIndex - 1);
            this.flatListRef.scrollToIndex({ animated: false, index: index - 1, viewPosition: 0.5 });
            // this.debounceScrollTo(index - 1);
          }
          break;
        case 'down':
          if (index !== currentQuestItems.ids.length - 1) {
            this.setActiveQuestion(currentQuestItems.ids[index + 1], activeQuestionIndex + 1);
            this.flatListRef.scrollToIndex({ animated: false, index: index + 1, viewPosition: 0.5 });
            // this.debounceScrollTo(index + 1);
          }
          break;
        default:
          return false;
      }
    }
    return false;
  }

  checkQuestions = async () => {
    const {
      currentRouteStore: { currentQuestItems, curVisitQuests },
      questionsToVisitItems, navigation,
    } = this.props;
    const unansweredQuestions = checkIfQuestionsAnswered(currentQuestItems);
    if (unansweredQuestions.length > 0) {
      this.setState(() => ({ alert: true }));
    } else {
      const { currentRouteStore: { curStepData: { QuestHeaderID } } } = this.props;
      const curVisitQuestsIndex = curVisitQuests.ids
        .findIndex(stepID => curVisitQuests[stepID].stepData.QuestHeaderID === QuestHeaderID);
      this.setState(() => ({ activeQuestion: null }));
      /* Update Quest Item */
      curVisitQuests[curVisitQuests.ids[curVisitQuestsIndex]].isCompleted = true;
      const questionUpdated = currentQuestItems;
      delete questionUpdated.ids;
      let { currentRouteStore: { curVisitItems } } = this.props;
      curVisitItems = {
        ...curVisitItems,
        ...questionUpdated,
      };
      /* Update VisitItems */

      await questionsToVisitItems(curVisitQuests, curVisitItems);
      navigation.navigate('VisitScreen');
    }
  }

  addPhoto = (image) => {
    const { currentRouteStore: { currentQuestItems } } = this.props;
    const { activeQuestion } = this.state;
    const photos = [...currentQuestItems[activeQuestion].photos, image];
    this.props.updateQuestion(activeQuestion, {
      ...currentQuestItems[activeQuestion],
      photos,
    });
  }

  removeImage = (index) => {
    const { currentRouteStore: { currentQuestItems } } = this.props;
    const { activeQuestion } = this.state;
    const photos = [...currentQuestItems[activeQuestion].photos];
    photos.splice(index, 1);
    this.props.updateQuestion(activeQuestion, {
      ...currentQuestItems[activeQuestion],
      photos,
    });
  }

  changeCommentText = text => this.setState(() => ({ comment: text }))

  toggleCommentModal = () => this.setState(state => ({ dropdownModal: !state.dropdownModal }))

  keyExtractor = item => item.toString();

  setCommentInputRef = (ref) => { this.commentInputRef = ref; }

  focusOnComment = () => {
    const { predefinedComments } = this.state;
    if (predefinedComments.length > 0) {
      this.toggleCommentModal();
    } else {
      this.commentInputRef._root.focus();
    }
  }

  onCommentTouch = () => {
    const { predefinedComments } = this.state;
    if (predefinedComments.length > 0) this.toggleCommentModal();
  }

  cellPress = (AnswerFormatID) => {
    switch (AnswerFormatID.toString()) {
      case '2':
        this.keyBoard.toggleCalendar();
        break;
      case '3':
        this.keyBoard.toggleCalculatorModal();
        break;
      case '4':
        this.keyBoard.toggleTextModal();
        break;
      case '8':
        this.keyBoard.toggleDropdownModal();
        break;
      default:
        break;
    }
  }

  renderQuestion = ({ item, index }) => {
    const { activeQuestion } = this.state;
    const { currentRouteStore: { currentQuestItems } } = this.props;
    const isActive = activeQuestion === item;
    return (
      <QuestionItem
        question={currentQuestItems[item]}
        key={item}
        active={isActive}
        onSelectQuestion={() => this.setActiveQuestion(item, index)}
        onAddImage={this.addPhoto}
        onAnswer={this.answerSubmit}
        onCellPress={this.cellPress}
        focusOnComment={this.focusOnComment}
      />
    );
  }


  render() {
    const {
      flexOne,
      mainArea,
      viewArea,
      tableHeader,
      textTable,
      cell,
      itemStyle,
      commentStyle,
    } = styles;
    const {
      navigation,
      currentRouteStore: { curVisitQuests, currentQuestItems, curStepData },
      QuestCategories,
    } = this.props;
    const { StepID } = curStepData;

    const {
      activeQuestion, comment, alert, dropdownModal, predefinedComments,
    } = this.state;
    let categoryName = null;
    if (currentQuestItems[activeQuestion]) {
      const category = currentQuestItems[activeQuestion].QuestCategoryID
        && QuestCategories.find(curCategoty => curCategoty.ID === currentQuestItems[activeQuestion].QuestCategoryID);
      categoryName = category && category.Name;
    }

    return (
      <View style={mainArea}>
        <KeyboardAwareScrollView bounces={false} contentContainerStyle={flexOne} keyboardShouldPersistTaps="always" keyboardDismissMode="on-drag">
          <MyHeader
            navigation={navigation}
            title={curVisitQuests[StepID].stepData.Name}
            backButton
            backGoto={this.redirectToVisitScreen}
            rightIcon="check"
            onRightIconClick={this.checkQuestions}
          />
          <View style={[viewArea]}>
            <View style={tableHeader}>
              <View style={{ width: '57.2%' }} />
              <View style={[cell, { width: '8.8%' }]}>
                <Text style={textTable}>
                  *
                </Text>
              </View>
              <View style={cell}>
                <Text style={textTable}>
                  Цель
                </Text>
              </View>
              <View style={cell}>
                <Text style={textTable}>
                  Ответ
                </Text>
              </View>
            </View>
            {activeQuestion
              && (
                <FlatList
                  ref={(ref) => { this.flatListRef = ref; }}
                  data={currentQuestItems.ids}
                  extraData={[this.state, this.props]}
                  keyExtractor={this.keyExtractor}
                  renderItem={this.renderQuestion}
                  keyboardShouldPersistTaps="always"
                  keyboardDismissMode="on-drag"
                />
              )}

            <Item regular style={itemStyle}>
              <Input
                ref={this.setCommentInputRef}
                placeholder="Ввести комментарий"
                onChangeText={this.commentSubmit}
                value={comment}
                onSubmitEditing={() => this.commentSubmit(comment)}
                style={commentStyle}
                onTouchStart={this.onCommentTouch}
                editable={!(predefinedComments.length > 0)}
                maxLength={1000}
              />
              <TouchableOpacity onPress={() => this.commentSubmit('')}>
                <Icon name="close-circle" />
              </TouchableOpacity>
            </Item>

          </View>

          {activeQuestion
            && (
              <Keyboard
                active={currentQuestItems[activeQuestion]}
                onAnswer={this.answerSubmit}
                onPressArrow={this.upDownArrow}
                onAddImage={this.addPhoto}
                onRemoveImage={this.removeImage}
                categoryName={categoryName}
                viewHeight={viewHeight}
                onRef={ref => (this.keyBoard = ref)}
              />
            )}
          <AlertModal
            visible={alert}
            onClick={() => this.setState(() => ({ alert: false }))}
            title="Внимание"
            message="Заполнены не все ответы на обязательные вопросы. Необходимо ответить или сделать по ним фотоотчет!"
          />
          {predefinedComments.length ? (
            <DropdownModal
              value={comment}
              isVisible={dropdownModal}
              onSubmit={(com) => {
                this.commentSubmit(com);
                this.toggleCommentModal();
              }}
              options={[...predefinedComments, '']}
              name="Name"
              key="ID"
              extraOptionText="Ввести свой комментарий"
              onCancel={this.toggleCommentModal}
              title="Ввести комментарий"
              maxLength={1000}
            />
          ) : null}
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  flexOne: {
    flex: 1,
  },
  mainArea: {
    flex: 1,
    backgroundColor: '#fff',
  },
  viewArea: {
    height: viewHeight,
  },
  tableHeader: {
    flexDirection: 'row',
    backgroundColor: 'rgb(233,233,237)',
  },
  cell: {
    width: '17%',
    borderLeftWidth: 1,
    borderColor: 'rgb(192,192,192)',
  },
  textTable: {
    lineHeight: 30,
    fontSize: 13,
    textAlign: 'center',
  },
  itemStyle: {
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 0,
    borderTopWidth: 0.5,
  },
  commentStyle: {
    height: 47,
  },
});

export default QuestionsTableScreen;
