import { connect } from 'react-redux';
import QuestionsTableScreen from './QuestionsTableScreen';
import { setWorkingDayStage } from '../../store/workingDay/duck';
import { getQuestions, updateQuestion, questionsToVisitItems } from '../../store/currentRoute/duck';
import { groupPredefinedCommentsByCategoryId } from '../../store/serverData/methods';

const mapStateToProps = ({ workingDayStore, currentRouteStore, serverDataStore }) => ({
  workingDayStore,
  currentRouteStore,
  preComments: groupPredefinedCommentsByCategoryId(serverDataStore),
  QuestCategories: serverDataStore.QuestCategories,
});

const mapDispatchToProps = {
  setWorkingDayStage,
  getQuestions,
  updateQuestion,
  questionsToVisitItems,
  updateData: () => ({ type: 'UPDATE_DATA' }),
};

export default connect(mapStateToProps, mapDispatchToProps)(QuestionsTableScreen);
