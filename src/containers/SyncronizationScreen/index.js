import { connect } from 'react-redux';
import SyncronizationScreen from './SyncronizationScreen';
import {
  setWorkingDayStage,
  setTimer,
  setWorkingDayChanged,
  setWorkingDayDate,
} from '../../store/workingDay/duck';
import { syncData, cancelSyncData } from '../../store/serverData/duck';

const mapStateToProps = ({
  workingDayStore, userStore, serverDataStore, currentRouteStore,
}) => ({
  workingDayStore,
  userStore,
  serverDataStore,
  currentRouteStore,
});

const mapDispatchToProps = {
  setWorkingDayStage,
  syncronizeData: payload => syncData(payload),
  cancelSyncData,
  initSync: () => ({ type: 'INIT_SYNC' }),
  setTimer,
  setWorkingDayChanged,
  setWorkingDayDate,
};

export default connect(mapStateToProps, mapDispatchToProps)(SyncronizationScreen);
