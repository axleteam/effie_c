import React from 'react';
import {
  View, Text, StyleSheet, Animated, Easing, Image,
} from 'react-native';
import PropTypes from 'prop-types';
import { Button } from 'native-base';
import clockPng from '../../img/clock.png';
import loaderPng from '../../img/group8.png';
import tickPng from '../../img/tick.png';
import errorPng from '../../img/errorSync.png';

class SyncStepView extends React.PureComponent {
  spinValue = new Animated.Value(0);

  static propTypes = {
    phase: PropTypes.string,
    onPress: PropTypes.func,
    progress: PropTypes.string,
    onCancel: PropTypes.func,
    onSuccess: PropTypes.func,
    stepNumber: PropTypes.number,
    lastSyncTime: PropTypes.string,
  }

  spinValue = new Animated.Value(0);

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.isRotating !== this.props.isRotating) {
      if (nextProps.isRotating) {
        Animated.loop(Animated.timing(
          this.spinValue,
          {
            toValue: 1,
            duration: 3000,
            easing: Easing.linear,
            useNativeDriver: true,
          },
        )).start();
      }
    }
  }

  render() {
    const {
      text,
      isCancelButton,
      onCancelPress,
      proggress,
      actionButtonTitle,
      onActionButtonPress,
      children,
      imgSource,
      isRotating,
    } = this.props;
    let spin = null;
    if (isRotating) {
      spin = this.spinValue.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '360deg'],
      });
    }
    return (
      <View style={styles.container}>
        <View style={styles.topPart}>
          <View style={styles.imgContainer}>
            {isRotating
              ? <Animated.Image style={{ transform: [{ rotate: spin }] }} source={imgSource} />
              : <Image source={imgSource} />
            }
          </View>
          <View style={styles.progressContainer}>
            {proggress}
          </View>
          <View style={styles.descriptionContainer}>
            {children || text}
          </View>
        </View>
        <View style={[styles.bottomPart]}>
          <View style={styles.bottomPartBlock}>
            {isCancelButton && (
              <Button block style={styles.loadingButtonStyle} onPress={onCancelPress}>
                <Text style={styles.buttonText}>
                  Отмена
                </Text>
              </Button>
            )}
          </View>
          <View style={styles.bottomPartBlock}>
            {actionButtonTitle && (
              <Button block style={styles.buttonStyle} onPress={onActionButtonPress}>
                <Text style={styles.buttonText}>
                  {actionButtonTitle}
                </Text>
              </Button>
            )}
          </View>
          <View style={styles.bottomLinkBlock}>
            <Text style={styles.bottomLinksText} onPress={this.props.navigateToFedbackScreen}>
              Связаться с нами
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

SyncStepView.defaultProps = {
  progress: null,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  topPart: {
    flex: 0.7,
    justifyContent: 'flex-end',
    alignItems: 'center',
    flexDirection: 'column',
  },
  imgContainer: {
    flex: 0.24,
    justifyContent: 'center',
  },
  progressContainer: {
    flex: 0.18,
    justifyContent: 'center',
  },
  descriptionContainer: {
    flex: 0.2,
    paddingHorizontal: 30,
    marginBottom: 75,
    justifyContent: 'center',
  },
  bottomPart: {
    flex: 0.3,
    paddingBottom: 40,
    paddingHorizontal: 20,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'column',
  },
  bottomPartBlock: {
    height: 55,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomLinkBlock: {
    height: 20,
  },
  buttonStyle: {
    height: 55,
    backgroundColor: 'rgb(95,95,209)',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loadingButtonStyle: {
    height: 55,
    width: '100%',
    backgroundColor: 'rgba(95,95,209, 0.7)',
    justifyContent: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  description: {
    fontSize: 17,
    color: 'rgb(5,6,51)',
    textAlign: 'center',
  },
  bottomLinksText: {
    paddingBottom: 10,
    color: 'rgb(5,6,51)',
    textDecorationLine: 'underline',
  },
});

export default SyncStepView;
