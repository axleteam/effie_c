import React from 'react';
import {
  View, StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';
import MyHeader from '../../components/header';
import SyncView from './SyncView';
import {
  ROUTE, ROUTE_PREPARING, ROUTE_FINISH, RESULTS,
} from '../../store/workingDay/constants';
import {
  INIT, LOADING, SUCCESS, ERROR,
} from '../../utils/constants';

class SyncronizationScreen extends React.Component {
    static navigationOptions = {
      header: null,
    }

    static propTypes = {
      navigation: PropTypes.object,
      userStore: PropTypes.object,
      syncronizeData: PropTypes.func,
      cancelSyncData: PropTypes.func,
      serverDataStore: PropTypes.object,
      initSync: PropTypes.func,
      workingDayStore: PropTypes.object,
    }

    constructor(props) {
      super(props);
      props.initSync();
    }

    state = {
      stepNumber: 1,
      workingDayStage: null,
      phase: null,
    }

    static getDerivedStateFromProps = (nextProps, state) => {
      const { workingDayStage } = nextProps.workingDayStore;
      const { synchDataPhase } = nextProps.serverDataStore;
      if ((state.workingDayStage !== workingDayStage) || (state.phase !== synchDataPhase)) {
        if (workingDayStage === ROUTE_PREPARING && synchDataPhase === INIT) {
          return { stepNumber: 1, workingDayStage, phase: synchDataPhase };
        }
        if (synchDataPhase === LOADING) {
          return { stepNumber: 2, workingDayStage, phase: synchDataPhase };
        }
        if (synchDataPhase === SUCCESS && workingDayStage === ROUTE_PREPARING) {
          return { stepNumber: 3, workingDayStage, phase: synchDataPhase };
        }
        if (synchDataPhase === ERROR) {
          return { stepNumber: 4, workingDayStage, phase: synchDataPhase };
        }
        if (workingDayStage === ROUTE_FINISH && synchDataPhase === INIT) {
          return { stepNumber: 7, workingDayStage, phase: synchDataPhase };
        }
        if (workingDayStage === ROUTE_FINISH && synchDataPhase === SUCCESS) {
          return { stepNumber: 8, workingDayStage, phase: synchDataPhase };
        }
        if (synchDataPhase === INIT) {
          return { stepNumber: 5, workingDayStage, phase: synchDataPhase };
        }
        if (synchDataPhase === SUCCESS) {
          return { stepNumber: 6, workingDayStage, phase: synchDataPhase };
        }
      }
      return null;
    }

    goBack = () => {
      const { navigation } = this.props;
      navigation.goBack();
    }

    redirectTo = async () => {
      const {
        setWorkingDayStage,
        navigation,
        setTimer,
        workingDayStore: { workingDayStage },
        setWorkingDayChanged,
        setWorkingDayDate,
      } = this.props;
      setWorkingDayChanged(false);
      const curDayMoment = moment({ h: 0, m: 0, s: 0, ms: 0 }).utc({ h: 0, m: 0, s: 0, ms: 0 });
      setWorkingDayDate(curDayMoment.toISOString());
      if (workingDayStage === ROUTE_FINISH) {
        await setWorkingDayStage(RESULTS);
        navigation.navigate('ResultScreen');
      // } else if (workingDayStage === ROUTE) {
      //   await setWorkingDayStage(ROUTE_FINISH);
      //   navigation.navigate('RouteScreen');
      } else {
        await setTimer();
        await setWorkingDayStage(ROUTE);
        navigation.navigate('RouteScreen');
      }
    }

    syncronize = () => {
      const { syncronizeData } = this.props;
      syncronizeData();
    }

    cancelSync = () => {
      const { cancelSyncData } = this.props;
      cancelSyncData();
    }

    loadText = () => {
      const { navigation, workingDayStore: { workingDayStage }, serverDataStore } = this.props;
      const fromRoute = navigation.getParam('fromRoute', null);
      switch (true) {
        case (fromRoute === 'LEFT_SYNC'):
          return `Последняя синхронизация была выполнена ${moment(serverDataStore.lastSynchDatetime).fromNow()} день назад`;
        case (workingDayStage === 'ROUTE_FINISH'):
          return 'Выполнить синхронизацию для окончания рабочего дня';
        default:
          return 'Для начала работы необходимо принять данные';
      }
    }

    navigateToFedbackScreen = () => {
      if (this.props.synchDataPhase === LOADING) this.cancelSync();
      this.props.navigation.navigate('Feedback', { previousRoute: 'Syncronization' });
    }

    renderSync = () => {
      const { synchDataProgress, lastSynchDatetime } = this.props.serverDataStore;
      return (
        <SyncView
          stepNumber={this.state.stepNumber}
          workingDayStage={this.state.workingDayStage}
          phase={this.state.phase}
          lastSyncTime={lastSynchDatetime ? moment(lastSynchDatetime).fromNow() : ''}
          onPress={this.syncronize}
          progress={synchDataProgress.toString()}
          onCancel={this.cancelSync}
          onSuccess={this.redirectTo}
          navigateToFedbackScreen={this.navigateToFedbackScreen}
        />
      );
    }

    render() {
      const {
        mainArea,
      } = styles;
      const { navigation } = this.props;
      const fromRoute = navigation.getParam('fromRoute', null);
      return (
        <View style={mainArea}>
          {fromRoute && (
          <MyHeader
            navigation={styles.navigation}
            title="Синхронизация"
            backButton
            backGoto={this.goBack}
          />
          )}
          {this.renderSync()}
        </View>
      );
    }
}

const styles = StyleSheet.create({
  mainArea: {
    flex: 1,
    backgroundColor: 'rgb(233,233,237)',
  },
});

export default SyncronizationScreen;
