import React from 'react';
import {
  View, Text, StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import { Button } from 'native-base';
import SyncStepView from './SyncStepView';
import clockPng from '../../img/clock.png';
import loaderPng from '../../img/group8.png';
import tickPng from '../../img/tick.png';
import errorPng from '../../img/errorSync.png';

class SyncView extends React.PureComponent {

  static propTypes = {
    phase: PropTypes.string,
    onPress: PropTypes.func,
    progress: PropTypes.string,
    onCancel: PropTypes.func,
    onSuccess: PropTypes.func,
    stepNumber: PropTypes.number,
    lastSyncTime: PropTypes.string,
  }

  state = {
    phase: null,
  }

  renderInitOnRoutePreparing = () => (
    <SyncStepView
      actionButtonTitle="Синхронизировать"
      onActionButtonPress={this.props.onPress}
      imgSource={loaderPng}
      navigateToFedbackScreen={this.props.navigateToFedbackScreen}
    >
      <Text style={styles.description}>
        Для начала работы
        необходимо принять данные
      </Text>
    </SyncStepView>
  )

  renderLoading = () => {
    return (
      <SyncStepView
        isRotating
        isCancelButton
        onCancelPress={this.props.onCancel}
        imgSource={loaderPng}
        navigateToFedbackScreen={this.props.navigateToFedbackScreen}
        proggress={(
          <Text style={styles.description}>
            {Math.floor(this.props.progress)}
            %
          </Text>
        )}
      >
        <Text style={styles.description}>
          Пожалуйста, дождитесь
          полного завершения процесса
        </Text>
      </SyncStepView>
    );
  };

  renderSuccessOnRoutePreparing = () => (
    <SyncStepView
      actionButtonTitle="Начать рабочий день"
      onActionButtonPress={this.props.onSuccess}
      imgSource={tickPng}
      navigateToFedbackScreen={this.props.navigateToFedbackScreen}
    >
      <Text style={styles.description}>
        Синхронизация прошла успешно.
      </Text>
      <Text style={styles.description}>
        Нажмите «Начать рабочий день»
      </Text>
    </SyncStepView>
  );

  renderError = () => (
    <SyncStepView
      actionButtonTitle="Синхронизировать"
      onActionButtonPress={this.props.onPress}
      imgSource={errorPng}
      navigateToFedbackScreen={this.props.navigateToFedbackScreen}
    >
      <Text style={styles.description}>
        Ошибка синхронизации
      </Text>
      <Text style={styles.description}>
        Проверьте интернет соединение
      </Text>
    </SyncStepView>
  );

  renderInitInMiddleOfDay = () => (
    <SyncStepView
      actionButtonTitle="Синхронизировать"
      onActionButtonPress={this.props.onPress}
      imgSource={clockPng}
      navigateToFedbackScreen={this.props.navigateToFedbackScreen}
    >
      <Text style={styles.description}>
        Последняя синхронизация была выполнена {this.props.lastSyncTime}
      </Text>
    </SyncStepView>
  );

  renderSuccessInMiddleOfDay = () => (
    <SyncStepView
      actionButtonTitle="Продолжить рабочий день"
      onActionButtonPress={this.props.onSuccess}
      imgSource={tickPng}
      navigateToFedbackScreen={this.props.navigateToFedbackScreen}
    >
      <Text style={styles.description}>
        Синхронизация прошла успешно.
      </Text>
      <Text style={styles.description}>
        Нажмите «Продолжить рабочий день»
      </Text>
    </SyncStepView>
  );

  renderSuccessOnRouteFinish = () => (
    <SyncStepView
      actionButtonTitle="Посмотреть результаты"
      onActionButtonPress={this.props.onSuccess}
      imgSource={tickPng}
      navigateToFedbackScreen={this.props.navigateToFedbackScreen}
    >
      <Text style={styles.description}>
        Синхронизация прошла успешно.
      </Text>
      <Text style={styles.description}>
        Нажмите «Посмотреть результаты»
      </Text>
    </SyncStepView>
  );

  renderInitOnRouteFinish = () => (
    <SyncStepView
      actionButtonTitle="Синхронизировать"
      onActionButtonPress={this.props.onPress}
      imgSource={loaderPng}
      navigateToFedbackScreen={this.props.navigateToFedbackScreen}
    >
      <Text style={styles.description}>
        Выполнить синхронизацию для окончания рабочего дня
      </Text>
    </SyncStepView>
  )

  renderView = () => {
    const { stepNumber } = this.props;
    switch (stepNumber) {
      case 1:
        return this.renderInitOnRoutePreparing();
      case 2:
        return this.renderLoading();
      case 3:
        return this.renderSuccessOnRoutePreparing();
      case 4:
        return this.renderError();
      case 5:
        return this.renderInitInMiddleOfDay();
      case 6:
        return this.renderSuccessInMiddleOfDay();
      case 7:
        return this.renderInitOnRouteFinish();
      case 8:
        return this.renderSuccessOnRouteFinish();
      default:
        return null;
    }
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        {this.renderView()}
      </View>
    );
  }
}

SyncView.defaultProps = {
  progress: null,
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  descriptionContainer: {
    paddingHorizontal: 30,
    marginTop: 30,
    justifyContent: 'center',
  },
  description: {
    fontSize: 17,
    color: 'rgb(5,6,51)',
    textAlign: 'center',
  },
});

export default SyncView;
