import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text, Image,
  ScrollView,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {
  View, Button,
} from 'native-base';
import PropTypes from 'prop-types';
import moment from 'moment';
import QuestItem from '../../components/QuestItem';
import {
  QUESTIONS,
  ROUTE,
  ONBOARDING,
} from '../../store/workingDay/constants';
import { checkDisabled } from '../../utils/methods';
import { STEP_ID_GPS } from '../../utils/constants';
import AlertModal from '../../components/AlertModal';
import backButtonPng from '../../img/pinLeft.png';

const { height } = Dimensions.get('window');
const defaultGeopositionOptions = {
  timeout: 7000,
};
const headerHeight = 100;
const viewportHeight = height - headerHeight;
class VisitScreen extends React.Component {
  static navigationOptions = {
    header: null,
  }

  constructor(props) {
    super(props);

    const { currentRouteStore: { curTT, curVisit } } = props;
    this.timeInTTInSeconds = curTT && curTT.TimeInTT ? curTT.TimeInTT * 60 * 1000 : null;
    this.begDateMoment = curVisit && moment(curVisit.BegDate);
    this.timer = setInterval(this.tick, 1000);
    const { timer, isTimerRed } = this.getTimerSetting();
    this.state = {
      alert: false,
      isTimerRed: isTimerRed || false,
      timer: timer || '00:00:00',
    };
  }

  static propTypes = {
    navigation: PropTypes.object,
    getVisitData: PropTypes.func,
    serverDataStore: PropTypes.object,
    curVisitQuests: PropTypes.object,
    currentRouteStore: PropTypes.object,
  }

  componentWillMount() {
    const {
      serverDataStore: { StepConfigClient },
      curVisitQuests,
      currentRouteStore: { curTT },
    } = this.props;
    if (typeof StepConfigClient !== 'undefined' && curTT !== undefined && !curVisitQuests.ids.length) {
      this.props.getVisitData(curTT.ExtID);
    }
  }

  componentWillUnmount = () => {
    clearInterval(this.timer);
  }

  getTimerSetting = () => {
    const { currentRouteStore: { curTT, curVisit } } = this.props;
    let timer = null;
    let isTimerRed = null;
    if (this.timeInTTInSeconds) {
      if (!this.begDateMoment && curVisit) this.begDateMoment = moment(curVisit.BegDate);
      if (!this.timeInTTInSeconds && curTT) this.timeInTTInSeconds = curTT.TimeInTT * 60 * 1000;
      const nowAndBegDateDiff = moment().diff(this.begDateMoment, 'milliseconds');
      if (nowAndBegDateDiff <= this.timeInTTInSeconds) {
        timer = moment.utc(moment.duration(this.timeInTTInSeconds - nowAndBegDateDiff, 'milliseconds').as('milliseconds')).format('HH:mm:ss');
        isTimerRed = false;
      } else {
        timer = moment.utc(moment.duration(nowAndBegDateDiff - this.timeInTTInSeconds, 'milliseconds').as('milliseconds')).format('HH:mm:ss');
        isTimerRed = true;
      }
    }
    return { timer, isTimerRed };
  }

  tick = () => {
    const { currentRouteStore: { curTT } } = this.props;
    if (curTT.TimeInTT) {
      this.setState(() => this.getTimerSetting());
    }
  }

  redirectToMapOrQuestionTableScreen = async (stepData) => {
    const {
      setWorkingDayStage,
      navigation,
      currentRouteStore,
      updateData,
    } = this.props;
    currentRouteStore.curStepData = stepData;
    if (stepData.StepID === STEP_ID_GPS) {
      await updateData();
      navigation.navigate('MapScreen', { isQuest: true, noButton: false });
    } else {
      await setWorkingDayStage(QUESTIONS);
      navigation.navigate('QuestionsTableScreen');
    }
  }

  getCurrentPosition = (options = defaultGeopositionOptions) => new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(resolve, reject, options);
  });

  checkQuestCompletion = async () => {
    const {
      curVisitQuests, finishQuest, finishCurrentVisit,
      currentRouteStore: {
        curVisit, curVisitItems, curRoute, curTT, timeOfLastVisitFinish,
      },
    } = this.props;
    const check = curVisitQuests.ids
      .filter(questId => curVisitQuests[questId].isCompleted === false);

    if (!check.length) {
      const visitData = { ...curVisit };
      if (curVisitQuests.ids[0] === STEP_ID_GPS) {
        visitData.EndLatitude = 0;
        visitData.EndLongitude = 0;
        try {
          const position = await this.getCurrentPosition();
          visitData.EndLatitude = position.coords.latitude;
          visitData.EndLongitude = position.coords.longitude;
        } catch (e) {
          console.log(e);
        }
      }
      await finishQuest({ // TODO merge with finishCurrentVisit (finishQuest is a wrong name)
        visitData,
        questItems: curVisitItems,
      });
      await finishCurrentVisit();
      const routeIndex = curRoute.findIndex(route => route.TTData.ExtID === curTT.ExtID);
      curRoute[routeIndex].isVisited = true;
      this.redirectToRouteScreen();
    } else {
      this.setState(() => ({ alert: true }));
    }
  }

  redirectToRouteScreen = async () => {
    const {
      setWorkingDayStage,
      navigation,
      clearVariables,
      workingDayStore: { hasWorkingDayChanged },
    } = this.props;

    await clearVariables();
    if (!hasWorkingDayChanged) {
      await setWorkingDayStage(ROUTE);
      navigation.navigate('RouteScreen');
    } else {
      await setWorkingDayStage(ONBOARDING);
      navigation.navigate('OnBoarding');
    }
  }

  navigateToFeedbackScreen = () => {
    const { navigation } = this.props;
    navigation.navigate('Feedback', { previousRoute: 'VisitScreen' });
  }

  backButtonAction = () => this.setState(() => ({ alert: true }));

  modalClose = () => this.setState(() => ({ alert: false }));


  render() {
    const { curVisitQuests, currentRouteStore: { curTT } } = this.props;
    const { alert, isTimerRed, timer } = this.state;
    const {
      mainArea,
      viewArea,
      headerStyle,
      centerHeader,
      titleText,
      bottomHeaderText,
      buttonContainer,
    } = styles;
    const screenTitle = [`Визит в ${curTT.Name}`, curTT.Address_1, curTT.Address_2, curTT.Address_3, curTT.Address_4]
      .filter(address => !!address).join(', ');
    return (
      <View>
        <View style={headerStyle}>
          <View>
            <TouchableOpacity
              style={{ paddingTop: 0, width: 20 }}
              hitSlop={{
                top: 20, bottom: 10, left: 20, right: 30,
              }}
              onPress={this.backButtonAction}
            >
              <Image source={backButtonPng} />
            </TouchableOpacity>
          </View>
          <View style={centerHeader}>
            <Text style={titleText} numberOfLines={2}>
              {screenTitle}
            </Text>
            {curTT.TimeInTT && (
              <Text style={[bottomHeaderText, { color: isTimerRed ? 'rgb(255, 85, 85)' : '#63E848' }]}>
                {isTimerRed ? 'Визит превышен на: ' : 'Время на визит: '}
                {timer}
              </Text>
            )}
          </View>
        </View>
        <ScrollView contentContainerStyle={mainArea} showsVerticalScrollIndicator={false}>
          <View style={viewArea}>
            {curVisitQuests.ids.map((stepId, index) => (
              <QuestItem
                length={curVisitQuests.ids.length}
                key={curVisitQuests[stepId].stepData.StepID}
                index={index}
                onPress={
                  () => this
                    .redirectToMapOrQuestionTableScreen(curVisitQuests[stepId].stepData)
                }
                item={curVisitQuests[stepId]}
                enabled={checkDisabled(index, stepId, curVisitQuests)}
              />

            ))}
          </View>
          <View style={buttonContainer}>
            <Button block style={{ backgroundColor: 'rgb(95,95,209)' }} onPress={this.checkQuestCompletion}>
              <Text style={{ color: '#fff' }}>
                Завершить визит
              </Text>
            </Button>
            <View style={styles.bottomLinkBlock}>
              <Text style={styles.bottomLinksText} onPress={this.navigateToFeedbackScreen}>
              Связаться с нами
              </Text>
            </View>
          </View>
        </ScrollView>
        <AlertModal
          visible={alert}
          onClick={this.modalClose}
          title="Завершение визита"
          message="Вы можете завершить визит без сохранения (в этом случае все внесенные данные будут утеряны)"
          leftButtonText="Завершить без сохранения"
          rightButtonText="Продолжить визит"
          leftClick={this.redirectToRouteScreen}
          largeButtons
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainArea: {
    minHeight: viewportHeight,
    paddingBottom: 60,
    backgroundColor: 'rgb(233,233,237)',
  },
  viewArea: {
    paddingRight: 15,
    flexDirection: 'column',
    minHeight: viewportHeight * 4 / 5,
  },
  buttonContainer: {
    paddingHorizontal: 20,
    marginTop: 20,
    height: viewportHeight * 1 / 5,
  },
  headerStyle: {
    paddingTop: 35,
    flexDirection: 'row',
    backgroundColor: 'rgb(95,95,209)',
    paddingBottom: 10,
    paddingHorizontal: 10,
  },
  centerHeader: {
    position: 'relative',
    alignItems: 'center',
    flex: 1,
  },
  titleText: {
    color: '#fff',
    fontSize: 17,
    textAlign: 'center',
    fontWeight: 'bold',
    flexWrap: 'wrap',
  },
  bottomHeaderText: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 16,
    paddingTop: 5,
  },
  bottomLinkBlock: {
    paddingVertical: 10,
    alignSelf: 'center',
  },
  bottomLinksText: {
    color: 'rgb(5,6,51)',
    textDecorationLine: 'underline',
  },
});

export default VisitScreen;
