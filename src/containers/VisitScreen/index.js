import { connect } from 'react-redux';
import VisitScreen from './VisitScreen';
import { setWorkingDayStage } from '../../store/workingDay/duck';
import { getVisitData, clearVariables, finishCurrentVisit } from '../../store/currentRoute/duck';
import { finishQuest } from '../../store/serverData/duck';

const mapStateToProps = ({ workingDayStore, serverDataStore, currentRouteStore }) => ({
  workingDayStore,
  serverDataStore,
  curVisitQuests: currentRouteStore.curVisitQuests,
  currentRouteStore,
});

const mapDispatchToProps = {
  setWorkingDayStage: stage => setWorkingDayStage(stage),
  getVisitData: extId => getVisitData(extId),
  clearVariables: () => clearVariables(),
  finishQuest,
  finishCurrentVisit,
  updateData: () => ({ type: 'UPDATE_DATA' }),
};

export default connect(mapStateToProps, mapDispatchToProps)(VisitScreen);
