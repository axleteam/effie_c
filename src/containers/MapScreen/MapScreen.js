import React, { Component } from 'react';
import {
  SafeAreaView, StyleSheet, Dimensions, View, Text, Alert, Linking, Platform,
} from 'react-native';
import { Button } from 'native-base';
import PropTypes from 'prop-types';
import MapView, { Marker } from 'react-native-maps';
import MyHeader from '../../components/header';
import AlertModal from '../../components/AlertModal';
import { calculateDistance } from '../../utils/methods';
import {
  VISIT,
} from '../../store/workingDay/constants';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const defaultGeopositionOptions = {
  timeout: 7000,
};

class MapScreen extends Component {
    static navigationOptions = {
      header: null,
    }

    static propTypes = {
      navigation: PropTypes.object,
      TTData: PropTypes.object,
    }

    state = {
      currentLocation: {
        latitude: 0,
        longitude: 0,
      },
      alert: false,
      distance: 0,
      tried: 0,
      isLoading: false,
    }

    componentWillMount = async () => {
      try {
        const position = await this.getCurrentPosition();
        const { latitude, longitude } = position.coords;
        const region = {
          latitude,
          longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        };
        const { Latitude, Longitude } = this.props.TTData;
        const TTLocation = {
          latitude: Latitude,
          longitude: Longitude,
        };
        /* Update map view to show both user position an TT marker */
        const coords = this.getRegionForCoordinates([region, TTLocation]);

        this.mapRef.animateToRegion(coords);

        this.setState(() => ({
          currentLocation: {
            latitude,
            longitude,
          },
        }));
      } catch (error) {
        console.log(error)
        if (Platform.OS === 'ios' && error.message === 'Location services disabled.' && error.code === 2) {
          Alert.alert('GPS is disabled !', 'Please turn on and Also Allow App to run in Background', [
            { text: 'Open Settings', onPress: this.handleOpenURL },
            {
              text: 'Cancel',
              onPress: () => console.log('Permission denied'),
            },
          ]);
        }
      }
    }

    getRegionForCoordinates = (points) => {
      // points should be an array of { latitude: X, longitude: Y }
      // init first point
      let minX = points[0].latitude;
      let maxX = points[0].latitude;
      let minY = points[0].longitude;
      let maxY = points[0].longitude;

      // calculate rect
      points.map((point) => {
        minX = Math.min(minX, point.latitude);
        maxX = Math.max(maxX, point.latitude);
        minY = Math.min(minY, point.longitude);
        maxY = Math.max(maxY, point.longitude);
      });

      const midX = (minX + maxX) / 2;
      const midY = (minY + maxY) / 2;
      const deltaX = (maxX - minX) * 1.40;
      const deltaY = (maxY - minY) * 1.40;

      return {
        latitude: midX,
        longitude: midY,
        latitudeDelta: deltaX,
        longitudeDelta: deltaY,
      };
    };

    getCurrentPosition = (options = defaultGeopositionOptions) => new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resolve, reject, options);
    });

    redirectToVisitScreen = async () => {
      const {
        navigation,
        setWorkingDayStage,
        curVisitQuests,
        updateCurrentVisit,
        TTData,
        stepData,
      } = this.props;
      const isQuest = navigation.getParam('isQuest', undefined);
      if (isQuest) {
        const { currentLocation } = this.state;
        const distance = Math.round(calculateDistance(
          currentLocation, { latitude: TTData.Latitude, longitude: TTData.Longitude },
        ));
        if (distance > 300 && this.state.tried < 3) {
          this.setState(state => ({ alert: true, distance, tried: state.tried + 1 }));
        } else {
          this.setState(() => ({ isLoading: true }));
          curVisitQuests[stepData.StepID].isCompleted = true;
          let lat = 0;
          let long = 0;
          let GPSMatched = 0;
          try {
            const position = await this.getCurrentPosition();
            lat = position.coords.latitude;
            long = position.coords.longitude;
            GPSMatched = distance <= 300 ? 1 : 0;
          } catch (error) {
            console.log(error);
          }
          await updateCurrentVisit({
            StartLatitude: lat,
            StartLongitude: long,
            GPSMatched,
          });
          this.navigateToVisitScreen();
          this.setState(() => ({ isLoading: false }));
        }
      } else {
        await this.props.createCurrentVisit(this.props.TTData.ExtID);
        this.navigateToVisitScreen();
      }
    }

    handleOpenURL = async () => {
      Linking.openURL('App-Prefs:root=Privacy');
    };

    navigateToVisitScreen = async () => {
      const {
        navigation,
        setWorkingDayStage,
      } = this.props;
      await setWorkingDayStage(VISIT);
      navigation.navigate('VisitScreen');
    }

    navigateBack = () => {
      const { navigation } = this.props;
      const isQuest = navigation.getParam('isQuest', undefined);

      navigation.goBack();
      if (isQuest) {
        navigation.navigate('VisitScreen');
      } else {
        navigation.goBack();
      }
    }

    checkClicksAndDistance = () => {
      this.setState({ alert: false });
      if (this.state.tried === 3) {
        this.redirectToVisitScreen();
      }
    }

    render() {
      const { navigation, TTData } = this.props;
      const {
        currentLocation, alert, distance, isLoading,
      } = this.state;
      const distanceInKm = Math.round((distance || 0) / 1000 * 10) / 10;
      const isQuest = navigation.getParam('isQuest', undefined);
      const noButton = navigation.getParam('noButton', false);
      const messageString = `Расстояние до торговой точки ${distanceInKm} км. Максимальное допустимое расстояние - 300 метров.`;
      const retriesNumber = 3 - this.state.tried;
      const retriesStr = retriesNumber ? (retriesNumber === 1 ? 'Осталась 1 попытка' : `Осталось ${retriesNumber} попытки`) : undefined;
      const {
        mapStyle, bottomContainer, buttonStyle, buttonText, topContainer, buttonDisabledStyle,
      } = styles;
      return (
        <SafeAreaView style={{ flex: 1 }}>
          <View style={mapStyle}>
            <MapView
              ref={(component) => { this.mapRef = component; }}
              style={{ ...StyleSheet.absoluteFillObject }}
              initialRegion={{
                latitude: TTData.Latitude || LATITUDE,
                longitude: TTData.Longitude || LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
              }}
              showsUserLocation
            >
              <Marker
                coordinate={{
                  latitude: TTData.Longitude || LATITUDE,
                  longitude: TTData.Longitude || LONGITUDE,
                }}
              />
            </MapView>
          </View>
          <View style={topContainer}>
            <MyHeader navigation={navigation} title="Карта" backButton backGoto={this.navigateBack} />
          </View>
          {!noButton && (
            <View style={bottomContainer}>
              <Button block style={[buttonStyle, isLoading && buttonDisabledStyle]} onPress={this.redirectToVisitScreen} disabled={isLoading}>
                <Text style={buttonText}>
                  {isQuest ? 'Подтвердить' : 'Начать визит'}
                </Text>
              </Button>
            </View>
          )}
          <AlertModal
            visible={alert}
            onClick={this.checkClicksAndDistance}
            title="Местоположение определено"
            message={messageString}
            subMessage={retriesStr}
          />
        </SafeAreaView>
      );
    }
}

const styles = StyleSheet.create({
  mapStyle: {
    ...StyleSheet.absoluteFillObject,
  },
  bottomContainer: {
    backgroundColor: '#fff',
    height: height / 7,
    position: 'absolute',
    width: '100%',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    justifyContent: 'center',
    bottom: 0,
  },
  topContainer: {
    position: 'absolute',
    width: '100%',
    justifyContent: 'center',
  },
  buttonStyle: {
    backgroundColor: 'rgb(95,95,209)',
    marginHorizontal: 20,
    height: 55,
  },
  buttonDisabledStyle: {
    backgroundColor: 'rgba(95,95,209, 0.7)',
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 16,
  },
});

export default MapScreen;
