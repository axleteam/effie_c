import { connect } from 'react-redux';
import MapScreen from './MapScreen';
import { setWorkingDayStage } from '../../store/workingDay/duck';
import { updateCurrentVisit, createCurrentVisit } from '../../store/currentRoute/duck';


const mapStateToProps = ({ workingDayStore, currentRouteStore }) => ({
  workingDayStore,
  curVisitQuests: currentRouteStore.curVisitQuests,
  TTData: currentRouteStore.curTT,
  stepData: currentRouteStore.curStepData,
});

const mapDispatchToProps = {
  setWorkingDayStage,
  updateCurrentVisit,
  createCurrentVisit,
};

export default connect(mapStateToProps, mapDispatchToProps)(MapScreen);
