import { connect } from 'react-redux';
import ResultScreen from './ResultScreen';

const mapStateToProps = ({ serverDataStore }) => ({
  serverDataStore,
});

export default connect(mapStateToProps)(ResultScreen);
