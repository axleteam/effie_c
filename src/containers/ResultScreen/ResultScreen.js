import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Dimensions,
  ScrollView,
  View,
} from 'react-native';
import { Header } from 'react-navigation';
import PropTypes from 'prop-types';
import MyHeader from '../../components/header';
import ScoreCard from '../../components/ScoreCard';

const { height } = Dimensions.get('window');
const viewportHeight = height - Header.HEIGHT;
class ResultScreen extends React.Component {
    static navigationOptions = {
      header: null,
    }

    static propTypes = {
      navigation: PropTypes.object,
      serverDataStore: PropTypes.object,
    }

    redirectToVisitScreen = () => {
      const { navigation } = this.props;
      navigation.navigate('RouteScreen');
    }

    render() {
      const { serverDataStore: { completionStatistics } } = this.props;
      return (
        <View style={styles.mainArea}>
          <MyHeader
            navigation={styles.navigation}
            title="Результаты работы"
            backButton
            backGoto={this.redirectToVisitScreen}
          />
          <ScrollView contentContainerStyle={styles.mainArea} showsVerticalScrollIndicator={false}>
            <View style={styles.viewArea}>
              <ScoreCard
                title="Торговые точки"
                get={completionStatistics.completedVisits}
                getText="Выполнен визит"
                total={completionStatistics.totalVisits}
                totalText="Всего"
              />
              <ScoreCard
                title="Вопросы по анкетам"
                get={completionStatistics.completedQuestions}
                getText="Ответов"
                total={completionStatistics.totalQuestions}
                totalText="Всего"
              />
            </View>
          </ScrollView>
        </View>
      );
    }
}

const styles = StyleSheet.create({
  mainArea: {
    minHeight: viewportHeight,
  },
  viewArea: {
    marginTop: 10,
  },
  buttonContainer: {
    paddingHorizontal: 20,
    marginTop: 20,
    height: viewportHeight * 1 / 5,
  },
  textHeading: {
    fontSize: 18,
    left: '15%',
  },
});

export default ResultScreen;
