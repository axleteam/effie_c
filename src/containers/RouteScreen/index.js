import { connect } from 'react-redux';
import RouteScreen from './RouteScreen';
import { setWorkingDayStage } from '../../store/workingDay/duck';
import { getRouteData, createCurrentVisit, setCurTT } from '../../store/currentRoute/duck';

const mapStateToProps = ({
  workingDayStore,
  serverDataStore,
  currentRouteStore,
}) => ({
  workingDayStore,
  serverDataStore,
  curRoute: currentRouteStore.curRoute,
  curRoutePhase: currentRouteStore.phase,
  currentRouteStore,
});

const mapDispatchToProps = {
  setWorkingDayStage: stage => setWorkingDayStage(stage),
  getRouteData: () => getRouteData(),
  createCurrentVisit: TTExtID => createCurrentVisit(TTExtID),
  updateData: () => ({ type: 'UPDATE_DATA' }),
  setCurTT,
};

export default connect(mapStateToProps, mapDispatchToProps)(RouteScreen);
