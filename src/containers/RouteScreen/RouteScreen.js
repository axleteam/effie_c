import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  FlatList,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';
import PropTypes from 'prop-types';
import { Header } from 'react-navigation';
import { Button, View } from 'native-base';
import MyHeader from '../../components/header';
import {
  VISIT,
  ROUTE_FINISH,
} from '../../store/workingDay/constants';
import RouteCard from '../../components/RouteCard';
import AlertModal from '../../components/AlertModal';
import { LOADING } from '../../utils/constants';

const { height } = Dimensions.get('window');
const viewportHeight = height - Header.HEIGHT;
class RouteScreen extends React.Component {
    static navigationOptions = {
      header: null,
    }

    static propTypes = {
      serverDataStore: PropTypes.object,
      getRouteData: PropTypes.func,
      setCurTT: PropTypes.func,
      curRoute: PropTypes.array,
      navigation: PropTypes.object,
      currentRouteStore: PropTypes.object,
    }

    state={
      alertModal: false,
    }

    componentWillMount() {
      const { serverDataStore: { SalerRoutes, TT }, curRoute, curRoutePhase } = this.props;
      if (typeof SalerRoutes !== 'undefined' && typeof TT !== 'undefined' && curRoute.length === 0 && curRoutePhase !== LOADING) {
        this.props.getRouteData();
      }
    }

    redirectToVisitScreen = async (TTData) => {
      const {
        setWorkingDayStage,
        createCurrentVisit,
        setCurTT,
        navigation,
      } = this.props;
      setCurTT(TTData);
      await createCurrentVisit(TTData.ExtID);
      await setWorkingDayStage(VISIT);
      navigation.navigate('VisitScreen');
    }

    checkRoutes = async () => {
      const { curRoute } = this.props;
      const check = curRoute
        .filter(route => route.isVisited === false);
      if (check.length > 0) {
        return this.setState(() => ({ alertModal: true }));
      }
      return this.redirectToSyncScreen();
    }

    closeModal = () => this.setState(() => ({ alertModal: false }));

    redirectToSyncScreen = async () => {
      const { setWorkingDayStage, navigation } = this.props;
      await setWorkingDayStage(ROUTE_FINISH);
      navigation.navigate('Syncronization', { fromRoute: null });
    }

    gotoMapScreen = async (data) => {
      const { navigation, currentRouteStore, updateData, setCurTT } = this.props;
      setCurTT(data.TTData);
      // currentRouteStore.curTT = data.TTData;
      updateData();
      navigation.navigate('MapScreen', { isQuest: false, noButton: data.isVisited });
    }

    keyExtractor = ({ TTData }, index) => `${TTData.ExtID.toString()}_${index}`;

    renderItem = ({ item, index }) => {
      const { curRoute } = this.props;
      return (
        <TouchableOpacity
          disabled={item.isVisited}
          onPress={() => this.redirectToVisitScreen(item.TTData)}
        >
          <RouteCard
            route={item}
            length={curRoute.length}
            index={index}
            onPress={() => this.gotoMapScreen(item)}
          />
        </TouchableOpacity>
      );
    }


    render() {
      const {
        mainArea,
        viewArea,
        buttonContainer,
        textHeading,
      } = styles;

      const { navigation, curRoute } = this.props;
      const { alertModal } = this.state;
      return (
        <View style={mainArea}>
          <MyHeader navigation={navigation} title="Маршрут" />
          <ScrollView contentContainerStyle={mainArea} showsVerticalScrollIndicator={false}>
            <View style={viewArea}>
              <View>
                <Text style={textHeading}>
                  {`Точек на маршруте ${curRoute.length}`}
                </Text>
                {curRoute.length > 0 && (
                  <FlatList
                    data={curRoute}
                    keyExtractor={this.keyExtractor}
                    renderItem={this.renderItem}
                  />
                )}
              </View>
            </View>
            <View style={buttonContainer}>
              <Button block style={{ backgroundColor: 'rgb(95,95,209)' }} onPress={this.checkRoutes}>
                <Text style={{ color: '#fff' }}>
                  Завершить маршрут
                </Text>
              </Button>
            </View>
            <AlertModal
              title="Завершение"
              message="Завершить маршрут на сегодня?"
              visible={alertModal}
              leftButtonText="Отмена"
              leftClick={this.closeModal}
              rightButtonText="Да"
              onClick={this.redirectToSyncScreen}
            />
          </ScrollView>
        </View>
      );
    }
}

const styles = StyleSheet.create({
  mainArea: {
    minHeight: viewportHeight,
    backgroundColor: 'rgb(233,233,237)',
  },
  viewArea: {
    paddingRight: 15,
    flexDirection: 'row',
    marginTop: 15,
    minHeight: viewportHeight * 4 / 5,
  },
  buttonContainer: {
    paddingHorizontal: 20,
    marginTop: 20,
    height: viewportHeight * 1 / 5,
  },
  textHeading: {
    fontSize: 18,
    left: '15%',
  },
});

export default RouteScreen;
