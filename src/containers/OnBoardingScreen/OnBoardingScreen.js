import React from 'react';
import {
  StyleSheet, Text, View, Image, StatusBar,
} from 'react-native';
import Permissions from 'react-native-permissions';
import Swiper from 'react-native-swiper';
import { Button } from 'native-base';
import logo from '../../img/s4.png';
import twoLogo from '../../img/4.png';
import thirdLogo from '../../img/3.png';
import fourth from '../../img/2.png';
import fifth from '../../img/5.png';

import {
  ROUTE_PREPARING,
} from '../../store/workingDay/constants';

class OnBoardingScreen extends React.Component {
    static navigationOptions = {
      header: null,
    }
    
    componentDidMount() {
      this.checkPermissions();
    }

    checkPermissions = () => {
      Permissions.request('camera').then((response) => {
        Permissions.request('photo').then((response) => {
          Permissions.request('location').then((response) => {
          });
        });
      });
    }

    redirectToSynchronizationScreenOnPreparing = async () => {
      const { setWorkingDayStage, navigation } = this.props;
      await setWorkingDayStage(ROUTE_PREPARING);
      navigation.navigate('Syncronization');
    }


    render() {
      const {
        textStyle,
        buttonText,
      } = styles;
      return (
        <View style={styles.container}>
          <StatusBar barStyle="dark-content" translucent={true} />

          <Swiper
            style={styles.wrapper}
            autoplay
            autoplayTimeout={3.5}
            dot={(
              <View style={{
                backgroundColor: 'rgba(95,95,209,0.57)',
                width: 8,
                height: 8,
                borderRadius: 8,
                marginLeft: 4,
                marginRight: 4,
              }}
              />
            )}
            activeDot={(
              <View style={{
                backgroundColor: 'rgb(95,95,209)',
                width: 10,
                height: 10,
                borderRadius: 8,
                marginLeft: 4,
                marginRight: 4,
              }}
              />
              )}
            paginationStyle={{
              bottom: 30,
            }}
            loop={false}
          >
            <View style={styles.slide}>
              <View>
                <Image
                  style={styles.image}
                  source={logo}
                  resizeMode="cover"
                />
              </View>
              <Text style={textStyle}>
              Отличное начало рабочего дня!
              </Text>
            </View>
            <View style={styles.slide}>
              <View>
                <Image
                  style={styles.image}
                  source={twoLogo}
                  resizeMode="cover"
                />
              </View>
              <Text style={textStyle}>
              Данные, которые ты соберешь,
              очень важны для компании
              </Text>
            </View>

            <View style={styles.slide}>
              <View>
                <Image
                  style={styles.image}
                  source={thirdLogo}
                  resizeMode="cover"
                />
              </View>
              <Text style={textStyle}>
              Они помогут построить аналитику
              </Text>
            </View>
            <View style={styles.slide}>
              <View>
                <Image
                  style={styles.image}
                  source={fourth}
                  resizeMode="cover"
                />
              </View>
              <Text style={textStyle}>
              И корректировать стратегию продаж
              </Text>
            </View>
            <View style={styles.slide}>
              <View>
                <Image
                  style={styles.image}
                  source={fifth}
                  resizeMode="cover"
                />
              </View>
              <Text style={textStyle}>
              Спасибо!
              </Text>
            </View>
          </Swiper>
          <Button onPress={this.redirectToSynchronizationScreenOnPreparing} block style={buttonText}>
            <Text style={{ color: '#fff', height: 18 }}>
                  Начать рабочий день
            </Text>
          </Button>
        </View>
      );
    }
}

const styles = StyleSheet.create({
  wrapper: {
    // backgroundColor: '#f00'
  },

  slide: {
    flex: 1,
    alignItems: 'center',
    marginHorizontal: 20,
  },
  container: {
    flex: 1,
    backgroundColor: 'rgb(233,233,237)',
  },

  textStyle: {
    flex: 1,
    flexWrap: 'wrap',
    textAlign: 'center',
    height: 20,
    marginTop: 50,
  },
  buttonText: {
    backgroundColor: 'rgb(95,95,209)',
    height: 50,
    fontWeight: 'bold',
    marginBottom: 50,
    marginHorizontal: 20,
  },
  imgBackground: {
    width: 30,
    height: 50,
    backgroundColor: 'transparent',
    position: 'absolute',
  },

  image: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 115,
  },
});

export default OnBoardingScreen;
