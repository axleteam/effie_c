import { connect } from 'react-redux';
import OnBoardingScreen from './OnBoardingScreen';
import { setWorkingDayStage } from '../../store/workingDay/duck';

const mapStateToProps = ({ workingDayStore }) => ({
  workingDayStore,
});

const mapDispatchToProps = {
  setWorkingDayStage: stage => setWorkingDayStage(stage),
  fire: () => ({ type: 'PUSH_DATA' }),
};

export default connect(mapStateToProps, mapDispatchToProps)(OnBoardingScreen);
