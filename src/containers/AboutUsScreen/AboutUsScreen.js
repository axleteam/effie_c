import React from 'react';
import {
  StyleSheet, View, Image, Dimensions, Text, ScrollView, StatusBar,
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import logo from '../../img/logoEffie.png';
import MyHeader from '../../components/header';

const { height } = Dimensions.get('window');
const appVersion = `${DeviceInfo.getVersion()}_${DeviceInfo.getBuildNumber()}`;

class AboutUsScreen extends React.Component {
  static navigationOptions = {
    header: null,
  }

  handleChange = (field, value) => {
    const formValues = { ...this.state.formValues };
    formValues[field] = value;
    this.setState(() => ({ formValues }));
  };

  render() {
    const {
      footerContainer,
      logoContainer,
      mainPadding,
      textView,
      textStyle,
      textStyles,
    } = styles;
    const { navigation } = this.props;
    return (
      <View style={mainPadding}>
        <StatusBar barStyle="light-content" translucent={true} />
        <MyHeader navigation={navigation} title="О программе" backButton backGoto={() => navigation.goBack()} />
        <ScrollView>
          <View style={logoContainer}>
            <Image style={{ height: 68 }} source={logo} resizeMode="contain" />
          </View>
          <View style={textView}>
            <Text style={{ color: 'rgb(5,6,51)' }}>
              Effie> - 1Т-сервис управления и автоматизации продаж и мерчандайзинга, разработанный специально для дистрибьюторов и производителей с учетом опыта мировых компаний. Разработка велась на базе передовых технологий. Сервис прост и удобен в использовании - мобильный сотрудник начинает работу на маршруте, нажав на кнопку е№е>. Это удобный инструмент для управления целями функции продаж; моделирования и контроля процессов продаж и мерчандайзинга, работы отдела продаж и мерчандайзинга; управления рабочим днем мобильного сотрудника для эффективной работы в торговых точках. С помощью 1Т-сервиса торговый представитель получает возможность формировать заказ в торговой точке, выполнять другие поручения руководителя. При этом он имеет оперативный доступ к актуальным остаткам товара на складе, справочнику товаров и т.д. Effie> помогает осуществлять сбор любой информации в торговой точке, в том числе, данные о ценах, акциях, сроках годности, контролировать другие договоренности с торговыми сетями. Сервис дает возможность отправлять фотоотчеты с привязкой к конкретному объекту. Результаты работы мобильного сотрудника отображаются в отчете «Эффективность мобильного сотрудника», что позволяет в дальнейшем корректировать его работу. 1Т-сервис также позволяет оптимизировать количество мобильных сотрудников, пробег автомобилей и снизить затраты на топливо. СРЗ-трекинг дает возможность контролировать следование мобильным сотрудником назначенным маршрутом, время пребывания на маршруте и в торговой точке. Все данные передаются в систему и защищены от постороннего вмешательства. Сервис оснащен уникальной системой отчетности, которая дает возможность онлайн из любой точки мира анализировать тенденции и реальные бизнес-показатели с помощью панели мониторинга, а также быстро оценить консолидированную информацию для принятия управленческих решений
            </Text>
          </View>
          <View style={footerContainer}>
            <Text style={textStyle}>
              Для получения дополнительной информации:
            </Text>
            <Text style={textStyle}>
              www.ipland.com.ua
            </Text>
            <Text style={textStyle}>
              044 364 8830
            </Text>
            <Text style={textStyle}>
              IPLAND - IT Service Provider
            </Text>
            <Text style={textStyles}>
              Текущая версия {appVersion}
            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({

  mainPadding: {
    flex: 1,
    backgroundColor: 'rgb(236,236,239)',
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: height / 5,
  },
  textView: {
    flex: 1,
    paddingHorizontal: 20,
  },
  footerContainer: {
    marginTop: 20,
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgb(247,247,247)',
    textAlign: 'center',
  },
  textStyle: {
    paddingHorizontal: 20.5,
    color: 'rgb(133,133,133)',
    textAlign: 'center',
    letterSpacing: 0.8,
    alignItems: 'center',
  },
  textStyles: {
    height: 14,
    marginTop: 20,
    textAlign: 'center',
    alignItems: 'center',
    paddingBottom: 30,
    color: 'rgb(183,183,185)',
  },

});

export default AboutUsScreen;
