import React from 'react';
import {
  StyleSheet, View, Text, ScrollView, TouchableOpacity,
  NetInfo, Platform,
} from 'react-native';
import { Button } from 'native-base';
import PropTypes from 'prop-types';
import call from 'react-native-phone-call';
import DeviceInfo from 'react-native-device-info';
import { EfInput } from '../../components/form';
import MyHeader from '../../components/header';
import FileAttachmentBlock from './parts/FileAttachmentBlock';
import {
  FEEDBACK_FROM_EMAIL, FEEDBACK_TO_EMAIL, ERROR, SUCCESS,
} from '../../utils/constants';

import {
  ROUTE_PREPARING,
} from '../../store/workingDay/constants';
import Loader from '../../components/Loader';


class FeedbackScreen extends React.Component {
  static navigationOptions = {
    header: null,
  }

  constructor(props) {
    super(props);
    this.state = {
      formValues: {},
      attachments: [],
      isLoaderShowing: false,
      error: null,
      isConnected: false,
      connectionType: 'unknown',
      deviceInfo: '',
    };
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this.onInitialNetConnection,
    );
    setTimeout(() => this.getDeviceInfo(), 1000);
  }

  static propTypes = {
    navigation: PropTypes.object,
    workingDayStage: PropTypes.string,
    feedBackRouteStore: PropTypes.object,
    sendMail: PropTypes.func,
    user: PropTypes.object,
  };

  onInitialNetConnection = () => {
    NetInfo.isConnected.removeEventListener(this.onInitialNetConnection);
  };

  componentWillReceiveProps = (nextProps) => {
    const { feedBackRouteStore } = nextProps;
    if (feedBackRouteStore.sendMailPhase === SUCCESS) {
      this.setState({
        isLoaderShowing: false,
      });
      this.goBack();
    }
    if (feedBackRouteStore.sendMailPhase === ERROR) {
      this.setState({
        error: 'При отправке сообщения произошла ошибка',
        isLoaderShowing: false,
      });
    }
  }

  submitFeedback = () => {
    const {
      formValues,
      attachments,
    } = this.state;
    const {
      user,
    } = this.props;
    const areAllFieldsFilled = formValues.subject && formValues.phone;
    if (areAllFieldsFilled) {
      let userInfo = '';
      if (user) {
        userInfo = `ИНФОРМАЦИЯ О ПОЛЬЗОВАТЕЛЕ: ${user.userFIO} ${user.userName} ${user.firmName} ${user.roleName}`;
      }
      const contentValue = `
        Сообщение: ${formValues.message}
        ${userInfo}
        ИНФОРМАЦИЯ ОБ УСТРОЙСТВЕ: prod_${DeviceInfo.getVersion()}_${DeviceInfo.getBuildNumber()} app inst time - ${DeviceInfo.getFirstInstallTime()} ${DeviceInfo.getModel()}

        ${this.state.deviceInfo}

        Номер телефона сотрудника: ${formValues.phone}
      `;
      const data = JSON.stringify({
        personalizations: [
          {
            to: [
              {
                email: FEEDBACK_TO_EMAIL,
              },
            ],
            subject: formValues.subject,
          },
        ],
        from: {
          email: FEEDBACK_FROM_EMAIL,
        },
        content: [
          {
            type: 'text/plain',
            value: contentValue,
          },
        ],
        attachments: (attachments && attachments.length) ? attachments : undefined,
      });
      this.setState({ isLoaderShowing: true, error: null });
      this.props.sendMail(data);
    } else {
      this.setState({ isLoaderShowing: false, error: 'Заполнены не все обязательные поля' });
    }
  }

  handleChange = (field, value) => {
    const formValues = { ...this.state.formValues };
    formValues[field] = value;
    this.setState(() => ({ formValues }));
  };

  goBack = () => {
    const { workingDayStage, navigation } = this.props;
    if (workingDayStage === ROUTE_PREPARING) {
      navigation.navigate('Syncronization');
    }
    const previousRoute = navigation.getParam('previousRoute', null);
    console.log(previousRoute);
    if (previousRoute) {
      navigation.navigate(previousRoute);
    }
  }

  onAttachmentsChange = attachments => this.setState(() => ({ attachments }));

  onPhoneCall = number => call({ number, prompt: false }).catch(console.error);

  renderFeedBackForm = () => (
    <View style={{ paddingHorizontal: 20, flex: 2 }}>
      <EfInput
        placeholder="Телефон для обратной связи"
        name="phone"
        onChange={this.handleChange}
        keyboardType="numeric"
      />
      <EfInput
        placeholder="Тема"
        name="subject"
        onChange={this.handleChange}
      />
      <EfInput
        placeholder="Описание инцидента"
        name="message"
        onChange={this.handleChange}
        multiline
      />
    </View>
  )

  renderPhoneNumbers = () => {
    const phoneNumbers = [
      '+38 (044) 333 49 94',
      '+38 (095) 286 19 89',
      '+38 (067) 623 76 73',
    ];
    const {
      footerContainerTop,
      footerTopText,
    } = styles;
    return (
      <View style={footerContainerTop}>
        {phoneNumbers.map(number => (
          <TouchableOpacity key={number} onPress={() => this.onPhoneCall(number)}>
            <Text style={footerTopText}>
              {number}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
    );
  }

  getDeviceInfo = async () => {
    const isConnected = await NetInfo.isConnected.fetch();
    const connectionInfo = await NetInfo.getConnectionInfo();
    const networkState = isConnected ? 'connected' : 'offline';
    const connectionType = (connectionInfo && connectionInfo.type) ? connectionInfo.type : 'unknown';
    const effectiveType = (connectionInfo && connectionInfo.effectiveType) ? connectionInfo.effectiveType : 'unknown';
    const battery = await DeviceInfo.getBatteryLevel();
    const deviceTime = await DeviceInfo.getLastUpdateTime();
    const deviceTimeZone = await DeviceInfo.getTimezone();
    const currentTime = new Date().toLocaleString();
    const info = `
    ${DeviceInfo.getSystemName()} - ${DeviceInfo.getSystemVersion()}
    Battery - ${Math.floor(battery * 100)}%
    NetworkInfo: type: ${connectionType}, state: ${networkState}
    Internet fast? - ${effectiveType}
    device time - ${deviceTime || currentTime} TimeZone - ${deviceTimeZone}`;
    this.setState({
      deviceInfo: info,
    });
  }

  render() {
    const {
      fileAttachmentBlockStyle,
      buttonText,
      buttonContainer,
      mainPadding,
      footerContainerTop,
      footerContainerBottom,
      footerTopText,
      footerBottomText,
      errorContainer,
      errorText,
    } = styles;
    const { navigation, user } = this.props;
    const { isLoaderShowing, error } = this.state;
    return (
      <View style={mainPadding}>
        <MyHeader navigation={navigation} title="Служба поддержки" backButton backGoto={this.goBack} />
        <ScrollView keyboardShouldPersistTaps="handled" keyboardDismissMode="on-drag">
          {this.renderFeedBackForm()}
          {user && <FileAttachmentBlock style={fileAttachmentBlockStyle} onChange={this.onAttachmentsChange} />}
          <View style={errorContainer}>
            {error && (
              <Text style={errorText}>
                {error}
              </Text>
            )}
          </View>
          <View style={buttonContainer}>
            <Button onPress={this.submitFeedback} block style={buttonText}>
              <Text style={{ color: '#fff' }}>
                Отправить
              </Text>
            </Button>
          </View>
          {this.renderPhoneNumbers()}
          {user && (
            <View style={footerContainerBottom}>
              <Text style={footerBottomText}>
                {user.userFIO}
              </Text>
              <Text style={footerBottomText}>
                {user.userName}
              </Text>
              <Text style={footerBottomText}>
                {user.firmName}
              </Text>
              <Text style={footerBottomText}>
                {user.roleName}
              </Text>
            </View>
          )}
        </ScrollView>
        {isLoaderShowing && <Loader />}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainPadding: {
    flex: 1,
    backgroundColor: 'rgb(255,255,255)',
  },
  fileAttachmentBlockStyle: {
    paddingHorizontal: 20,
    marginTop: 20,
  },
  buttonText: {
    backgroundColor: 'rgb(95,95,209)',
    height: 50,
    textAlign: 'center',
  },
  buttonContainer: {
    marginTop: 20,
    paddingHorizontal: 20,
    borderRadius: 20,
    justifyContent: 'center',
    flex: 1,
  },
  footerContainerTop: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgb(247,247,247)',
    textAlign: 'center',
    paddingVertical: 20,
    marginTop: 30,
  },
  footerContainerBottom: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgb(247,247,247)',
    textAlign: 'center',
    paddingVertical: 20,
    marginTop: 40,
  },
  footerTopText: {
    textAlign: 'center',
    fontSize: 17,
    paddingVertical: 5,
    letterSpacing: 1,
    textDecorationLine: 'underline',
    color: 'rgb(95,95,209)',
  },
  footerBottomText: {
    textAlign: 'center',
    fontSize: 17,
    paddingVertical: 5,
    letterSpacing: 1,
    color: 'rgb(133,133,133)',
  },
  errorContainer: {
    position: 'relative',
  },
  errorText: {
    color: 'red',
    textAlign: 'center',
    fontSize: 15,
    paddingTop: 20,
  },
});

export default FeedbackScreen;
