import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Image,
  ScrollView,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';

import deletePng from '../../../img/dell.png';
import plusSignIcon from '../../../img/plusIcon.png';

class FileAttachmentBlock extends Component {

  state = {
    attachments: [],
  }

  onAddPress = () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        path: 'effie',
      },
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      if (!response || response.error || response.didCancel) return;
      console.log('response: ', response);
      const attachments = [
        ...this.state.attachments,
        {
          content: response.data,
          type: 'image/jpeg',
          filename: response.fileName,
        },
      ];
      this.setState(() => ({ attachments }));
      this.props.onChange(attachments);
    });
  }

  onRemoveAttachment = (index) => {
    const attachments = [...this.state.attachments];
    attachments.splice(index, 1);
    this.setState(() => ({ attachments }));
    this.props.onChange(attachments);
  }

  render() {
    const {
      container,
      imageView,
      imageStyle,
      removeIcon,
      fileAttachmentContainer,
      fileAttachmentText,
      fileAttachmentIcon,
    } = styles;
    const { style } = this.props;
    const { attachments } = this.state;
    return (
      <View style={[container, style]}>

        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          {attachments.map((attachment, i) => (
            <View key={`${attachment.filename}_${i}`} style={imageView}>
              <Image
                source={{ uri: `data:image/jpg;base64,${attachment.content}` }}
                style={imageStyle}
              />
              <TouchableOpacity style={removeIcon} onPress={() => this.onRemoveAttachment(i)}>
                <Image source={deletePng} />
              </TouchableOpacity>
            </View>
          ))}

        </ScrollView>
        <TouchableOpacity style={fileAttachmentContainer} onPress={this.onAddPress}>
          <Text style={fileAttachmentText}>Прикрепить файл</Text>
          <Image style={fileAttachmentIcon} source={plusSignIcon} resizeMode="contain" />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
  },
  fileAttachmentContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  fileAttachmentText: {
    fontSize: 17,
    color: 'rgb(133, 133, 133)',
  },
  fileAttachmentIcon: {
  },
  imageView: {
    marginRight: 10,
    paddingRight: 8,
    paddingTop: 8,
  },
  removeIcon: {
    position: 'absolute',
    right: 0,
  },
  imageStyle: {
    height: 64,
    width: 50,
  },
});

export default FileAttachmentBlock;
