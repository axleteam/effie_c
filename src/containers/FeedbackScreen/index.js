import { connect } from 'react-redux';
import FeedbackScreen from './FeedbackScreen';
import { sendMail } from '../../store/feedbackData/duck';

const mapStateToProps = ({
  workingDayStore: { workingDayStage }, feedBackRouteStore, userStore,
}) => ({
  workingDayStage,
  user: userStore.user,
  feedBackRouteStore,
});
const mapDispatchToProps = {
  sendMail,
};
export default connect(mapStateToProps, mapDispatchToProps)(FeedbackScreen);
