import React from 'react';
import PropTypes from 'prop-types';
import {
  Text, SafeAreaView, View, StyleSheet, Image, TouchableOpacity, StatusBar,
} from 'react-native';
import { Button } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import moment from 'moment';
import { EfInput, EfCheckbox } from '../../components/form';
import bugsnag from '../../utils/bugsnag';
import Loader from '../../components/Loader';
import logo from '../../img/logoEffie.png';
import {
  ONBOARDING,
  ROUTE,
  VISIT,
  QUESTIONS,
  RESULTS,
} from '../../store/workingDay/constants';
import { LOADING, SUCCESS, ERROR, NETWORK_ERROR_MESSAGE } from '../../utils/constants';

const linkHitSlop = { top: 5, bottom: 5, left: 10, right: 10 };

class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null,
  }

  constructor(props) {
    super(props);
    const { userStore: { savedCredentials, phase } } = props;
    this.state = {
      remember: savedCredentials ? savedCredentials.remember : false,
      formValues: {
        UserName: savedCredentials && savedCredentials.UserName ? savedCredentials.UserName : '',
        Password: savedCredentials && savedCredentials.Password ? savedCredentials.Password : '',
      },
      success: phase === SUCCESS,
      isLoaderShowing: false,
      error: null,
    };
    this.checkRoute();
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.userStore.phase === SUCCESS && !this.state.success) {
      this.setState(() => ({ success: true, isLoaderShowing: false, error: null }));
      const { userStore: { user } } = nextProps;
      bugsnag.setUser(user.userGuid, user.employeeID, user.userName);
      this.checkRoute(true);
    }
    if (this.props.userStore.phase !== nextProps.userStore.phase && nextProps.userStore.phase === LOADING) {
      this.setState(() => ({ isLoaderShowing: true, error: null }));
    }
    if (this.props.userStore.phase !== nextProps.userStore.phase && nextProps.userStore.phase === ERROR) {
      const error = (nextProps.userStore.error && nextProps.userStore.error.message === NETWORK_ERROR_MESSAGE) ? 'Ошибка интернет соединения' : 'Неверно указан логин или пароль';
      this.setState(() => ({ isLoaderShowing: false, error }));
    }
  }

  checkRoute = async (exist = false) => {
    const {
      workingDayStore: { workingDayStage, dayDate },
      navigation,
      setWorkingDayStage,
      userStore: { user },
      setWorkingDayChanged,
    } = this.props;

    const curDayMoment = moment({ h: 0, m: 0, s: 0, ms: 0 }).utc({ h: 0, m: 0, s: 0, ms: 0 });
    const hasWorkingDayChanged = dayDate ? moment.utc(dayDate).diff(curDayMoment, 'days', true) < 0 : true;
    setWorkingDayChanged(hasWorkingDayChanged);

    if (user) {
      bugsnag.setUser(user.userGuid, user.EmployeeID, user.UserName);
    }

    if (hasWorkingDayChanged) {
      this.props.clearserverDataVariables();
    }

    if (user || exist) {
      if (workingDayStage === ROUTE) {
        if (hasWorkingDayChanged) {
          await setWorkingDayStage(ONBOARDING);
          navigation.navigate('OnBoarding');
        } else {
          await setWorkingDayStage(ROUTE);
          navigation.navigate('RouteScreen');
        }
      } else if (workingDayStage === VISIT) {
        await setWorkingDayStage(VISIT);
        navigation.navigate('VisitScreen');
      } else if (workingDayStage === QUESTIONS) {
        await setWorkingDayStage(QUESTIONS);
        navigation.navigate('QuestionsTableScreen');
      } else if (workingDayStage === RESULTS && !hasWorkingDayChanged) {
        navigation.navigate('ResultScreen');
      } else {
        await setWorkingDayStage(ONBOARDING);
        navigation.navigate('OnBoarding');
      }
    }

    return null;
  }


  toggleRemember = () => {
    this.setState(prevState => ({ remember: !prevState.remember }));
  }

  loginUser = () => {
    const { loginUser, updateSavedUserCreds } = this.props;
    const { formValues, remember } = this.state;
    const formData = { ...formValues, remember };
    this.setState(() => ({ success: false }), () => {
      updateSavedUserCreds({ UserName: formValues.UserName });
      loginUser(formData);
    });
  }

  handleChange = (field, value) => {
    const formValues = { ...this.state.formValues };
    formValues[field] = value;
    this.setState(() => ({ formValues }));
  };

  setPasswordInputRef = ref => this.passwordInputRef = ref;

  onEmailReturnPress = () => this.passwordInputRef.focus();

  navigateToAboutUsScreen = () => this.props.navigation.navigate('AuthAboutUs');

  /* This also called from Loader screen */
  navigateToFedbackScreen = () => {
    this.props.cancelLoginUser();
    this.setState(() => ({ isLoaderShowing: false }));
    this.props.navigation.navigate('AuthFeedback');
  }

  render() {
    const { remember, formValues: { UserName, Password } } = this.state;
    const {
      flexOne,
      mainArea,
      mainPadding,
      linksContainer,
      logoContainer,
      buttonStyle,
      bottomLinks,
      bottomLinksText,
      errorContainer,
      errorText,
      buttonText,
    } = styles;
    const { userStore } = this.props;
    const { isLoaderShowing, error } = this.state;

    return (
      <SafeAreaView style={mainArea}>
        <StatusBar barStyle="dark-content" />
        <KeyboardAwareScrollView bounces={false} contentContainerStyle={flexOne} keyboardShouldPersistTaps="always" keyboardDismissMode="on-drag">
          <View style={mainPadding}>
            <View style={logoContainer}>
              <Image style={{ height: 61 }} source={logo} resizeMode="contain" />
            </View>

            <View style={{ height: '30%', justifyContent: 'center' }}>
              <View style={errorContainer}>
                {userStore.error && (
                  <Text style={errorText}>
                    {error}
                  </Text>
                )}
              </View>
              <EfInput
                placeholder="Логин"
                name="UserName"
                onChange={this.handleChange}
                error={userStore.error}
                value={UserName}
                autoCapitalize="none"
                keyboardType="email-address"
                returnKeyType="next"
                onSubmitEditing={this.onEmailReturnPress}
                blurOnSubmit={false}
              />
              <EfInput
                onRef={this.setPasswordInputRef}
                placeholder="Пароль"
                name="Password"
                onChange={this.handleChange}
                secret
                error={userStore.error}
                value={Password}
                autoCapitalize="none"
                returnKeyType="done"
              />
              <View style={linksContainer}>
                <EfCheckbox value={remember} text="Запомнить пароль" onChange={this.toggleRemember} />
              </View>

            </View>

            <View style={bottomLinks}>
              <Button onPress={this.loginUser} block style={buttonStyle}>
                <Text style={buttonText}>
                  Войти
                </Text>
              </Button>
              <View style={{ alignItems: 'center' }}>
                <TouchableOpacity
                  onPress={this.navigateToFedbackScreen}
                  hitSlop={linkHitSlop}
                >
                  <Text style={bottomLinksText}>
                    Связаться с нами
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={this.navigateToAboutUsScreen}
                  hitSlop={linkHitSlop}
                >
                  <Text style={bottomLinksText}>
                    О программе
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>

        </KeyboardAwareScrollView>
        {isLoaderShowing && <Loader navigateToFedbackScreen={this.navigateToFedbackScreen} displayFeedBackLink />}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  flexOne: {
    flex: 1,
  },
  mainArea: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgb(236,236,239)',
    marginTop: 40,
  },
  mainPadding: {
    paddingHorizontal: 20,
    flex: 1,
    marginTop: 30,
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: '30%',
  },
  linksContainer: {
    marginTop: 10,
  },
  buttonStyle: {
    backgroundColor: 'rgb(95,95,209)',
    height: 55,
    marginTop: 20,
  },
  bottomLinks: {
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 20,
    height: '35%',
  },
  bottomLinksText: {
    paddingBottom: 20,
    color: 'rgb(5,6,51)',
    textDecorationLine: 'underline',
  },
  errorContainer: {
    position: 'relative',
  },
  errorText: {
    color: 'red',
    textAlign: 'center',
    fontSize: 15,
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
  },
});

LoginScreen.propTypes = {
  navigation: PropTypes.object,
  userStore: PropTypes.object,
  loginUser: PropTypes.func,
  setWorkingDayStage: PropTypes.func,

};

export default LoginScreen;
