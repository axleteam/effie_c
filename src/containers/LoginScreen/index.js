import { connect } from 'react-redux';
import LoginScreen from './LoginScreen';
import { loginUser, cancelLoginUser, updateSavedUserCreds } from '../../store/user/duck';
import { setWorkingDayStage, setWorkingDayDate, setWorkingDayChanged } from '../../store/workingDay/duck';
import { clearserverDataVariables } from '../../store/serverData/duck';

const mapStateToProps = ({ userStore, workingDayStore }) => ({
  userStore,
  workingDayStore,
});
const mapDispatchToProps = {
  loginUser,
  setWorkingDayStage,
  setWorkingDayDate,
  setWorkingDayChanged,
  cancelLoginUser,
  updateSavedUserCreds,
  clearserverDataVariables,
};
export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
