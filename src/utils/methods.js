import Base64 from 'base-64';
import { Observable } from 'rxjs';
import uuid from 'react-native-uuid';
import Crypto from './Crypto';
import { STEP_ID_GPS } from './constants';

export const exampleFunction = () => {};
export const getKeyByValue = (object, value) => Object.keys(object)
  .find(key => object[key] === value);

export const getRequiredFields = (QuestionItem, answer = null) => {
  let isAnswerRequired = false;
  let isPhotoRequired = false;
  let isCommentRequired = false;
  const { ObligatoryFlag, AnswerFormatID, PhotoReport } = QuestionItem;
  if (ObligatoryFlag === 1) {
    // both answer and photo are required.
    isAnswerRequired = true;
    isPhotoRequired = PhotoReport;
  }
  if (ObligatoryFlag === 1 && AnswerFormatID === '1' && answer === 'Нет') {
    //  comment is required and photo is not required
    isCommentRequired = true;
    isPhotoRequired = false;
  }
  return {
    isAnswerRequired,
    isPhotoRequired,
    isCommentRequired,
  };
};

export const getYear = date => `${new Date(date).getFullYear()}`;
export const dateFromIso = date => new Date(date).getDate();
export const checkDisabled = (index, id, data) => {
  if (!index && !data[data.ids[index]].isCompleted) {
    return 'INPROGRESS';
  }
  const findCompleted = data.ids.find(visitId => data[visitId].isCompleted && data[visitId].stepData.StepID !== STEP_ID_GPS);
  if (typeof findCompleted !== 'undefined' && data[data.ids[index]].stepData.StepID === STEP_ID_GPS) {
    return false;
  }
  if (index) {
    if (data[data.ids[index - 1]].isCompleted && !data[data.ids[index]].isCompleted) {
      return 'INPROGRESS';
    }
  }
  if (data[data.ids[index]].isCompleted) {
    return true;
  }
  return false;
};

export const calculateDistance = (pointA, pointB) => {
  const lat1 = pointA.latitude;
  const lon1 = pointA.longitude;

  const lat2 = pointB.latitude;
  const lon2 = pointB.longitude;

  const R = 6371e3; // earth radius in meters
  const fi1 = lat1 * (Math.PI / 180);
  const fi2 = lat2 * (Math.PI / 180);
  const deltaFi = (lat2 - lat1) * (Math.PI / 180);
  const deltaLambda = (lon2 - lon1) * (Math.PI / 180);

  const a = (Math.sin(deltaFi / 2) * Math.sin(deltaFi / 2))
    + ((Math.cos(fi1) * Math.cos(fi2)) * (Math.sin(deltaLambda / 2) * Math.sin(deltaLambda / 2)));

  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

  const distance = R * c;
  return distance; // in meters
};

export const getSASFromAzureStorageKey = (azureKey) => {
  const s = Base64.decode(azureKey);
  const a = s.substring(s.length - 36, s.length).toLowerCase().replace('-', '');
  const b = a.split('').reverse().join('');
  const key = b.substring(0, 11).concat('effie');

  const decode = Base64.decode(azureKey);
  const decode1 = Base64.encode(decode.substring(0, decode.length - 36));

  const cr = new Crypto('aes-128-ecb', key);
  return cr.decrypt(decode1);
};

export const getRandomUUID = () => uuid.v4();


const concatMapThroughRecursion = (funcs, obs, prevRes) => {
  if (!funcs.length) return obs.complete();
  const observable = typeof funcs[0] === 'function' ? funcs[0](prevRes) : funcs[0];
  let lastRes = null;
  observable.subscribe((res) => {
    lastRes = res;
    obs.next(res);
  }, err => obs.error(err), () => {
    concatMapThroughRecursion(funcs.slice(1), obs, lastRes);
  });
};

export const concatMapThrough = (...observables) => Observable.create(obs => concatMapThroughRecursion(observables, obs, null));

export const capitalizeFirstLetter = string => string.charAt(0).toUpperCase() + string.slice(1);

export const truncateDropdownRecommentAnswer = (str) => {
  let res = str;
  if (str[0] === '*' && str[str.length - 1] === '*') res = str.substring(1, str.length - 1);
  return res;
};

export const getRecommentDropdownAnswer = (answer) => {
  const findAnswer = answer.split('*');
  if (findAnswer.length > 1) {
    return findAnswer[1];
  }
  return answer.split(';')[0];
};

export const isCommentEmpty = comment => comment === null || comment === '';

export const isAnswerEmpty = answer => answer === null || answer === '';

export const isPhotosEmpty = photos => !photos || !photos.length;
