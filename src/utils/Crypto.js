const crypto = require('react-native-crypto');

module.exports = class Crypto {
  /**
   * 
   * @param  {String} algorithm `aes-128-ecb`
   * @param  {String} key       秘钥
   * @param  {String} iv        initialization vector
   */
  constructor(algorithm, key, iv = '') {
    this.algorithm = algorithm;
    this.key = key;
    this.iv = iv;
  }

  /**
   *
   *
   * @param  {String} message
   * @param  {String} messageEncoding
   * @param  {String} cipherEncoding  
   *
   * @return {String} encrypted
   */
  encrypt(message, messageEncoding = 'utf8', cipherEncoding = 'base64') {
    const cipher = crypto.createCipheriv(this.algorithm, this.key, this.iv);
    cipher.setAutoPadding(true);

    let encrypted = cipher.update(message, messageEncoding, cipherEncoding);
    encrypted += cipher.final(cipherEncoding);

    return encrypted;
  }

  /**
   * 
   *
   * @param  {String} encrypted
   * @param  {String} cipherEncoding 
   * @param  {String} messageEncoding
   *
   * @return {String} decrypted     
   */
  decrypt(encrypted, cipherEncoding = 'base64', messageEncoding = 'utf8') {
    const decipher = crypto.createDecipheriv(this.algorithm, this.key, this.iv);
    decipher.setAutoPadding(true);

    let decrypted = decipher.update(encrypted, cipherEncoding, messageEncoding);
    decrypted += decipher.final(messageEncoding);

    return decrypted;
  }
};