import { Alert, AsyncStorage } from 'react-native';

const base64 = require('base-64');
// import NavigatorService from './navigation';

const handleNetworkError = () => {
  // Alert.alert(
  //   'Internet connection error',
  //   'Please check your internet connection and try again',
  // );
};

const handleHTTPErrors = async (res) => {
  if (res.ok) return res;
  return res.json().then((err) => { throw err; });
};

// for testing purposes only
// make api work like it is completes with error (for example 500 error)
const fakeHandleHTTPErrors = res => res.json().then((err) => { throw err; });

const defaultHeaders = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

export default async (url, options) => {
  const { userStore } = JSON.parse(await AsyncStorage.getItem('persist:root') || '{}'); // for token based authorization
  const { user } = JSON.parse(userStore);
  let authAddedOptions = options;
  if (typeof options !== 'object') {
    authAddedOptions = {};
  }
  if (typeof authAddedOptions.headers !== 'object') {
    authAddedOptions.headers = {};
  }

  if (typeof authAddedOptions.body !== 'undefined') {
    const formData = JSON.parse(authAddedOptions.body);
    authAddedOptions.body = JSON.stringify(formData);
    defaultHeaders['Content-Type'] = 'application/json';
  }

  if (typeof authAddedOptions.data !== 'undefined') {
    const form = new FormData();
    Object.keys(authAddedOptions.data).map(key => form.append(key, authAddedOptions.data[key]));

    if (typeof authAddedOptions.upload !== 'undefined') {
      defaultHeaders['Content-Type'] = 'multipart/form-data';
    }
    authAddedOptions.body = form;
  }

  if (typeof authAddedOptions.auth !== 'undefined') {
    let formData = {};
    if (typeof authAddedOptions.body !== 'undefined') {
      formData = JSON.parse(authAddedOptions.body);
    }
    formData.UserGuid = user.userGuid;
    authAddedOptions.body = JSON.stringify(formData);
  }

  if (typeof authAddedOptions.authentication !== 'undefined') {
    defaultHeaders.Authorization = `Basic ${base64.encode('admin:Qq123456')}`;
  }


  // if (auth_token) { // for token based authorization
  //   defaultHeaders.Authorization = `Bearer ${auth_token}`;
  // }
  authAddedOptions.headers = {
    ...defaultHeaders,
    ...authAddedOptions.headers,
  };
  console.log('fetch', url, authAddedOptions);
  return fetch(url, authAddedOptions).then(handleHTTPErrors);
};
