//-------------------------------------------------------------------------------------------------
// Create a singleton instance of the bugsnag client so we don't have to duplicate our configuration
// anywhere.
//-------------------------------------------------------------------------------------------------
// https://docs.bugsnag.com/platforms/react-native/#basic-configuration
import { Client, Configuration } from 'bugsnag-react-native';
import DeviceInfo from 'react-native-device-info';
const appVersion = DeviceInfo.getVersion();
const buildVersion = DeviceInfo.getBuildNumber();

const configuration = new Configuration();
configuration.autoCaptureSessions = true;
configuration.apiKey = 'd9939c0f94b225ae465124b1dce3fd85';
configuration.appVersion = `${appVersion}_${buildVersion}`;
const client = new Client(configuration);
//-------------------------------------------------------------------------------------------------
export default client;
