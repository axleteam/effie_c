
#!/bin/sh

API_KEY="d9939c0f94b225ae465124b1dce3fd85"

echo "Arguments: 1 - build type (prod, dev), 2 - android app version, 3 - ios app version"
echo "API key: $API_KEY"

BUILD_TYPE=$1
ANDROID_VERSION=$2
IOS_VERSION=$3

mkdir sourcemaps

if [ "$BUILD_TYPE" = "dev" ]
then
    # DEV
    echo "Development sourcemaps"

    echo "Generate sourcemaps"
    react-native bundle \
        --platform ios \
        --dev true \
        --entry-file index.js \
        --bundle-output sourcemaps/ios-debug.bundle \
        --sourcemap-output sourcemaps/ios-debug.bundle.map

    react-native bundle \
        --platform android \
        --dev true \
        --entry-file index.js \
        --bundle-output sourcemaps/android-debug.bundle \
        --sourcemap-output sourcemaps/android-debug.bundle.map

    echo "Uploading sourcemaps"
    bugsnag-sourcemaps upload \
        --api-key "$API_KEY" \
        --app-version "$IOS_VERSION" \
        --minified-file sourcemaps/ios-debug.bundle \
        --source-map sourcemaps/ios-debug.bundle.map \
        --minified-url "http://localhost:8081/index.bundle?platform=ios&dev=true&minify=false"

    bugsnag-sourcemaps upload \
        --api-key "$API_KEY" \
        --app-version "$ANDROID_VERSION" \
        --minified-file sourcemaps/android-debug.bundle \
        --source-map sourcemaps/android-debug.bundle.map \
        --minified-url "http://10.0.2.2:8081/index.bundle?platform=android&dev=true&minify=false"


elif [ "$BUILD_TYPE" = "prod" ]
then
    # PROD
    echo "Production sourcemaps"

    echo "Generate sourcemaps"
    react-native bundle \
        --platform ios \
        --dev false \
        --entry-file index.js \
        --bundle-output sourcemaps/ios-release.bundle \
        --sourcemap-output sourcemaps/ios-release.bundle.map

    react-native bundle \
        --platform android \
        --dev false \
        --entry-file index.js \
        --bundle-output sourcemaps/android-release.bundle \
        --sourcemap-output sourcemaps/android-release.bundle.map


    echo "Uploading sourcemaps"
    bugsnag-sourcemaps upload \
        --api-key "$API_KEY" \
        --app-version "$IOS_VERSION" \
        --minified-file sourcemaps/ios-release.bundle \
        --source-map sourcemaps/ios-release.bundle.map \
        --minified-url main.jsbundle \
        --upload-sources

    bugsnag-sourcemaps upload \
        --api-key "$API_KEY" \
        --app-version "$ANDROID_VERSION" \
        --minified-file sourcemaps/android-release.bundle \
        --source-map sourcemaps/android-release.bundle.map \
        --minified-url index.android.bundle \
        --upload-sources

fi